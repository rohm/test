package coadjute.dto

import coadjute.states.project.ProcessStageState
import coadjute.states.project.ProjectStageState
import com.fasterxml.jackson.annotation.JsonCreator
import net.corda.core.contracts.StateAndRef

data class ProjectStageDTO(
        val projectId: String,
        val processStageId: String?,
        val name: String?,
        val description: String?,
        val order: Int?,
        val gateBit: Boolean,
        val gateStageId: String?,
        val linearId: String,
        val stages: List<ProjectStageDTO>?
)

data class ProjectStageCreateFlowDTO @JsonCreator constructor(
        val projectId: String,
        val processStageId: String?,
        val name: String,
        val description: String?,
        val order: Int?,
        val gateBit: Boolean,
        val gateStageId: String?
)

data class ProjectStageUpdateFlowDTO @JsonCreator constructor(
        val name: String,
        val description: String?,
        val order: Int?,
        val gateBit: Boolean
)

data class ProcessStageDTO(
        val projectId: String,
        val processId: String,
        val name: String,
        val gateBit: Boolean,
        val gateStageId: String?,
        val description: String?,
        val order: Int?,
        val linearId: String,
        val stages: List<ProcessStageDTO>?
)

data class ProcessStageCreateFlowDTO @JsonCreator constructor(
        val projectId: String,
        val processId: String,
        val name: String,
        val gateBit: Boolean,
        val gateStageId: String?,
        val description: String?,
        val order: Int?
)

data class ProcessStageUpdateFlowDTO @JsonCreator constructor(
        val name: String,
        val description: String?,
        val order: Int?,
        val gateBit: Boolean
)

fun mapToProcessStageDTO(processStage: ProcessStageState, processStages: List<StateAndRef<ProcessStageState>>?): ProcessStageDTO
{
    return ProcessStageDTO(
            projectId = processStage.projectId,
            processId = processStage.processId,
            name = processStage.name,
            gateBit = processStage.gateBit,
            gateStageId = processStage.gateStageId,
            description = processStage.description,
            order = processStage.order,
            linearId = processStage.linearId.toString(),
            stages = processStages?.map { mapToProcessStageDTO(it.state.data, null) }
    )
}

fun mapToProjectStageDTO(projectStage: ProjectStageState, projectStages: List<StateAndRef<ProjectStageState>>?): ProjectStageDTO
{
    return ProjectStageDTO(
            projectId = projectStage.projectId,
            processStageId = projectStage.processStageId,
            name = projectStage.name,
            gateBit = projectStage.gateBit,
            gateStageId = projectStage.gateStageId,
            description = projectStage.description,
            order = projectStage.order,
            linearId = projectStage.linearId.toString(),
            stages = projectStages?.map { mapToProjectStageDTO(it.state.data, null) }
    )
}



