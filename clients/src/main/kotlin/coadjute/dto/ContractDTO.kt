package coadjute.dto

import coadjute.states.project.ContractArtifactState
import coadjute.states.project.ContractObligationState
import coadjute.states.project.ProjectContractState
import com.fasterxml.jackson.annotation.JsonCreator
import java.io.FileInputStream

data class ContractDTO(
        val name: String,
        val linearId: String
)

data class ContractFlowDTO @JsonCreator constructor(
        val title: String
)

data class ContractArtifactDTO(
        val name: String,
        val contractId: String,
        val description: String?,
        val url: String?,
        val fileSize: Double?,
        val linearId: String
)

data class ContractArtifactCreateFlowDTO @JsonCreator constructor(
        val name: String,
        val contractId: String,
        val description: String?,
        val file: String?
)

data class ContractArtifactUpdateFlowDTO @JsonCreator constructor(
        val name: String,
        val description: String?,
        val file: String?
)

data class ContractObligationDTO @JsonCreator constructor(
        val contractId: String,
        val name: String,
        val description: String?,
        val linearId: String
)

data class ContractObligationCreateFlowDTO @JsonCreator constructor(
        val contractId: String,
        val name: String,
        val description: String?
)

data class ContractObligationUpdateFlowDTO @JsonCreator constructor(
        val name: String,
        val description: String?
)

fun mapToContractDTO(projectContract: ProjectContractState): ContractDTO
{
    return ContractDTO(
            name = projectContract.name,
            linearId = projectContract.linearId.toString()
    )
}

fun mapToContractArtifactDTO(contractArtifact: ContractArtifactState): ContractArtifactDTO
{
    return ContractArtifactDTO(
            name = contractArtifact.name,
            contractId = contractArtifact.contractId,
            description = contractArtifact.description,
            url = contractArtifact.url,
            fileSize = contractArtifact.fileSize,
            linearId = contractArtifact.linearId.toString()
    )
}

fun mapToContractObligationDTO(contractObligation: ContractObligationState): ContractObligationDTO
{
    return ContractObligationDTO(
            contractId = contractObligation.contractId,
            name = contractObligation.name,
            description = contractObligation.description,
            linearId = contractObligation.linearId.toString()
    )
}

