package coadjute.dto

import coadjute.states.job.*
import com.fasterxml.jackson.annotation.JsonCreator
import net.corda.core.contracts.StateAndRef
import net.corda.core.crypto.SecureHash
import java.time.Instant

data class JobDTO(
        val name: String,
        val projectId: String,
        val artifactBit: Boolean,
        val status: String,
        val action: String,
        val instruction: String?,
        val remarks: String?,
        val referenceJobId: String?,
        val projectStageId: String?,
        val startAt: Instant?,
        val endAt: Instant?,
        val responsibleCollaboratorId: String?,
        val responsibleUserId: String?,
        val responsibleAcceptedAt: Instant?,
        val ownerCollaboratorId: String?,
        val ownerUserId: String?,
        val ownerAcceptedAt: Instant?,
        val creatorCollaboratorId: String,
        val creatorUserId: String,
        val linearId: String
)

data class JobCreateFlowDTO @JsonCreator constructor(
        val name: String,
        val projectId: String,
        val artifactBit: Boolean,
        val status: String,
        val instruction: String?,
        val remarks: String?,
        val referenceJobId: String?,
        val projectStageId: String?,
        val startAt: String?,
        val endAt: String?,
        val responsibleCollaboratorId: String?,
        val responsibleUserId: String?,
        val responsibleAcceptedAt: String?,
        val ownerCollaboratorId: String?,
        val ownerUserId: String?,
        val ownerAcceptedAt: String?
)

data class JobUpdateFlowDTO @JsonCreator constructor(
        val name: String,
        val status: String,
        val referenceJobId: String?,
        val instruction: String?,
        val remarks: String?,
        val projectStageId: String?,
        val startAt: String?,
        val endAt: String?,
        val responsibleCollaboratorId: String?,
        val responsibleUserId: String?,
        val ownerCollaboratorId: String?,
        val ownerUserId: String?
)

data class DoJobDTO(
        val responsibleCollaboratorId: String,
        val instruction: String?,
        val responsibleUserId: String?
)

data class ReviewersDTO(
        val jobId: String,
        val collaboratorId: String,
        val acceptedAt: Instant?,
        val userId: String?,
        val linearId: String
)

data class ReviewersCreateFlowDTO(
        val jobId: String,
        val collaboratorId: String,
        val userId: String?
)

data class FollowerDTO(
        val jobId: String,
        val collaboratorId: String,
        val userId: String?,
        val linearId: String
)

data class FollowerCreateFlowDTO(
        val jobId: String,
        val collaboratorId: String,
        val userId: String?
)

data class SignatoriesDTO(
        val jobId: String,
        val collaboratorId: String,
        val acceptedAt: Instant?,
        val userId: String?,
        val linearId: String
)

data class SignatoriesCreateFlowDTO(
        val jobId: String,
        val collaboratorId: String,
        val userId: String?
)

data class JobLogDTO(
        val jobId: String,
        val content: String,
        val action: String,
        val splitJobIds: List<String>?,
        val mergeFromJobIds: List<String>?,
        val updatedByCollaboratorId: String?,
        val updatedByUserId: String?,
        val updatedByUserFirstName: String?,
        val updatedByUserMiddleName: String?,
        val updatedByUserLastName: String?,
        val updatedAt: String
)

fun mapToJobDTO(job: JobState): JobDTO
{
    return JobDTO(
            name = job.name,
            projectId = job.projectId,
            artifactBit = job.artifactBit,
            status = job.status,
            action = job.action,
            instruction = job.instruction,
            remarks = job.remarks,
            referenceJobId = job.referenceJobId,
            projectStageId = job.projectStageId,
            startAt = job.startAt,
            endAt = job.endAt,
            responsibleCollaboratorId = job.responsibleCollaboratorId,
            responsibleUserId = job.responsibleUserId,
            responsibleAcceptedAt = job.responsibleAcceptedAt,
            ownerCollaboratorId = job.ownerCollaboratorId,
            ownerUserId = job.ownerUserId,
            ownerAcceptedAt = job.ownerAcceptedAt,
            creatorCollaboratorId = job.creatorCollaboratorId,
            creatorUserId = job.creatorUserId,
            linearId = job.linearId.toString()
    )
}

fun mapToReviewersDTO(reviewers: ReviewersState): ReviewersDTO
{
    return ReviewersDTO(
            jobId = reviewers.jobId,
            collaboratorId = reviewers.collaboratorId,
            acceptedAt = reviewers.acceptedAt,
            userId = reviewers.userId,
            linearId = reviewers.linearId.toString()
    )
}

fun mapToFollowerDTO(follower: FollowerState): FollowerDTO
{
    return FollowerDTO(
            jobId = follower.jobId,
            collaboratorId = follower.collaboratorId,
            userId = follower.userId,
            linearId = follower.linearId.toString()
    )
}

fun mapToSignatoriesDTO(signatories: SignatoriesState): SignatoriesDTO
{
    return SignatoriesDTO(
            jobId = signatories.jobId,
            collaboratorId = signatories.collaboratorId,
            acceptedAt = signatories.acceptedAt,
            userId = signatories.userId,
            linearId = signatories.linearId.toString()
    )
}

fun mapToJobLogDTO(jobLog: JobLogState): JobLogDTO
{
    return JobLogDTO(
            jobId = jobLog.jobId,
            content = jobLog.content,
            action = jobLog.action,
            splitJobIds = jobLog.splitJobIds,
            mergeFromJobIds = jobLog.mergeFromJobIds,
            updatedByCollaboratorId =  jobLog.updatedByCollaboratorId,
            updatedByUserId = jobLog.updatedByUserId,
            updatedByUserFirstName =  jobLog.updatedByUserFirstName,
            updatedByUserMiddleName = jobLog.updatedByUserMiddleName,
            updatedByUserLastName = jobLog.updatedByUserLastName,
            updatedAt = jobLog.updatedAt.toString()
    )
}


