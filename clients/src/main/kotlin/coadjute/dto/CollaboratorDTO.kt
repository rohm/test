package coadjute.dto

import coadjute.states.collaborator.CollaboratorInviteState
import coadjute.states.collaborator.CollaboratorRoleState
import coadjute.states.collaborator.CollaboratorState
import com.fasterxml.jackson.annotation.JsonCreator

data class CollaboratorDTO(
        val name: String,
        val projectId: String,
        val collaboratorRoleId: String,
        val nodeO: String,
        val nodeCN: String?,
        val nodeOU: String?,
        val nodeL: String?,
        val nodeC: String?,
        val status: String,
        val remarks: String?,
        val linearId: String
)

data class CollaboratorCreateDTO @JsonCreator constructor(
        val collaboratorName: String,
        val collaboratorNodeO: String,
        val projectId: String,
        val collaboratorRoleId: String
)

data class CollaboratorUpdateDTO @JsonCreator constructor(
        val status: String,
        val remarks: String?
)

data class CollaboratorRoleDTO(
        val name: String,
        val linearId: String
)

data class CollaboratorRoleFlowDTO @JsonCreator constructor(
        val name: String
)

data class CollaboratorInviteDTO(
        val projectId: String,
        val name: String,
        val email: String,
        val normalizedEmail: String,
        val contactName: String,
        val contactNumber: String,
        val nodeO: String?,
        val linearId: String
)

data class CollaboratorInviteFlowDTO @JsonCreator constructor(
        val projectId: String,
        val name: String,
        val email: String,
        val contactName: String,
        val contactNumber: String
)


data class CollaboratorLookupDTO @JsonCreator constructor(
        val name: String,
        val normalizedName: String,
        val role: String,
        val normalizedRole: String,
        val nodeO: String
)

fun mapToCollaboratorDTO(collaborator: CollaboratorState): CollaboratorDTO
{
    return CollaboratorDTO(
            name = collaborator.name,
            projectId = collaborator.projectId,
            collaboratorRoleId = collaborator.collaboratorRoleId,
            nodeO = collaborator.nodeO,
            nodeCN = collaborator.nodeCN,
            nodeOU = collaborator.nodeOU,
            nodeL = collaborator.nodeL,
            nodeC = collaborator.nodeC,
            status = collaborator.status,
            remarks = collaborator.remarks,
            linearId = collaborator.linearId.toString()
    )
}

fun mapToCollaboratorRoleDTO(collaboratorRole: CollaboratorRoleState): CollaboratorRoleDTO
{
    return CollaboratorRoleDTO(
            name = collaboratorRole.name,
            linearId = collaboratorRole.linearId.toString()
    )
}

fun mapToCollaboratorInviteDTO(invite: CollaboratorInviteState): CollaboratorInviteDTO
{
    return CollaboratorInviteDTO(
            projectId = invite.projectId,
            name = invite.name,
            email = invite.email,
            normalizedEmail = invite.normalizedEmail,
            contactName = invite.contactName,
            contactNumber = invite.contactNumber,
            nodeO = invite.nodeO,
            linearId = invite.linearId.toString()
    )
}

fun mapToCollaboratorLookupDTO(name: String, role: String, nodeO: String): CollaboratorLookupDTO
{
    return CollaboratorLookupDTO(
            name = name,
            normalizedName = name.trim().toLowerCase(),
            role = role,
            normalizedRole = role.trim().toLowerCase(),
            nodeO = nodeO
    )
}