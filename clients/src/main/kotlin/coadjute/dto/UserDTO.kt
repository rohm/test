package coadjute.dto

import coadjute.states.user.UserRoleState
import coadjute.states.user.UserState
import com.fasterxml.jackson.annotation.JsonCreator

data class UserDTO(
        val firstName: String,
        val middleName: String?,
        val lastName: String,
        val email: String,
        val contactNumber: String?,
        val addressLine1: String?,
        val addressLine2: String?,
        val town: String?,
        val city: String?,
        val province: String?,
        val state: String?,
        val country: String?,
        val postalCode: String?,
        val organizationId: String?,
        val roleId: String,
        val image: String?,
        val linearId: String
)

data class UserRegisterFlowDTO @JsonCreator constructor(
        val firstName: String,
        val middleName: String?,
        val lastName: String,
        val email: String,
        val contactNumber: String?,
        val addressLine1: String?,
        val addressLine2: String?,
        val town: String?,
        val city: String?,
        val province: String?,
        val state: String?,
        val country: String?,
        val postalCode: String?,
        val organizationId: String,
        val roleId: String,
        val image: String?,
        val password: String
)

data class UserUpdateFlowDTO @JsonCreator constructor(
        val firstName: String,
        val middleName: String?,
        val lastName: String,
        val email: String,
        val contactNumber: String?,
        val addressLine1: String?,
        val addressLine2: String?,
        val town: String?,
        val city: String?,
        val province: String?,
        val state: String?,
        val country: String?,
        val postalCode: String?,
        val roleId: String,
        val image: String?
)

data class UserChangePasswordFlowDTO @JsonCreator constructor(
        val currentPassword: String,
        val newPassword: String
)

data class UserRoleDTO(
        val name: String,
        val linearId: String
)

data class UserRoleFlowDTO @JsonCreator constructor(
        val name: String
)

fun mapToUserDTO(user: UserState): UserDTO
{
    return UserDTO(
            firstName = user.firstName,
            middleName = user.middleName,
            lastName = user.lastName,
            email = user.email,
            contactNumber = user.contactNumber,
            addressLine1 = user.addressLine1,
            addressLine2 = user.addressLine2,
            town = user.town,
            city = user.city,
            province = user.province,
            state = user.state,
            country = user.country,
            postalCode = user.postalCode,
            organizationId = user.organizationId,
            roleId = user.roleId,
            image = user.image,
            linearId = user.linearId.toString()
    )
}

fun mapToUserRoleDTO(userRole: UserRoleState): UserRoleDTO
{
    return UserRoleDTO(
            name = userRole.name,
            linearId = userRole.linearId.toString()
    )
}

