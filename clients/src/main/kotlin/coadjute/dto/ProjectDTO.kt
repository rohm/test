package coadjute.dto

import coadjute.states.project.*
import coadjute.states.user.UserState
import com.fasterxml.jackson.annotation.JsonCreator
import net.corda.core.contracts.StateAndRef

data class ProjectDTO(
        val name: String,
        val organizationId: String,
        val startAt: String?,
        val endAt: String?,
        val budgetCurrency: String?,
        val budgetAmount: Double?,
        val image: String?,
        val industryType: String,
        val contractId: String?,
        val processId: String?,
        val programId: String?,
        val status: String,
        val percentCompleted: Int,
        val linearId: String
)

data class ProjectRegisterFlowDTO @JsonCreator constructor(
        val name: String,
        val organizationId: String,
        val startAt: String?,
        val endAt: String?,
        val budgetCurrency: String?,
        val budgetAmount: Double?,
        val image: String?,
        val contractId: String?,
        val processId: String?,
        val programId: String?
)

data class ProjectUpdateFlowDTO @JsonCreator constructor(
        val name: String,
        val startAt: String?,
        val endAt: String?,
        val budgetCurrency: String?,
        val budgetAmount: Double?,
        val image: String?,
        val contractId: String?,
        val processId: String?
)

data class ProcessDTO(
        val name: String,
        val linearId: String
)

data class ProcessFlowDTO @JsonCreator constructor(
        val name: String
)

data class ProgramDTO(
        val name: String,
        val description: String?,
        val linearId: String,
        val projects: List<ProjectDTO>?
)

data class ProgramFlowDTO @JsonCreator constructor(
        val name: String,
        val description: String?,
        val projectIds: List<String>?
)

data class SetProgramToProjectFlowDTO @JsonCreator constructor(
        val programId: String?
)

data class ProjectStatusFlowDTO @JsonCreator constructor(
        val status: String
)

data class ProjectLogDTO(
        val content: String,
        val updatedBy: String?,
        val updatedByFirstName: String?,
        val updatedByMiddleName: String?,
        val updatedByLastName: String?,
        val updatedAt: String
)

fun mapToProjectDTO(project: ProjectState): ProjectDTO
{
    return ProjectDTO(
            name = project.name,
            organizationId = project.organizationId,
            startAt = project.startAt?.toString(),
            endAt = project.endAt?.toString(),
            budgetCurrency = project.budgetCurrency,
            budgetAmount = project.budgetAmount,
            image = project.image,
            industryType = project.industryType,
            contractId = project.contractId,
            processId = project.processId,
            programId = project.programId,
            status = project.status,
            percentCompleted = 0,
            linearId = project.linearId.toString()
    )
}

fun mapToProcessDTO(process: ProcessState): ProcessDTO
{
    return ProcessDTO(
            name = process.name,
            linearId = process.linearId.toString()
    )
}

fun mapToProgramDTO(program: ProgramState, projects: List<StateAndRef<ProjectState>>?): ProgramDTO
{
    return ProgramDTO(
            name = program.name,
            description = program.description,
            linearId = program.linearId.toString(),
            projects = projects?.map { mapToProjectDTO(it.state.data) }
    )
}

fun mapToProjectLogDTO(projectLogState: ProjectLogState, updatedByUser: UserState?): ProjectLogDTO
{
    if (updatedByUser != null) {
        return ProjectLogDTO(
                content = projectLogState.content,
                updatedBy = projectLogState.updatedBy.toString(),
                updatedByFirstName = updatedByUser.firstName,
                updatedByMiddleName = updatedByUser.middleName,
                updatedByLastName = updatedByUser.lastName,
                updatedAt = projectLogState.updatedAt.toString()
        )
    }else{
        return ProjectLogDTO(
                content = projectLogState.content,
                updatedBy = "system",
                updatedByFirstName = null,
                updatedByMiddleName = null,
                updatedByLastName = null,
                updatedAt = projectLogState.updatedAt.toString()
        )
    }
}