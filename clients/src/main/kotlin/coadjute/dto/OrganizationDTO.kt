package coadjute.dto

import coadjute.states.organization.OrganizationRoleState
import coadjute.states.organization.OrganizationState
import com.fasterxml.jackson.annotation.JsonCreator

data class OrganizationDTO (
        val name: String,
        val email: String,
        val normalizedEmail: String,
        val contactNumber: String?,
        val registeredAt: String,
        val addressLine1: String?,
        val addressLine2: String?,
        val town: String?,
        val city: String?,
        val province: String?,
        val state: String?,
        val country: String?,
        val postalCode: String?,
        val image: String?,
        val organizationRoleId: String,
        val linearId: String
)

data class OrganizationRegisterFlowDTO @JsonCreator constructor(
        val name: String,
        val email: String,
        val contactNumber: String?,
        val addressLine1: String?,
        val addressLine2: String?,
        val town: String?,
        val city: String?,
        val province: String?,
        val state: String?,
        val country: String?,
        val postalCode: String?,
        val image: String?,
        val organizationRoleId: String
)

data class OrganizationUpdateFlowDTO(
        val contactNumber: String?,
        val addressLine1: String?,
        val addressLine2: String?,
        val town: String?,
        val city: String?,
        val province: String?,
        val state: String?,
        val country: String?,
        val postalCode: String?,
        val image: String?,
        val organizationRoleId: String
)

data class OrganizationRoleDTO (
        val name: String,
        val linearId: String
)

data class OrganizationRoleFlowDTO @JsonCreator constructor(
        val name: String
)

fun mapToOrganizationDTO(organization : OrganizationState): OrganizationDTO
{
    return OrganizationDTO(
            name = organization.name,
            email = organization.email,
            normalizedEmail = organization.normalizedEmail,
            contactNumber = organization.contactNumber,
            registeredAt = organization.registeredAt.toString(),
            addressLine1 = organization.addressLine1,
            addressLine2 = organization.addressLine2,
            town = organization.town,
            city = organization.city,
            province = organization.province,
            state = organization.state,
            country = organization.country,
            postalCode = organization.postalCode,
            image = organization.image,
            organizationRoleId = organization.organizationRoleId,
            linearId = organization.linearId.toString()
    )
}

fun mapToOrganizationRoleDTO(organizationRole: OrganizationRoleState): OrganizationRoleDTO
{
    return OrganizationRoleDTO(
            organizationRole.name,
            organizationRole.linearId.toString()
    )
}