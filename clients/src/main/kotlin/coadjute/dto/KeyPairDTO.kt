package coadjute.dto

import coadjute.states.job.SignatoriesState
import com.google.common.io.LineReader
import java.time.Instant
import java.util.*

data class KeyPairDTO(
        val key: String,
        val name: String
)

fun mapToKeyNamePairDTO(key: String, name: String): KeyPairDTO
{
    return KeyPairDTO(key, name)
}