package coadjute.dto

import coadjute.states.user.UserNotificationState
import com.fasterxml.jackson.annotation.JsonCreator
import java.time.Instant

data class UserNotificationDTO(
        val userId: String,
        val notificationToken: String?,
        val issuedAt: Instant?,
        val linearId: String
)

fun mapToUserNotificationDTO(notification : UserNotificationState):UserNotificationDTO{
    return UserNotificationDTO(
            userId = notification.userId,
            notificationToken = notification.notificationToken,
            issuedAt = notification.issuedAt,
            linearId = notification.linearId.toString())
}

data class UserNotificationFlowDTO @JsonCreator constructor(
        val userId: String,
        val notificationToken: String?,
        val issuedAt: Instant?
)