package coadjute.dto

import coadjute.states.project.NotificationState
import com.fasterxml.jackson.annotation.JsonCreator
import java.time.Instant


data class NotificationsDTO(
        val name: String,
        val content: String,
        val recipientCollaboratorId: String?,
        val recipientUserId: String,
        val sentAt: Instant?,
        val senderCollaboratorId: String?,
        val senderUserId: String?,
        val projectId: String?,
        val jobId: String?,
        val linearId: String
)

fun mapToNotificationsDTO(notification : NotificationState):NotificationsDTO{
    return NotificationsDTO(
            name = notification.name,
            content = notification.content,
            recipientCollaboratorId = notification.recipientCollaboratorId,
            recipientUserId = notification.recipientUserId,
            sentAt = notification.sentAt,
            senderCollaboratorId = notification.senderCollaboratorId,
            senderUserId = notification.senderUserId,
            projectId = notification.projectId,
            jobId = notification.jobId,
            linearId = notification.linearId.toString())
}

data class NotificationFlowDTO @JsonCreator constructor(
        val name: String,
        val content: String,
        val recipientCollaboratorId: String?,
        val recipientUserId: String,
        val sentAt: Instant?,
        val senderCollaboratorId: String?,
        val senderUserId: String?,
        val projectId: String?,
        val jobId: String?
)