package coadjute.dto

data class AuthenticateDTO (
        val email: String,
        val password: String
)

data class AuthResultDTO (
        val userLinearId : String,
        val firstName: String,
        val middleName: String,
        val lastName : String,
        val orgLinearId : String,
        val orgName : String,
        val accessToken: String,
        val tokenExpiresAt : String,
        val needsProfileSetupBit : Boolean
)

data class SeedDataDTO (
        val organizationRole : OrganizationRoleDTO,
        val organization: OrganizationDTO,
        val processes : List<ProcessDTO>,
        val contracts : List<ContractDTO>,
        val users : List<UserDTO>,
        val projects : List<ProjectDTO>,
        val collaborators : List<CollaboratorDTO>,
        val stages : List<ProjectStageDTO>
)

data class AuthInfo (
        val userId: String?,
        val organizationId: String?
)