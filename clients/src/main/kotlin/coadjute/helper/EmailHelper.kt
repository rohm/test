package coadjute.helper

import coadjute.common.AppConstants.Companion.EMAIL_TEMPLATES_PATH
import coadjute.common.appexceptions.MailerException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ResourceLoader
import org.springframework.stereotype.Component
import java.io.File
import java.lang.StringBuilder
import java.nio.file.Paths

@Component
class EmailHelper
{
    fun emailToText(): String
    {
        try
        {
            var path = "$EMAIL_TEMPLATES_PATH/inviteCollaborator.html"
            path = path.replace("\\","/").trim()
            println(path)
            return File(path).readText()
        } catch (ex: Exception) {
            throw MailerException(String.format("Failed to construct mail, error: %s", ex.message.toString()))
        }

    }

//    fun organizationRegistration(organizationName: String, contactNumber: String, addressOne: String,
//                                 addressTwo: String, town: String, city: String, province: String,
//                                 state: String, country: String, postalCode: String,
//                                 organizationRoleId: String) : String
//    {
//        val sb = StringBuilder("")
//        sb.append("")
//                .append("<!DOCTYPE html>")
//                .append("<html><head>")
//                .append("<title>Welcome to Coadjute</title>")
//                .append("</head> <body bgcolor=\"#F5F5F5\">")
//                .append("<div style=\"padding-top: 2%; margin: 0 auto; display:block; width: 60%; line-height: 1; font-family:'Arial', Helvetica,")
//                .append(" sans-serif; background-color: #ffffff; padding: 30px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.1),")
//                .append("0 6px 20px 0 rgba(0, 0, 0, 0.1); margin-top: 20px;\">")
//                .append("<img src=\"https://ipsumimage.appspot.com/80x80\" alt=\"qc-icon\"")
//                .append("style=\"display: block; margin: 0 auto;\">")
//                .append("<p style=\"text-align: center;\"><strong>Welcome to Coadjute Quality Chain!</strong></p>")
//                .append("<p style=\"padding-top: 10px\"><span>Hi $organizationName!</span></p>")
//                .append("<p style=\"padding-top: 20px\"><span>Thank you for registering on the Coadjute Quality Chain.")
//                .append("Below are your organization profile details;</span></p>")
//                .append("<div>Organization name: $organizationName</div>")
//                .append("<div>Contact number: $contactNumber</div>")
//                .append("<div>Address line 1: $addressOne</div>")
//                .append("<div>Address line 2: $addressTwo</div>")
//                .append("<div>Town: $town</div>")
//                .append("<div>City: $city</div>")
//                .append("<div>Province: $province</div>")
//                .append("<div>State: $state</div>")
//                .append("<div>Country: $country</div>")
//                .append("<div>Postal code: $postalCode</div>")
//                .append("<div>Organization Role ID: $organizationRoleId</div>")
//                .append("<span>&nbsp;</span>")
//                .append("<p><span>If you have any questions, please contact </span><a href=\"mailto:noreply-coadjute@gmail.com\"><span")
//                .append("style=\"font-weight: 400;\">noreply-coadjute@gmail.com</span></a><span")
//                .append("style=\"font-weight: 400;\">.</span></p>")
//                .append("<p>&nbsp;</p>")
//                .append("<p></p>Have a great day,</p>")
//                .append("<p>Administrator</p>")
//                .append("</div>")
//                .append("</body")
//                .append("</html>")
//        return sb.toString()
//    }

}