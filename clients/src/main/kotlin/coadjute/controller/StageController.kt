package coadjute.controller

import coadjute.dto.*
import coadjute.service.StageService
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI

private const val CONTROLLER_NAME = "api/v1/stages"

@RestController
@CrossOrigin(origins = ["*"])
@RequestMapping(CONTROLLER_NAME) // The paths for HTTP requests are relative to this base path.
class StageController(private val stageService: StageService) : BaseController()
{
    /**
     * Get all project stage
     */
    @GetMapping(value = ["/all"], produces = ["application/json"])
    private fun getAllProjectStage(): ResponseEntity<Any>
    {
        return try
        {
            val response = stageService.getAll()
            ResponseEntity.ok(response)

        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get project stage using linearId
     */
    @GetMapping(value = ["/{linearId}"], produces = ["application/json"])
    private fun getProjectStageUsingLinearId(@PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = stageService.get(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get project stage using projectId
     */
    @GetMapping(value = ["/project/{projectId}"], produces = ["application/json"])
    private fun getProjectStageUsingProjectId(@PathVariable projectId: String, @RequestParam gateBit: Boolean?): ResponseEntity<Any>
    {
        return try
        {
            val response =
                    when (gateBit) {
                        true -> stageService.getProjectGates(projectId)
                        false -> stageService.getProjectStages(projectId, false)
                        else -> stageService.getProjectStagesByProjectId(projectId)
                    }
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get project stage using projectId
     */
    @GetMapping(value = ["/project/{projectId}/gates"], produces = ["application/json"])
    private fun getProjectGatesByProjectId(@PathVariable projectId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = stageService.getProjectGates(projectId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }


    /**
     * Create project stage
     */
    @PostMapping(value = [], produces = ["application/json"])
    private fun createProjectStage(@RequestHeader headers: HttpHeaders, @RequestBody request: ProjectStageCreateFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            stageService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = stageService.createProjectStage(request)
            ResponseEntity.created(URI("/" + CONTROLLER_NAME + "/" + response.linearId)).body(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Update project stage
     */
    @PutMapping(value = ["/{linearId}"], produces = ["application/json"])
    private fun updateProjectStage(@RequestHeader headers: HttpHeaders, @PathVariable linearId: String, @RequestBody request: ProjectStageUpdateFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            stageService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = stageService.updateProjectStage(linearId, request)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get all process stage
     */
    @GetMapping(value = ["/process/all"], produces = ["application/json"])
    private fun getAllProcessStage(): ResponseEntity<Any>
    {
        return try {
            val response = stageService.getAllProcessStage()
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get process stage using linearId
     */
    @GetMapping(value = ["/process/{linearId}"], produces = ["application/json"])
    private fun getProcessStage(@PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = stageService.getProcessStage(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Create process stage
     */
    @PostMapping(value = ["/process"], produces = ["application/json"])
    private fun createProcessStage(@RequestHeader headers: HttpHeaders, @RequestBody request: ProcessStageCreateFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            stageService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = stageService.createProcessStage(request)
            ResponseEntity.created(URI("/" + CONTROLLER_NAME + "/process/" + response.linearId)).body(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Update process stage
     */
    @PutMapping(value = ["/process/{linearId}"], produces = ["application/json"])
    private fun updateProcessStage(@RequestHeader headers: HttpHeaders, @PathVariable linearId: String, @RequestBody request: ProcessStageUpdateFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            stageService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = stageService.updateProcessStage(linearId, request)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }
}