package coadjute.controller

import coadjute.dto.*
import coadjute.service.JobService
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI

private const val CONTROLLER_NAME = "api/v1/jobs"

@RestController
@CrossOrigin(origins = ["*"])
@RequestMapping(CONTROLLER_NAME) // The paths for HTTP requests are relative to this base path.
class JobController(private val jobService: JobService) : BaseController()
{
    /**
     * Get all jobs
     */
    @GetMapping(value = ["/all"], produces = ["application/json"])
    private fun getAllJobs(): ResponseEntity<Any>
    {
        return try
        {
            val response = jobService.getAll()
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get job using linearId
     */
    @GetMapping(value = ["/{linearId}"], produces = ["application/json"])
    private fun getJobUsingLinearId(@PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = jobService.get(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get job using stages
     */
    @GetMapping(value = ["/stage/{projectStageId}"], produces = ["application/json"])
    private fun getJobUsingStageId(@PathVariable projectStageId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = jobService.getJobsByStage(projectStageId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get job using projectId
     */
    @GetMapping(value = ["/project/{projectId}"], produces = ["application/json"])
    private fun getJobUsingProjectId(@PathVariable projectId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = jobService.getJobsByProject(projectId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Create Job
     */
    @PostMapping(value = [], produces = ["application/json"])
    private fun createJob(@RequestHeader headers: HttpHeaders, @RequestBody request: JobCreateFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            jobService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = jobService.createJob(request)
            ResponseEntity.created(URI("/" + CONTROLLER_NAME + "/" + response.linearId)).body(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Update project
     */
    @PutMapping(value = ["/{linearId}"], produces = ["application/json"])
    private fun updateJob(@RequestHeader headers: HttpHeaders, @PathVariable linearId: String, @RequestBody request: JobUpdateFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            jobService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = jobService.updateJob(linearId, request)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get all reviewers
     */
    @GetMapping(value = ["/reviewers/all"], produces = ["application/json"])
    private fun getAllReviewers(): ResponseEntity<Any>
    {
        return try
        {
            val response = jobService.getAllReviewers()
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get reviewers by jobId
     */
    @GetMapping(value = ["/{jobId}/reviewers"], produces = ["application/json"])
    private fun getReviewersByJobId(@PathVariable jobId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = jobService.getReviewersByJobId(jobId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get reviewer using linearId
     */
    @GetMapping(value = ["/reviewers/{linearId}"], produces = ["application/json"])
    private fun getReviewerUsingLinearId(@PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = jobService.getReviewer(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Create reviewer
     */
    @PostMapping(value = ["/reviewer"], produces = ["application/json"])
    private fun createReviewer(@RequestHeader headers: HttpHeaders, @RequestBody request: ReviewersCreateFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            jobService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = jobService.createReviewer(request)
            ResponseEntity.created(URI("/" + CONTROLLER_NAME + "/reviewer/" + response.linearId)).body(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get all follower
     */
    @GetMapping(value = ["/followers/all"], produces = ["application/json"])
    private fun getAllFollower(): ResponseEntity<Any>
    {
        return try
        {
            val response = jobService.getAllFollowers()
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get followers by jobId
     */
    @GetMapping(value = ["/{jobId}/followers"], produces = ["application/json"])
    private fun getFollowersByJobId(@PathVariable jobId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = jobService.getFollowersByJobId(jobId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get follower using linearId
     */
    @GetMapping(value = ["/follower/{linearId}"], produces = ["application/json"])
    private fun getFollowerUsingLinearId(@PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = jobService.getFollower(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Create follower
     */
    @PostMapping(value = ["/follower"], produces = ["application/json"])
    private fun createFollower(@RequestHeader headers: HttpHeaders, @RequestBody request: FollowerCreateFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            jobService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = jobService.createFollower(request)
            ResponseEntity.created(URI("/" + CONTROLLER_NAME + "/follower/" + response.linearId)).body(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get all signatories
     */
    @GetMapping(value = ["/signatories/all"], produces = ["application/json"])
    private fun getAllSignatories(): ResponseEntity<Any>
    {
        return try
        {
            val response = jobService.getAllSignatories()
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get signatories by jobId
     */
    @GetMapping(value = ["/{jobId}/signatories"], produces = ["application/json"])
    private fun getSignatoriesByJobId(@PathVariable jobId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = jobService.getSignatoriesByJobId(jobId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get signatories using linearId
     */
    @GetMapping(value = ["/signatories/{linearId}"], produces = ["application/json"])
    private fun getSignatoriesUsingLinearId(@PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = jobService.getSignatories(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Create signatories
     */
    @PostMapping(value = ["/signatories"], produces = ["application/json"])
    private fun createSignatories(@RequestHeader headers: HttpHeaders, @RequestBody request: SignatoriesCreateFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            jobService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = jobService.createSignatories(request)
            ResponseEntity.created(URI("/" + CONTROLLER_NAME + "/signatories/" + response.linearId)).body(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get job logs using linearId
     */
    @GetMapping(value = ["/{linearId}/logs/{pageNum}/{pageSize}"], produces = ["application/json"])
    private fun getAllJobLogs(@PathVariable linearId: String, @PathVariable pageNum: Int, @PathVariable pageSize: Int): ResponseEntity<Any>
    {
        return try
        {
            val response = jobService.getAllJobLog(linearId, pageNum, pageSize)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Do job
     */
    @PutMapping(value = ["/{linearId}/do"], produces = ["application/json"])
    private fun doJob(@RequestHeader headers: HttpHeaders, @PathVariable linearId: String, @RequestBody request: DoJobDTO): ResponseEntity<Any>
    {
        return try
        {
            jobService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = jobService.doJob(linearId, request)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Accept job responsibility
     */
    @PatchMapping(value = ["/{linearId}/accept"], produces = ["application/json"])
    private fun acceptJobResponsibility(@RequestHeader headers: HttpHeaders, @PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            jobService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = jobService.acceptResponsibleJob(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Decline job responsibility
     */
    @PatchMapping(value = ["/{linearId}/decline"], produces = ["application/json"])
    private fun declineJobResponsibility(@RequestHeader headers: HttpHeaders, @PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            jobService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = jobService.declineResponsibleJob(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }
}