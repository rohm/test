package coadjute.controller

import coadjute.dto.CollaboratorCreateDTO
import coadjute.dto.CollaboratorInviteFlowDTO
import coadjute.dto.CollaboratorRoleFlowDTO
import coadjute.dto.CollaboratorUpdateDTO
import coadjute.service.CollaboratorService
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI

private const val CONTROLLER_NAME = "api/v1/collaborators"

@RestController
@CrossOrigin(origins = ["*"])
@RequestMapping(CONTROLLER_NAME) // The paths for HTTP requests are relative to this base path.
class CollaboratorController(private val collaboratorService: CollaboratorService) : BaseController()
{
    /**
     * Get all collaborators
     */
    @GetMapping(value = ["/all"], produces = ["application/json"])
    private fun getAllCollaborators(): ResponseEntity<Any>
    {
        return try {
            val response = collaboratorService.getAll()
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get collaborator using linearId
     */
    @GetMapping(value = ["/{linearId}"], produces = ["application/json"])
    private fun getCollaboratorUsingLinearId(@PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = collaboratorService.get(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get collaborators by status
     */
    @GetMapping(value = ["/status/{status}"], produces = ["application/json"])
    private fun getCollaboratorsByStatus(@PathVariable status: String): ResponseEntity<Any>
    {
        return try
        {
            val response = collaboratorService.getCollaboratorsByStatus(status)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get collaborators by projectId
     */
    @GetMapping(value = ["/project/{projectId}"], produces = ["application/json"])
    private fun getCollaboratorsByProject(@PathVariable projectId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = collaboratorService.getCollaboratorsByProject(projectId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * get self as collaborator on a project
     */
    @GetMapping(value = ["/project/{projectId}/self"], produces = ["application/json"])
    private fun getSelfCollaboratorsByProject(@PathVariable projectId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = collaboratorService.getCollaboratorSelf(projectId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Create collaborator
     */
    @PostMapping(value = [], produces = ["application/json"])
    private fun createCollaborator(@RequestHeader headers: HttpHeaders, @RequestBody request: CollaboratorCreateDTO): ResponseEntity<Any>
    {
        return try
        {
            collaboratorService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = collaboratorService.createCollaborator(request)
            ResponseEntity.created(URI("/" + CONTROLLER_NAME + "/" + response.linearId)).body(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Update collaborator
     */
    @PatchMapping(value = ["/{linearId}"], produces = ["application/json"])
    private fun setCollaboratorStatus(@RequestHeader headers: HttpHeaders, @PathVariable linearId: String, @RequestBody request: CollaboratorUpdateDTO): ResponseEntity<Any>
    {
        return try
        {
            collaboratorService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = collaboratorService.setCollaboratorStatus(linearId, request)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            this.handleException(ex)
        }
    }


    /**
     * Get all collaborator role
     */
    @GetMapping(value = ["/roles/all"], produces = ["application/json"])
    private fun getAllCollaboratorRoles(): ResponseEntity<Any>
    {
        return try {
            val response = collaboratorService.getAllCollaboratorRoles()
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }


    /**
     * Get collaborator role using linearId
     */
    @GetMapping(value = ["/roles/{linearId}"], produces = ["application/json"])
    private fun getCollaboratorRoleUsingLinearId(@PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = collaboratorService.getCollaboratorRole(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Create collaborator role
     */
    @PostMapping(value = ["/roles"], produces = ["application/json"])
    private fun createCollaboratorRole(@RequestHeader headers: HttpHeaders, @RequestBody request: CollaboratorRoleFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            collaboratorService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = collaboratorService.createCollaboratorRole(request)
            ResponseEntity.created(URI("/" + CONTROLLER_NAME + "/collaborators/role/" + response.linearId)).body(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get all collaborators on all projects
     */
    @GetMapping(value = ["/invites/all"], produces = ["application/json"])
    private fun getAllCollaboratorInvites(): ResponseEntity<Any>
    {
        return try
        {
            val response = collaboratorService.getAllCollaboratorInvites()
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get all collaborator invites on one project
     */
    @GetMapping(value = ["/invites/project/{projectId}"], produces = ["application/json"])
    private fun getCollaboratorInvitesUsingProjectId(@PathVariable projectId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = collaboratorService.getAllCollaboratorInvitesUsingProjectId(projectId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get project collaborator invite using linearId
     */
    @GetMapping(value = ["/invites/{linearId}"], produces = ["application/json"])
    private fun getAllCollaboratorInviteUsingLinearId(@PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = collaboratorService.getCollaboratorInvite(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }


    /**
     * Create collaborator invite
     */
    @PostMapping(value = ["/invites"], produces = ["application/json"])
    private fun createCollaboratorInvite(@RequestHeader headers: HttpHeaders, @RequestBody request: CollaboratorInviteFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            collaboratorService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = collaboratorService.inviteCollaborator(request)
            ResponseEntity.created(URI("/" + CONTROLLER_NAME + "/invites/" + response.linearId)).body(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * lookup possible collaborators
     */
    @GetMapping(value = ["/lookup"], produces = ["application/json"])
    private fun lookupPossibleCollaborators(@RequestParam searchText: String?): ResponseEntity<Any>
    {
        return try
        {
            val response = collaboratorService.lookupCollaborators(searchText)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }


}

