package coadjute.controller

import coadjute.dto.*
import coadjute.service.ProjectService
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI

private const val CONTROLLER_NAME = "api/v1/projects"

@RestController
@CrossOrigin(origins = ["*"])
@RequestMapping(CONTROLLER_NAME) // The paths for HTTP requests are relative to this base path.
class ProjectController(private val projectService:ProjectService) : BaseController()
{
    /**
     * Get all projects
     */
    @GetMapping(value = ["/all"], produces = ["application/json"])
    private fun getAllProjects(): ResponseEntity<Any>
    {
        return try {
            val response = projectService.getAll()
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Filter projects by program
     */
    @GetMapping(value = ["filterbyprogram/{withProgram}"], produces = ["application/json"])
    private fun filterProjectsByProgram(@PathVariable withProgram: String?): ResponseEntity<Any>
    {
        return try {
            var wProgram = true
            if(withProgram != null)
                wProgram = withProgram.toBoolean()
            val response = projectService.filterProjectsByProgram(wProgram)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get all projects by organization
     */
    @GetMapping(value = ["/organization/{linearId}"], produces = ["application/json"])
    private fun getProjectsByOrganization(@PathVariable linearId: String): ResponseEntity<Any>
    {
        return try {
            val response = projectService.getProjectsByOrganization(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get project using linearId
     */
    @GetMapping(value = ["{linearId}"], produces = ["application/json"])
    private fun getProjectUsingLinearId(@PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = projectService.get(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Create a project
     */

    @PostMapping(value = [], produces = ["application/json"])
    private fun createProject(@RequestHeader headers: HttpHeaders, @RequestBody request: ProjectRegisterFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            projectService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = projectService.createProject(request)
            ResponseEntity.created(URI("/" + CONTROLLER_NAME + "/" + response.linearId )).body(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Update a project
     */
    @PutMapping(value = ["/{linearId}"], produces = ["application/json"])
    private fun updateProject(@RequestHeader headers: HttpHeaders, @PathVariable linearId: String, @RequestBody request: ProjectUpdateFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            projectService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = projectService.updateProject(linearId, request)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Change status of a project
     */
    @PatchMapping(value = ["/{linearId}/status"], produces = ["application/json"])
    private fun setProjectStatus(@RequestHeader headers: HttpHeaders, @PathVariable linearId: String, @RequestBody request: ProjectStatusFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            projectService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = projectService.setProjectStatus(linearId, request)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {

            this.handleException(ex)
        }
    }


    /**
     * Set Program To Project
     */
    @PatchMapping(value = ["/{linearId}/program"], produces = ["application/json"])
    private fun setProgramToProject(@RequestHeader headers: HttpHeaders, @PathVariable linearId: String, @RequestBody request: SetProgramToProjectFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            projectService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = projectService.setProgramToProject(linearId, request)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get all process
     */
    @GetMapping(value = ["/process/all"], produces = ["application/json"])
    private fun getAllProcess(): ResponseEntity<Any>
    {
        return try {
            val response = projectService.getAllProcess()
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get a process using linearId
     */
    @GetMapping(value = ["/process/{linearId}"], produces = ["application/json"])
    private fun getProcessUsingLinearId(@PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = projectService.getProcess(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Create a process
     */
    @PostMapping(value = ["/process"], produces = ["application/json"])
    private fun createProcess(@RequestHeader headers: HttpHeaders, @RequestBody request: ProcessFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            projectService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = projectService.createProcess(request)
            ResponseEntity.created(URI("/" + CONTROLLER_NAME + "/process/" + response.linearId)).body(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get all program
     */
    @GetMapping(value = ["/program/all"], produces = ["application/json"])
    private fun getAllProgram(): ResponseEntity<Any>
    {
        return try {
            val response = projectService.getAllPrograms()
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get program using linearId
     */
    @GetMapping(value = ["/program/{linearId}"], produces = ["application/json"])
    private fun getProgramUsingLinearId(@PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = projectService.getProgram(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Create a program
     */
    @PostMapping(value = ["/program"], produces = ["application/json"])
    private fun createProgram(@RequestHeader headers: HttpHeaders, @RequestBody request: ProgramFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            projectService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = projectService.createProgram(request)
            ResponseEntity.created(URI("/" + CONTROLLER_NAME + "/program/" + response.linearId)).body(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get project logs using linearId
     */
    @GetMapping(value = ["/{linearId}/logs/{pageNum}/{pageSize}"], produces = ["application/json"])
    private fun getAllProjectLogs(@PathVariable linearId: String, @PathVariable pageNum: Int, @PathVariable pageSize: Int): ResponseEntity<Any>
    {
        return try
        {
            val response = projectService.getAllProjectLog(linearId, pageNum, pageSize)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }
}