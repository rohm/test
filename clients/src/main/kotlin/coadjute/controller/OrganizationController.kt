package coadjute.controller

import coadjute.dto.OrganizationRegisterFlowDTO
import coadjute.dto.OrganizationRoleFlowDTO
import coadjute.dto.OrganizationUpdateFlowDTO
import coadjute.service.OrganizationService
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI

private const val CONTROLLER_NAME = "api/v1/organizations"

@RestController
@CrossOrigin(origins = ["*"])
@RequestMapping(CONTROLLER_NAME) // The paths for HTTP requests are relative to this base path.
class OrganizationController(private val organizationService: OrganizationService) : BaseController()
{    
//    /**
//     * Get all parties involve
//     */
//    @GetMapping(value = ["/collaborators"], produces = ["application/json"])
//    private fun getAllCollaborators(): ResponseEntity<Any>
//    {
//        TODO()
//    }

    /**
     * Get all organizations
     */
    @GetMapping(value = ["/all"], produces = ["application/json"])
    private fun getAllOrganizations(): ResponseEntity<Any>
    {
        return try
        {
            val response = organizationService.getAll()
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Register an organization
     */
    @PostMapping(value = [], produces = ["application/json"])
    private fun createOrganization(@RequestHeader headers: HttpHeaders, @RequestBody request: OrganizationRegisterFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            organizationService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = organizationService.createOrganization(request)
            ResponseEntity.created(URI("/" + CONTROLLER_NAME + "/" + response.linearId)).body(response)
        } catch (ex: Exception) {
            this.handleException(ex)
        }
    }

    /**
     * Update an organization
     */
    @PutMapping(value = ["{linearId}"], produces = ["application/json"])
    private fun updateOrganization(@RequestHeader headers: HttpHeaders, @PathVariable linearId: String, @RequestBody request: OrganizationUpdateFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            organizationService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = organizationService.updateOrganization(linearId, request)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            this.handleException(ex)
        }
    }

    /**
     * Get organization using linearId
     */
    @GetMapping(value = ["/{linearId}"], produces = ["application/json"])
    private fun getOrganizationUsingLinearId(@PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = organizationService.get(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get all organization role
     */
    @GetMapping(value = ["/role/all"], produces = ["application/json"])
    private fun getAllOrganizationRoles(): ResponseEntity<Any>
    {
        return try
        {
            val response = organizationService.getAllOrganizationRoles()
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get organization role using linearId
     */
    @GetMapping(value = ["/role/{linearId}"], produces = ["application/json"])
    private fun getOrganizationRoleUsingLinearId(@PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = organizationService.getOrganizationRole(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Register a role for an organization
     */

    @PostMapping(value = ["/role"], produces = ["application/json"])
    private fun createOrganizationRole(@RequestHeader headers: HttpHeaders, @RequestBody request: OrganizationRoleFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            organizationService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = organizationService.createOrganizationRole(request)
            ResponseEntity.created(URI("/" + CONTROLLER_NAME + "/role/" + response.linearId)).body(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }
}