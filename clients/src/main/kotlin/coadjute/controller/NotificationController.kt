package coadjute.controller

import coadjute.dto.NotificationFlowDTO
import coadjute.service.NotificationService
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

private const val CONTROLLER_NAME = "api/v1/notification"

@RestController
@CrossOrigin(origins = ["*"])
@RequestMapping(CONTROLLER_NAME) // The paths for HTTP requests are relative to this base path.
class NotificationController(private val notificationService : NotificationService) : BaseController() {


    @PostMapping(value = ["/send/{token}"], produces = ["application/json"])
    private fun sendNotification(@RequestHeader headers: HttpHeaders, @PathVariable token: String, @RequestBody request: NotificationFlowDTO): ResponseEntity<Any> {
        return try {
            notificationService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = notificationService.sendMessage(token, request)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    @GetMapping(value = ["/registrationToken"], produces = ["application/json"])
    private fun registrationToken(): ResponseEntity<Any> {
        return try {
            val response = notificationService.generateToken()
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }
}