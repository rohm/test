package coadjute.controller

import coadjute.dto.AuthenticateDTO
import coadjute.service.AuthService
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

private const val CONTROLLER_NAME = "api/v1/auth"

@RestController
@CrossOrigin(origins = ["*"])
@RequestMapping(CONTROLLER_NAME) // The paths for HTTP requests are relative to this base path.
class AuthController(private val authService: AuthService) : BaseController()
{
    /*
     * Authenticate login
     */
    @PostMapping(value = ["/authenticate"], produces = ["application/json"])
    private fun authenticate(@RequestBody request: AuthenticateDTO): ResponseEntity<Any>
    {
        return try
        {
            val response = authService.authenticate(request)
            ResponseEntity.ok(response)
        } catch (e: Exception) {
            return this.handleException(e)
        }
    }

//    /*
//     * TEST
//     */
//    @GetMapping(value = ["/test"], produces = ["application/json"])
//    private fun test(@RequestHeader headers: HttpHeaders): ResponseEntity<Any>
//    {
//        return try
//        {
//            val userId = getUserId(headers)
//            val organizationId = getOrganizationId(headers)
//            ResponseEntity.ok(null)
//        } catch (e: Exception) {
//            return this.handleException(e)
//        }
//    }

    @GetMapping(value = ["/seed/{orgType}"], produces = ["application/json"])
    private fun seedData(@PathVariable orgType: String): ResponseEntity<Any>
    {
        return try
        {
            val response = authService.seedData(orgType)
            ResponseEntity.ok(response)
        } catch (e: Exception) {
            return this.handleException(e)
        }
    }
}