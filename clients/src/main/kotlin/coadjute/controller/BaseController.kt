package coadjute.controller

import coadjute.common.appexceptions.MailerException
import coadjute.common.appexceptions.NotFoundException
import coadjute.common.appexceptions.UnauthorizedException
import net.corda.core.contracts.TransactionVerificationException
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity


abstract class BaseController()
{
    companion object
    {
        private val logger = LoggerFactory.getLogger(BaseController::class.java)
    }

    fun getUserId(headers: HttpHeaders): String? {
        return headers.getFirst("user-id")
    }

    fun getOrganizationId(headers: HttpHeaders): String? {
        return headers.getFirst("organization-id")
    }

    fun handleException(ex: Exception) : ResponseEntity<Any> {
        logger.error(ex.printStackTrace().toString())
        return when (ex) {
            is UnauthorizedException -> ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ex.message.toString())
            is NotFoundException -> ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.message.toString())
            is TransactionVerificationException -> ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.localizedMessage.toString())
            is MailerException -> ResponseEntity.status(HttpStatus.BAD_REQUEST).body(String.format("Mailer failed to send, error: %s", ex.message.toString()))
            else -> ResponseEntity.status(HttpStatus.BAD_REQUEST).body(String.format("Action has failed, error: %s", ex.message.toString()))
        }
    }
}

