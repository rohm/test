package coadjute.controller

import coadjute.dto.ContractArtifactCreateFlowDTO
import coadjute.dto.ContractFlowDTO
import coadjute.dto.ContractObligationCreateFlowDTO
import coadjute.dto.ContractObligationUpdateFlowDTO
import coadjute.service.ContractService
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI

private const val CONTROLLER_NAME = "api/v1/contracts"

@RestController
@CrossOrigin(origins = ["*"])
@RequestMapping(CONTROLLER_NAME) // The paths for HTTP requests are relative to this base path.
class ContractController (private val contractService: ContractService) : BaseController()
{
    /**
     * Get all project contract
     */
    @GetMapping(value = ["/all"], produces = ["application/json"])
    private fun getAllProjectContracts(): ResponseEntity<Any>
    {
        return try {
            val response = contractService.getAll()
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get project contract using linearId
     */
    @GetMapping(value = ["/{linearId}"], produces = ["application/json"])
    private fun getProjectContractUsingLinearId(@PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = contractService.get(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Create a project contract
     */
    @PostMapping(value = [], produces = ["application/json"])
    private fun createProjectContract(@RequestHeader headers: HttpHeaders, @RequestBody request: ContractFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            contractService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = contractService.createProjectContract(request)
            ResponseEntity.created(URI("/" + CONTROLLER_NAME + "/" + response.linearId)).body(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get all contract artifacts
     */
    @GetMapping(value = ["/artifacts/all"], produces = ["application/json"])
    private fun getAllContractArtifacts(): ResponseEntity<Any>
    {
        return try
        {
            val response = contractService.getAllContractArtifacts()
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get contract artifact using linearId
     */
    @GetMapping(value = ["/artifact/{linearId}"], produces = ["application/json"])
    private fun getContractArtifactUsingLinearId(@PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = contractService.getContractArtifact(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Create contract artifact
     */
    @PostMapping(value = ["/artifact"], produces = ["application/json"])
    private fun createContractArtifact(@RequestHeader headers: HttpHeaders, @RequestBody request: ContractArtifactCreateFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            contractService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = contractService.createContractArtifact(request)
            ResponseEntity.created(URI("/" + CONTROLLER_NAME + "/artifact/" + response.linearId)).body(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get all contract obligations
     */
    @GetMapping(value = ["/obligations/all"], produces = ["application/json"])
    private fun getAllContractObligations(): ResponseEntity<Any>
    {
        return try
        {
            val response = contractService.getAllContractObligations()
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get contract obligation using linearId
     */
    @GetMapping(value = ["/obligation/{linearId}"], produces = ["application/json"])
    private fun getContractObligationUsingLinearId(@PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = contractService.getContractObligations(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Create contract obligation
     */
    @PostMapping(value = ["/obligation"], produces = ["application/json"])
    private fun createContractObligation(@RequestHeader headers: HttpHeaders, @RequestBody request: ContractObligationCreateFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            contractService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = contractService.createContractObligation(request)
            ResponseEntity.created(URI("/" + CONTROLLER_NAME + "/obligation/" + response.linearId)).body(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Update contract obligation
     */
    @PutMapping(value = ["/obligation/{linearId}"], produces = ["application/json"])
    private fun updateContractObligation(@RequestHeader headers: HttpHeaders, @PathVariable linearId: String, @RequestBody request: ContractObligationUpdateFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            contractService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = contractService.updateContractObligation(linearId, request)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }
}