package coadjute.controller

import coadjute.dto.*
import coadjute.service.UserService
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI

private const val CONTROLLER_NAME = "api/v1/users"

@RestController
@CrossOrigin(origins = ["*"])
@RequestMapping(CONTROLLER_NAME) // The paths for HTTP requests are relative to this base path.
class UserController(private val userService: UserService) : BaseController()
{
    /**
     * Get all users
     */
    @GetMapping(value = ["/all"], produces = ["application/json"])
    private fun getAllProjects(): ResponseEntity<Any>
    {
        return try
        {
            val response = userService.getAll()
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get user using linearId
     */
    @GetMapping(value = ["{linearId}"], produces = ["application/json"])
    private fun getUserLinearId(@PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = userService.get(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Create a user
     */
    @PostMapping(value = [], produces = ["application/json"])
    private fun createUser(@RequestHeader headers: HttpHeaders, @RequestBody request: UserRegisterFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            userService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = userService.createUser(request)
            ResponseEntity.created(URI("/" + CONTROLLER_NAME + "/" + response.linearId)).body(response)
        } catch (ex: Exception) {
            this.handleException(ex)
        }
    }

    /**
     * Update a user
     */
    @PutMapping(value = ["{linearId}"], produces = ["application/json"])
    private fun updateUser(@RequestHeader headers: HttpHeaders, @PathVariable linearId: String, @RequestBody request: UserUpdateFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            userService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = userService.updateUser(linearId, request)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            this.handleException(ex)
        }
    }

    /**
     * Change password of a user
     */
    @PatchMapping(value = ["/password/{linearId}"], produces = ["application/json"])
    private fun changePassword(@RequestHeader headers: HttpHeaders, @PathVariable linearId: String, @RequestBody request: UserChangePasswordFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            userService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = userService.changeUserPassword(linearId, request)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            this.handleException(ex)
        }
    }

    //TODO - User Role
    /**
     * Get all user roles
     */
    @GetMapping(value = ["/roles/all"], produces = ["application/json"])
    private fun getAllUserRoles(): ResponseEntity<Any>
    {
        return try
        {
            val response = userService.getAllUserRoles()
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Get user role using linearId
     */
    @GetMapping(value = ["/roles/{linearId}"], produces = ["application/json"])
    private fun getUserRoleUsingLinearId(@PathVariable linearId: String): ResponseEntity<Any>
    {
        return try
        {
            val response = userService.getUserRole(linearId)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            return this.handleException(ex)
        }
    }

    /**
     * Create user role
     */
    @PostMapping(value = ["/roles"], produces = ["application/json"])
    private fun createUserRole(@RequestHeader headers: HttpHeaders, @RequestBody request: UserRoleFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            userService.setCurrentUserDetails(getUserId(headers), getOrganizationId(headers))
            val response = userService.createUserRole(request)
            ResponseEntity.created(URI("/" + CONTROLLER_NAME + "/roles/" + response.linearId)).body(response)
        } catch (ex: Exception) {
            this.handleException(ex)
        }
    }

    /**
     * Save User Notification Token
     */
    @PostMapping(value = ["/saveToken"], produces = ["application/json"])
    private fun saveToken(@RequestBody request: UserNotificationFlowDTO): ResponseEntity<Any>
    {
        return try
        {
            val response = userService.saveUserNotificationToken(request)
            ResponseEntity.ok(response)
        } catch (ex: Exception) {
            this.handleException(ex)
        }
    }
}