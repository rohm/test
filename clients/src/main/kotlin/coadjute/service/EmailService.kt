package coadjute.service

import coadjute.common.appexceptions.MailerException
import coadjute.service.interfaces.IEmailService
import coadjute.webserver.NodeRPCConnection
import coadjute.webserver.utilities.FlowHandlerCompletion
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.RestController
import javax.mail.MessagingException

@Service
class EmailService (private val rpc: NodeRPCConnection, private val fhc: FlowHandlerCompletion): IEmailService
{
//    companion object
//    {
//        private val logger = LoggerFactory.getLogger(RestController::class.java)
//    }

    @Autowired
    private val javaMailSender: JavaMailSender? = null

    override fun sendEmail(recipientEmail: String, subject: String, text: String)
    {
        val msg = javaMailSender!!.createMimeMessage()

        try
        {
            val helper = MimeMessageHelper(msg, true)
            helper.setTo(recipientEmail)
            helper.setSubject(subject)
            helper.setText(text, true)
            javaMailSender.send(msg)
        } catch (ex: Exception) {
            throw MailerException("Mailer error")
        }
    }
}