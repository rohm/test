package coadjute.service

import coadjute.common.AppConstants
import coadjute.common.appexceptions.NotFoundException
import coadjute.dto.*
import coadjute.flows.project.*
import coadjute.service.interfaces.IContractService
import coadjute.states.project.ContractArtifactState
import coadjute.states.project.ContractObligationState
import coadjute.states.project.ProjectContractState
import coadjute.webserver.NodeRPCConnection
import coadjute.webserver.utilities.FlowHandlerCompletion
import net.corda.core.crypto.SecureHash
import net.corda.core.internal.declaredField
import org.springframework.stereotype.Service

@Service
class ContractService (private val rpc: NodeRPCConnection, private val fhc: FlowHandlerCompletion): IContractService
{
    override var currentUserId: String = AppConstants.DEFAULT_USER_ID
    override var currentUserOrgId: String = AppConstants.DEFAULT_ORGANIZATION_ID

    override fun setCurrentUserDetails(userId: String?, organizationId: String?)
    {
        currentUserId = userId ?: currentUserId
        currentUserOrgId = organizationId ?: currentUserOrgId
    }

    override fun getAll(): Any
    {
        val projectContractStateRef = rpc.proxy.vaultQuery(ProjectContractState::class.java).states
        return projectContractStateRef.map { mapToContractDTO(it.state.data) }
    }

    override fun get(linearId: String): Any
    {
        val projectContractStateRef = rpc.proxy.vaultQuery(ProjectContractState::class.java).states
        val projectContractState = projectContractStateRef.find { stateAndRef ->
            stateAndRef.state.data.linearId.toString() == linearId
        } ?: throw NotFoundException("Project contract found")
        return mapToContractDTO(projectContractState.state.data)
    }

    override fun createProjectContract(request: ContractFlowDTO): ContractDTO
    {
        if(request.title.isBlank() || request.title.isEmpty())
            throw Exception("title cannot be empty")

        val flowReturn = rpc.proxy.startFlowDynamic(
                CreateProjectContractFlow::class.java,
                request.title
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as ProjectContractState
        return mapToContractDTO(flowResult)
    }

    override fun getAllContractArtifacts(): List<ContractArtifactDTO>
    {
        val contractArtifactStateRef = rpc.proxy.vaultQuery(ContractArtifactState::class.java).states
        return contractArtifactStateRef.map { mapToContractArtifactDTO(it.state.data) }
    }

    override fun getContractArtifact(linearId: String): ContractArtifactDTO
    {
        val contractArtifactStateRef = rpc.proxy.vaultQuery(ContractArtifactState::class.java).states
        val contractArtifactState = contractArtifactStateRef.find { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Contract artifact not found")
        return mapToContractArtifactDTO(contractArtifactState.state.data)
    }

    override fun createContractArtifact(request: ContractArtifactCreateFlowDTO): ContractArtifactDTO
    {
        if(request.contractId.isEmpty() || request.contractId.isBlank())
            throw Exception("Contract id cannot be empty")

        if(request.name.isEmpty() || request.name.isBlank())
            throw Exception("Name cannot be empty")

        val projectContractStateRef = rpc.proxy.vaultQuery(ProjectContractState::class.java).states
        val foundProjectContract = projectContractStateRef.find { it.state.data.linearId.toString() == request.contractId } ?: throw NotFoundException("Project contract not found")

        //ToDo: we should insert file storage logic here
        val flowReturn = rpc.proxy.startFlowDynamic(
                CreateContractArtifactFlow::class.java,
                request.name,
                foundProjectContract.state.data.linearId.toString(),
                request.description,
                null,
                null,
                null
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as ContractArtifactState
        return mapToContractArtifactDTO(flowResult)
    }

    override fun updateContractArtifact(linearId: String, request: ContractArtifactUpdateFlowDTO): ContractArtifactDTO
    {
        if(linearId.isEmpty() || linearId.isBlank())
            throw Exception("Contract Artifact Id cannot be empty")

        if(request.name.isEmpty() || request.name.isBlank())
            throw Exception("Name cannot be empty")

        val cArtifactStateRef = rpc.proxy.vaultQuery(ContractArtifactState::class.java).states
        val cArtifact = cArtifactStateRef.find { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Contract Artifact not found")

        //ToDo: we should insert file storage logic here
        val flowReturn = rpc.proxy.startFlowDynamic(
                UpdateContractArtifactFlow::class.java,
                request.name,
                request.description,
                null,
                null,
                null,
                cArtifact.state.data.linearId.toString()
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as ContractArtifactState
        return mapToContractArtifactDTO(flowResult)
    }

    override fun getAllContractObligations(): List<ContractObligationDTO>
    {
        val contractObligationStateRef = rpc.proxy.vaultQuery(ContractObligationState::class.java).states
        return contractObligationStateRef.map { mapToContractObligationDTO(it.state.data) }
    }

    override fun getContractObligations(linearId: String): ContractObligationDTO
    {
        val contractObligationStateRef = rpc.proxy.vaultQuery(ContractObligationState::class.java).states
        val contractObligationState = contractObligationStateRef.find { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Contract obligation not found")
        return mapToContractObligationDTO(contractObligationState.state.data)
    }

    override fun createContractObligation(request: ContractObligationCreateFlowDTO): ContractObligationDTO
    {
        if(request.name.isBlank() || request.name.isEmpty())
            throw Exception("Name cannot be empty")

        val projectContractStateRef = rpc.proxy.vaultQuery(ProjectContractState::class.java).states
        val foundProjectContract = projectContractStateRef.find { it.state.data.linearId.toString() == request.contractId } ?: throw NotFoundException("Project contract not found")

        val flowReturn = rpc.proxy.startFlowDynamic(
                CreateContractObligationFlow::class.java,
                foundProjectContract.state.data.linearId.toString(),
                request.name,
                request.description
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as ContractObligationState
        return mapToContractObligationDTO(flowResult)
    }

    override fun updateContractObligation(linearId: String, request: ContractObligationUpdateFlowDTO): ContractObligationDTO
    {
        if(request.name.isBlank() || request.name.isEmpty())
            throw Exception("Name cannot be empty")

        val contractObligationStateRef = rpc.proxy.vaultQuery(ContractObligationState::class.java).states
        val foundContractObligation = contractObligationStateRef.find { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Contract obligation not found")

        val flowReturn = rpc.proxy.startFlowDynamic(
                UpdateContractObligationFlow::class.java,
                request.name,
                request.description,
                foundContractObligation.state.data.linearId.toString()
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as ContractObligationState
        return mapToContractObligationDTO(flowResult)
    }
}
