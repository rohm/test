package coadjute.service

import coadjute.common.AppConstants
import coadjute.common.appexceptions.NotFoundException
import coadjute.common.passwordmanager.PasswordManager
import coadjute.dto.*
import coadjute.flows.organization.CreateOrganizationFlow
import coadjute.flows.organization.CreateOrganizationRoleFlow
import coadjute.flows.organization.UpdateOrganizationFlow
import coadjute.service.interfaces.IOrganizationService
import coadjute.states.organization.OrganizationRoleState
import coadjute.states.organization.OrganizationState
import coadjute.webserver.NodeRPCConnection
import coadjute.webserver.utilities.FlowHandlerCompletion
import org.springframework.stereotype.Service

@Service
class OrganizationService (private val rpc: NodeRPCConnection, private val fhc: FlowHandlerCompletion) : IOrganizationService
{
    override var currentUserId: String = AppConstants.DEFAULT_USER_ID
    override var currentUserOrgId: String = AppConstants.DEFAULT_ORGANIZATION_ID

    override fun setCurrentUserDetails(userId: String?, organizationId: String?)
    {
        currentUserId = userId ?: currentUserId
        currentUserOrgId = organizationId ?: currentUserOrgId
    }

    override fun getAll(): Any
    {
        val organizationStateRef = rpc.proxy.vaultQuery(OrganizationState::class.java).states
        return organizationStateRef.map { mapToOrganizationDTO(it.state.data) }
    }

    override fun get(linearId: String): Any
    {
        val organizationStateRef = rpc.proxy.vaultQuery(OrganizationState::class.java).states
        val organizationState = organizationStateRef.find { stateAndRef ->
            stateAndRef.state.data.linearId.toString() == linearId
        } ?: throw NotFoundException("Organization not found")
        return mapToOrganizationDTO(organizationState.state.data)
    }

    override fun createOrganizationRole(request: OrganizationRoleFlowDTO): OrganizationRoleDTO
    {
        if(request.name.isBlank() || request.name.isEmpty())
            throw Exception("Name cannot be empty")

        val organizationRoleRef = rpc.proxy.vaultQuery(OrganizationRoleState::class.java).states
        val found = organizationRoleRef.find { it.state.data.name == request.name }

        if(found != null)
            throw Exception("Organization role already exists")

        val flowReturn = rpc.proxy.startFlowDynamic(
                CreateOrganizationRoleFlow::class.java,
                request.name
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as OrganizationRoleState
        return mapToOrganizationRoleDTO(flowResult)
    }

    override fun createOrganization(request: OrganizationRegisterFlowDTO): OrganizationDTO
    {
        if(request.name.isBlank() || request.name.isEmpty())
            throw Exception("Name cannot be empty")

        if(request.email.isBlank() || request.email.isEmpty())
            throw Exception("Email cannot be empty")

        if(request.organizationRoleId.isBlank() || request.organizationRoleId.isEmpty())
            throw Exception("Organization role id cannot be empty")

        if(request.contactNumber != null && request.contactNumber.length <= 6)
            throw Exception("Contact number must be at least 6 characters")

        val organizationRoleRef = rpc.proxy.vaultQuery(OrganizationRoleState::class.java).states
        val found = organizationRoleRef.find { it.state.data.linearId.toString() == request.organizationRoleId } ?: throw NotFoundException("Organization role not found")
        val encryptKey = PasswordManager().generatePassword(true, isWithUppercase = true, isWithNumbers = true, isWithSpecial = false, length = 32)
        val flowReturn = rpc.proxy.startFlowDynamic(
                CreateOrganizationFlow::class.java,
                request.name,
                request.email,
                request.contactNumber,
                request.addressLine1,
                request.addressLine2,
                request.town,
                request.city,
                request.province,
                request.state,
                request.country,
                request.postalCode,
                request.image,
                found.state.data.linearId.toString(),
                encryptKey
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as OrganizationState
        return mapToOrganizationDTO(flowResult)
    }

    override fun updateOrganization(linearId: String, request: OrganizationUpdateFlowDTO): OrganizationDTO
    {
        val organizationRoleStateRef = rpc.proxy.vaultQuery(OrganizationRoleState::class.java).states
        val foundOrganizationRole = organizationRoleStateRef.find { it.state.data.linearId.toString() == request.organizationRoleId } ?: throw NotFoundException("Organization role not found")

        val organizationStateRef = rpc.proxy.vaultQuery(OrganizationState::class.java).states
        val foundOrganization = organizationStateRef.firstOrNull { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Not found")

        val flowReturn = rpc.proxy.startFlowDynamic(
                UpdateOrganizationFlow::class.java,
                request.contactNumber,
                request.addressLine1,
                request.addressLine2,
                request.town,
                request.city,
                request.province,
                request.state,
                request.country,
                request.postalCode,
                request.image,
                foundOrganization.state.data.encryptKey,
                foundOrganizationRole.state.data.linearId.toString(),
                foundOrganization.state.data.linearId.toString()
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as OrganizationState
        return mapToOrganizationDTO(flowResult)
    }

    override fun getAllOrganizationRoles(): List<OrganizationRoleDTO>
    {
        val organizationRoleStateRef = rpc.proxy.vaultQuery(OrganizationRoleState::class.java).states
        return organizationRoleStateRef.map { mapToOrganizationRoleDTO(it.state.data) }
    }

    override fun getOrganizationRole(linearId: String): OrganizationRoleDTO
    {
        val organizationRoleStateRef = rpc.proxy.vaultQuery(OrganizationRoleState::class.java).states
        val organizationRoleState = organizationRoleStateRef.find { stateAndRef ->
            stateAndRef.state.data.linearId.toString() == linearId
        } ?: throw NotFoundException("Organization role not found")
        return mapToOrganizationRoleDTO(organizationRoleState.state.data)
    }
}


