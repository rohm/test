package coadjute.service

import coadjute.common.AppConstants
import coadjute.common.appexceptions.NotFoundException
import coadjute.dto.*
import coadjute.flows.collaborator.CreateCollaboratorFlow
import coadjute.flows.collaborator.CreateCollaboratorRoleFlow
import coadjute.flows.collaborator.InviteCollaboratorFlow
import coadjute.flows.collaborator.UpdateCollaboratorFlow
import coadjute.helper.EmailHelper
import coadjute.service.interfaces.ICollaboratorService
import coadjute.states.collaborator.CollaboratorInviteState
import coadjute.states.collaborator.CollaboratorRoleState
import coadjute.states.collaborator.CollaboratorState
import coadjute.states.organization.OrganizationState
import coadjute.states.project.ProjectState
import coadjute.webserver.NodeRPCConnection
import coadjute.webserver.utilities.FlowHandlerCompletion
import org.springframework.stereotype.Service

@Service
class CollaboratorService(private val rpc: NodeRPCConnection, private val fhc: FlowHandlerCompletion,
                          private val emailService: EmailService, private val emailHelper: EmailHelper) : ICollaboratorService
{
    override var currentUserId: String = AppConstants.DEFAULT_USER_ID
    override var currentUserOrgId: String = AppConstants.DEFAULT_ORGANIZATION_ID

    override fun setCurrentUserDetails(userId: String?, organizationId: String?)
    {
        currentUserId = userId ?: currentUserId
        currentUserOrgId = organizationId ?: currentUserOrgId
    }

    override fun get(linearId: String): Any {
        val collaboratorStateRef = rpc.proxy.vaultQuery(CollaboratorState::class.java).states
        val collaboratorState = collaboratorStateRef.find { stateAndRef ->
            stateAndRef.state.data.linearId.toString() == linearId
        } ?: throw NotFoundException("Collaborator not found")
        return mapToCollaboratorDTO(collaboratorState.state.data)
    }

    override fun getAll(): Any {
        val collaboratorStateRef = rpc.proxy.vaultQuery(CollaboratorState::class.java).states
        return collaboratorStateRef.map { mapToCollaboratorDTO(it.state.data) }
    }

    override fun getCollaboratorsByStatus(status: String): List<CollaboratorDTO>
    {
        val collaboratorStateRef = rpc.proxy.vaultQuery(CollaboratorState::class.java).states
        val collaboratorState = collaboratorStateRef.filter { it.state.data.status == status.toUpperCase() }
        return collaboratorState.map { mapToCollaboratorDTO(it.state.data) }
    }

    override fun getCollaboratorsByProject(projectId: String): List<CollaboratorDTO>
    {
        val collaboratorStateRef = rpc.proxy.vaultQuery(CollaboratorState::class.java).states
        val collaboratorState = collaboratorStateRef.filter { it.state.data.projectId == projectId }
        return collaboratorState.map { mapToCollaboratorDTO(it.state.data) }
    }

    override fun createCollaborator(request: CollaboratorCreateDTO): CollaboratorDTO
    {
        if(request.collaboratorNodeO.isEmpty() || request.collaboratorNodeO.isBlank())
            throw Exception("Collaborator's node organisation name cannot be empty")

        if(request.projectId.isEmpty() || request.projectId.isBlank())
            throw Exception("Project ID cannot be empty")

        if(request.collaboratorRoleId.isEmpty() || request.collaboratorRoleId.isBlank())
            throw Exception("Collaborator role ID cannot be empty")

        val projectStateRef = rpc.proxy.vaultQuery(ProjectState::class.java).states
        val foundProject = projectStateRef.find { it.state.data.linearId.toString() == request.projectId} ?: throw NotFoundException("Project not found")

        val collaboratorRoleStateRef = rpc.proxy.vaultQuery(CollaboratorRoleState::class.java).states
        val foundCollaboratorRole = collaboratorRoleStateRef.find { it.state.data.linearId.toString() == request.collaboratorRoleId} ?: throw NotFoundException("Collaborator role not found")

        val flowReturn = rpc.proxy.startFlowDynamic(
                CreateCollaboratorFlow::class.java,
                request.collaboratorName,
                request.collaboratorNodeO,
                foundProject.state.data.linearId.toString(),
                foundCollaboratorRole.state.data.linearId.toString()
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as CollaboratorState
        return mapToCollaboratorDTO(flowResult)
    }

    override fun setCollaboratorStatus(linearId: String, request: CollaboratorUpdateDTO): CollaboratorDTO
    {
        if(request.status.isEmpty() || request.status.isBlank())
            throw Exception("Status cannot be empty")

        val collaboratorStateRef = rpc.proxy.vaultQuery(CollaboratorState::class.java).states
        val foundCollaborator = collaboratorStateRef.find { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Collaborator not found")

        val flowReturn = rpc.proxy.startFlowDynamic(
                UpdateCollaboratorFlow::class.java,
                request.status,
                request.remarks,
                foundCollaborator.state.data.linearId.toString()
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as CollaboratorState
        return mapToCollaboratorDTO(flowResult)
    }

    override fun getAllCollaboratorRoles(): List<CollaboratorRoleDTO>
    {
        val collaboratorRoleStateRef = rpc.proxy.vaultQuery(CollaboratorRoleState::class.java).states
        return collaboratorRoleStateRef.map { mapToCollaboratorRoleDTO(it.state.data) }
    }

    override fun getCollaboratorRole(linearId: String): CollaboratorRoleDTO
    {
        val collaboratorRoleStateRef = rpc.proxy.vaultQuery(CollaboratorRoleState::class.java).states
        val collaboratorRoleState = collaboratorRoleStateRef.find { stateAndRef ->
            stateAndRef.state.data.linearId.toString() == linearId
        } ?: throw NotFoundException("Collaborator role not found")
        return mapToCollaboratorRoleDTO(collaboratorRoleState.state.data)
    }

    override fun createCollaboratorRole(request: CollaboratorRoleFlowDTO): CollaboratorRoleDTO
    {
        if(request.name.isEmpty() || request.name.isBlank())
            throw Exception("Name cannot be empty")

        val flowReturn = rpc.proxy.startFlowDynamic(
                CreateCollaboratorRoleFlow::class.java,
                request.name
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as CollaboratorRoleState
        return mapToCollaboratorRoleDTO(flowResult)
    }

    override fun getAllCollaboratorInvites(): List<CollaboratorInviteDTO>
    {
        val inviteCollaboratorStateRef = rpc.proxy.vaultQuery(CollaboratorInviteState::class.java).states
        return inviteCollaboratorStateRef.map { mapToCollaboratorInviteDTO(it.state.data) }
    }

    override fun getAllCollaboratorInvitesUsingProjectId(projectId: String): List<CollaboratorInviteDTO>
    {
        val inviteCollaboratorStateRef = rpc.proxy.vaultQuery(CollaboratorInviteState::class.java).states
        val inviteCollaboratorStates = inviteCollaboratorStateRef.filter { it.state.data.projectId == projectId }
        return inviteCollaboratorStates.map { mapToCollaboratorInviteDTO(it.state.data) }
    }

    override fun getCollaboratorInvite(linearId: String): CollaboratorInviteDTO
    {
        val inviteCollaboratorStateRef = rpc.proxy.vaultQuery(CollaboratorInviteState::class.java).states
        val inviteCollaboratorState = inviteCollaboratorStateRef.find { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Collaborator invite not found")
        return mapToCollaboratorInviteDTO(inviteCollaboratorState.state.data)
    }

    override fun inviteCollaborator(request: CollaboratorInviteFlowDTO): CollaboratorInviteDTO
    {
        if(request.name.isEmpty() || request.name.isBlank())
            throw Exception("Name cannot be empty")

        val projectStateRef = rpc.proxy.vaultQuery(ProjectState::class.java).states
        val foundProject = projectStateRef.find { it.state.data.linearId.toString() == request.projectId } ?: throw NotFoundException("Project not found")

        val organizationStateRef = rpc.proxy.vaultQuery(OrganizationState::class.java).states
        val foundOrganization = organizationStateRef.find { it.state.data.linearId.toString() == foundProject.state.data.organizationId } ?: throw NotFoundException("Organization not found")

        val nEmail = request.email.toLowerCase().trim()
        val regex = Regex(AppConstants.REGEX_EMAIL, RegexOption.IGNORE_CASE)
        if(!regex.matches(nEmail))
            throw Exception("Must use a valid email address")

        val flowReturn = rpc.proxy.startFlowDynamic(
                InviteCollaboratorFlow::class.java,
                foundProject.state.data.linearId.toString(),
                request.name,
                request.email,
                request.contactName,
                request.contactNumber
        )
        fhc.flowHandlerCompletion(flowReturn)
        var emailContent = emailHelper.emailToText()
        emailContent = emailContent.replace("[{INVITER}]", foundOrganization.state.data.name)
        emailContent = emailContent.replace("[{INVITEE}]", request.contactName)
        emailService.sendEmail(request.email, "You are invited to collaborate", emailContent)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as CollaboratorInviteState
        return mapToCollaboratorInviteDTO(flowResult)
    }

    override fun lookupCollaborators(searchText: String?): List<CollaboratorLookupDTO> {
        val list = mutableListOf<CollaboratorLookupDTO>()
        list.add(mapToCollaboratorLookupDTO("Client","Client", "C28daaf37-2b1a-49f0-b93f-8a2aed8d4ff5"))
        list.add(mapToCollaboratorLookupDTO("PrincipalDesigner", "Principal Designer", "Ca439f287-74f4-40a5-846a-215c46e7e318"))
        list.add(mapToCollaboratorLookupDTO("PrincipalContractor", "Principal Contractor", "C4de4df03-594b-451e-ba47-ad95aea537db"))
        list.add(mapToCollaboratorLookupDTO("SubContractor", "Sub Contractor", "Cc7d33a97-0bf6-49eb-9d71-a8110ebddb4d"))
        list.add(mapToCollaboratorLookupDTO("Regulator", "Regulator", "Caedb7c4f-beb0-4816-8f25-05b0ba7fdbd2"))

        if(searchText == null || searchText.isEmpty())
            return list

        val nSearchText = searchText.toLowerCase().trim()
        return list.filter { it.normalizedName.contains(nSearchText) || it.normalizedRole.contains(nSearchText) }
    }

    override fun getCollaboratorSelf(projectId: String): CollaboratorDTO
    {
        val projectStateRef = rpc.proxy.vaultQuery(ProjectState::class.java).states
        val foundProject = projectStateRef.find { it.state.data.linearId.toString() == projectId } ?: throw NotFoundException("Project not found")

        val orgStateRef = rpc.proxy.vaultQuery(OrganizationState::class.java).states
        val orgState = orgStateRef.firstOrNull() ?: throw NotFoundException("Organization not found")
        val ownNodeO :String = orgState.state.data.nodeO

//        val ownNode = rpc.proxy.nodeInfo().legalIdentities.firstOrNull()
//        if(ownNode != null)
//        {
//            ownNodeO = ownNode.name.organisation
//        }

        val pId = foundProject.state.data.linearId.toString()
        val collaboratorStateRef = rpc.proxy.vaultQuery(CollaboratorState::class.java).states
        val found = collaboratorStateRef.find { it.state.data.projectId == pId && it.state.data.nodeO == ownNodeO } ?: throw NotFoundException("Self collaborator not found")
        return mapToCollaboratorDTO(found.state.data)
    }
}
