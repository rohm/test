package coadjute.service

import coadjute.common.AppConstants
import coadjute.common.appexceptions.NotFoundException
import coadjute.common.passwordencryption.PasswordEncryption
import coadjute.common.passwordmanager.PasswordManager
import coadjute.dto.*
import coadjute.flows.user.CreateUserFlow
import coadjute.flows.user.CreateUserRoleFlow
import coadjute.flows.user.UpdateUserProfileFlow
import coadjute.flows.user.UserNotificationFlow
import coadjute.service.interfaces.IUserService
import coadjute.states.organization.OrganizationState
import coadjute.states.user.UserNotificationState
import coadjute.states.user.UserRoleState
import coadjute.states.user.UserState
import coadjute.webserver.NodeRPCConnection
import coadjute.webserver.utilities.FlowHandlerCompletion
import org.springframework.stereotype.Service

@Service
class UserService (private val rpc: NodeRPCConnection, private val fhc: FlowHandlerCompletion): IUserService
{
    override var currentUserId: String = AppConstants.DEFAULT_USER_ID
    override var currentUserOrgId: String = AppConstants.DEFAULT_ORGANIZATION_ID

    override fun setCurrentUserDetails(userId: String?, organizationId: String?)
    {
        currentUserId = userId ?: currentUserId
        currentUserOrgId = organizationId ?: currentUserOrgId
    }

    override fun getAll(): Any
    {
        val userStateRef = rpc.proxy.vaultQuery(UserState::class.java).states
        return userStateRef.map { mapToUserDTO(it.state.data) }
    }

    override fun get(linearId: String): Any
    {
        val userStateRef = rpc.proxy.vaultQuery(UserState::class.java).states
        val userState = userStateRef.find { stateAndRef ->
            stateAndRef.state.data.linearId.toString() == linearId
        } ?: throw NotFoundException("User not found.")
        return mapToUserDTO(userState.state.data)
    }

    override fun createUser(request: UserRegisterFlowDTO): UserDTO
    {
        if(request.firstName.isBlank() || request.firstName.isEmpty())
            throw Exception("First name cannot be empty")

        if(request.lastName.isBlank() || request.lastName.isEmpty())
            throw Exception("Last name cannot be empty")

        if(request.email.isBlank() || request.email.isEmpty())
            throw Exception("Email cannot be empty")

        val nEmail = request.email.toLowerCase().trim()
        val regex = Regex(AppConstants.REGEX_EMAIL, RegexOption.IGNORE_CASE)
        if(!regex.matches(nEmail))
            throw Exception("Must use a valid email address")

        if(request.roleId.isBlank() || request.roleId.isEmpty())
            throw Exception("Role id cannot be empty")

        if(request.password.isBlank() || request.password.isEmpty())
            throw Exception("Password cannot be empty")

        val userStateRef = rpc.proxy.vaultQuery(UserState::class.java).states
        val foundUser = userStateRef.firstOrNull { it.state.data.normalizedEmail == nEmail }

        val userRoleStateRef = rpc.proxy.vaultQuery(UserRoleState::class.java).states
        val foundUserRole = userRoleStateRef.find { it.state.data.linearId.toString() == request.roleId } ?: throw NotFoundException("User role not found")

        val organizationStateRef = rpc.proxy.vaultQuery(OrganizationState::class.java).states
        val foundOrganization = organizationStateRef.find { it.state.data.linearId.toString() == request.organizationId } ?: throw NotFoundException("Organization not found")

        if(foundUser != null)
            throw Exception("A User with the same email already exists")

        val salt = PasswordManager().generatePassword(true, isWithUppercase = true, isWithNumbers = true, isWithSpecial = false, length = 32)
        val key = foundOrganization.state.data.encryptKey
        val encryptedPassword = PasswordEncryption(salt,key).encryption.encrypt(request.password)

        val flowReturn = rpc.proxy.startFlowDynamic(
                CreateUserFlow::class.java,
                request.firstName,
                request.middleName,
                request.lastName,
                request.email,
                request.contactNumber,
                request.addressLine1,
                request.addressLine2,
                request.town,
                request.city,
                request.province,
                request.state,
                request.country,
                request.postalCode,
                foundOrganization.state.data.linearId.toString(),
                foundUserRole.state.data.linearId.toString(),
                request.image,
                encryptedPassword,
                salt
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as UserState
        return mapToUserDTO(flowResult)
    }

    override fun updateUser(linearId: String, request: UserUpdateFlowDTO): UserDTO
    {
        if(request.firstName.isBlank() || request.firstName.isEmpty())
            throw Exception("First name cannot be empty")

        if(request.lastName.isBlank() || request.lastName.isEmpty())
            throw Exception("Last name cannot be empty")

        if(request.email.isBlank() || request.email.isEmpty())
            throw Exception("Email cannot be empty")

        val nEmail = request.email.toLowerCase().trim()
        val regexEmail = Regex(AppConstants.REGEX_EMAIL, RegexOption.IGNORE_CASE)
        if(!regexEmail.matches(nEmail))
            throw Exception("Must use a valid email address")

        if(request.roleId.isBlank() || request.roleId.isEmpty())
            throw Exception("Role id cannot be empty")

        val userStateRef = rpc.proxy.vaultQuery(UserState::class.java).states
        val foundUser = userStateRef.firstOrNull { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("User not found")

        val userRoleStateRef = rpc.proxy.vaultQuery(UserRoleState::class.java).states
        val foundUserRole = userRoleStateRef.find { it.state.data.linearId.toString() == request.roleId } ?: throw NotFoundException("User role not found")

        val flowReturn = rpc.proxy.startFlowDynamic(
                UpdateUserProfileFlow::class.java,
                request.firstName,
                request.middleName,
                request.lastName,
                request.email,
                request.contactNumber,
                request.addressLine1,
                request.addressLine2,
                request.town,
                request.city,
                request.province,
                request.state,
                request.country,
                request.postalCode,
                foundUserRole.state.data.linearId.toString(),
                request.image,
                foundUser.state.data.password,
                foundUser.state.data.linearId.toString()
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as UserState
        return mapToUserDTO(flowResult)
    }

    override fun changeUserPassword(linearId: String, request: UserChangePasswordFlowDTO): Boolean {
        if(request.currentPassword.isBlank() || request.currentPassword.isEmpty())
            throw Exception("Current password cannot be empty")

        if(request.newPassword.isBlank() || request.newPassword.isEmpty())
            throw Exception("New password cannot be empty")

        val userStateRef = rpc.proxy.vaultQuery(UserState::class.java).states
        val foundUser = userStateRef.firstOrNull { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("User not found")

        val organizationStateRef = rpc.proxy.vaultQuery(OrganizationState::class.java).states
        val foundOrganization = organizationStateRef.find { it.state.data.linearId.toString() == foundUser.state.data.organizationId } ?: throw NotFoundException("Organization not found")

        val newEncryptedPassword = PasswordEncryption(foundUser.state.data.salt, foundOrganization.state.data.encryptKey).encryption.encrypt(request.newPassword)

        if(foundUser.state.data.password != newEncryptedPassword)
            throw Exception("Specified current password is incorrect")

        if(foundUser.state.data.password == newEncryptedPassword)
            throw Exception("New password must be different from the current")

        val flowReturn = rpc.proxy.startFlowDynamic(
                UpdateUserProfileFlow::class.java,
                foundUser.state.data.firstName,
                foundUser.state.data.middleName,
                foundUser.state.data.lastName,
                foundUser.state.data.email,
                foundUser.state.data.contactNumber,
                foundUser.state.data.addressLine1,
                foundUser.state.data.addressLine2,
                foundUser.state.data.town,
                foundUser.state.data.city,
                foundUser.state.data.province,
                foundUser.state.data.state,
                foundUser.state.data.country,
                foundUser.state.data.postalCode,
                foundUser.state.data.roleId,
                foundUser.state.data.image,
                newEncryptedPassword,
                foundUser.state.data.linearId.toString()
        )
        fhc.flowHandlerCompletion(flowReturn)
        return true
    }

    override fun getAllUserRoles(): List<UserRoleDTO>
    {
        val userRoleStateRef = rpc.proxy.vaultQuery(UserRoleState::class.java).states
        return userRoleStateRef.map { mapToUserRoleDTO(it.state.data) }
    }

    override fun getUserRole(linearId: String): UserRoleDTO
    {
        val userRoleStateRef = rpc.proxy.vaultQuery(UserRoleState::class.java).states
        val userRoleState = userRoleStateRef.find { stateAndRef ->
            stateAndRef.state.data.linearId.toString() == linearId
        } ?: throw NotFoundException("User role not found")
        return mapToUserRoleDTO(userRoleState.state.data)
    }

    override fun createUserRole(request: UserRoleFlowDTO): UserRoleDTO
    {
        val flowReturn = rpc.proxy.startFlowDynamic(
                CreateUserRoleFlow::class.java,
                request.name
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as UserRoleState
        return mapToUserRoleDTO(flowResult)
    }

    override fun saveUserNotificationToken(request: UserNotificationFlowDTO): UserNotificationDTO {
        val flowReturn = rpc.proxy.startFlowDynamic(
                UserNotificationFlow::class.java,
                request.userId,
                request.notificationToken,
                request.issuedAt
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as UserNotificationState
        return mapToUserNotificationDTO(flowResult)
    }
}