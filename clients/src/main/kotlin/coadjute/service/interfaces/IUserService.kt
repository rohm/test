package coadjute.service.interfaces

import coadjute.dto.*

interface IUserService : IService
{
    fun createUser(request: UserRegisterFlowDTO): UserDTO
    fun updateUser(linearId: String, request: UserUpdateFlowDTO): UserDTO
    fun changeUserPassword(linearId: String, request: UserChangePasswordFlowDTO): Boolean
    fun getAllUserRoles(): List<UserRoleDTO>
    fun getUserRole(linearId: String): UserRoleDTO
    fun createUserRole(request: UserRoleFlowDTO): UserRoleDTO
    fun saveUserNotificationToken(request: UserNotificationFlowDTO): UserNotificationDTO
}