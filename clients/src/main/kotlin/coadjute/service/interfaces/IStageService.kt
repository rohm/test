package coadjute.service.interfaces

import coadjute.dto.*

interface IStageService : IService
{
    fun getProjectStagesByProjectId(projectId: String?): List<ProjectStageDTO>
    fun getProjectStages(projectId: String, gateBit: Boolean): List<ProjectStageDTO>
    fun getProjectGates(projectId: String): MutableList<ProjectStageDTO>
    fun createProjectStage(request: ProjectStageCreateFlowDTO): ProjectStageDTO
    fun updateProjectStage(linearId: String, request: ProjectStageUpdateFlowDTO): ProjectStageDTO
    fun getAllProcessStage(): List<ProcessStageDTO>
    fun getProcessStage(linearId: String): ProcessStageDTO
    fun createProcessStage(request: ProcessStageCreateFlowDTO): ProcessStageDTO
    fun updateProcessStage(linearId: String, request: ProcessStageUpdateFlowDTO): ProcessStageDTO
}