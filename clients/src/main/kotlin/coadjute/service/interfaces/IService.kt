package coadjute.service.interfaces

interface IService
{
    var currentUserId: String
    var currentUserOrgId: String
    fun get(linearId : String) : Any
    fun getAll(): Any
    fun setCurrentUserDetails(userId: String?, organizationId: String?)
}