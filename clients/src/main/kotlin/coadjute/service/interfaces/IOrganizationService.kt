package coadjute.service.interfaces

import coadjute.dto.*

interface IOrganizationService : IService
{
    fun getAllOrganizationRoles(): List<OrganizationRoleDTO>
    fun getOrganizationRole(linearId: String): OrganizationRoleDTO
    fun createOrganizationRole(request: OrganizationRoleFlowDTO): OrganizationRoleDTO
    fun createOrganization(request: OrganizationRegisterFlowDTO): OrganizationDTO
    fun updateOrganization(linearId: String, request: OrganizationUpdateFlowDTO): OrganizationDTO
}