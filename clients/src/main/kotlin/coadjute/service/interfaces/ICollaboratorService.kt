package coadjute.service.interfaces

import coadjute.dto.*

interface ICollaboratorService : IService
{
    fun getCollaboratorSelf(projectId: String): CollaboratorDTO
    fun getCollaboratorsByStatus(status: String): List<CollaboratorDTO>
    fun getCollaboratorsByProject(projectId: String): List<CollaboratorDTO>
    fun createCollaborator(request: CollaboratorCreateDTO): CollaboratorDTO
    fun setCollaboratorStatus(linearId: String, request: CollaboratorUpdateDTO): CollaboratorDTO
    fun getAllCollaboratorRoles(): List<CollaboratorRoleDTO>
    fun getCollaboratorRole(linearId: String): CollaboratorRoleDTO
    fun createCollaboratorRole(request: CollaboratorRoleFlowDTO): CollaboratorRoleDTO
    fun getAllCollaboratorInvites(): List<CollaboratorInviteDTO>
    fun getAllCollaboratorInvitesUsingProjectId(projectId: String): List<CollaboratorInviteDTO>
    fun getCollaboratorInvite(linearId: String): CollaboratorInviteDTO
    fun inviteCollaborator(request: CollaboratorInviteFlowDTO): CollaboratorInviteDTO
    fun lookupCollaborators(searchText: String?): List<CollaboratorLookupDTO>
}