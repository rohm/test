package coadjute.service.interfaces

import coadjute.dto.*

interface IContractService : IService
{
    fun createProjectContract(request: ContractFlowDTO): ContractDTO
    fun getAllContractArtifacts(): List<ContractArtifactDTO>
    fun getContractArtifact(linearId: String): ContractArtifactDTO
    fun createContractArtifact(request: ContractArtifactCreateFlowDTO): ContractArtifactDTO
    fun updateContractArtifact(linearId: String, request: ContractArtifactUpdateFlowDTO): ContractArtifactDTO
    fun getAllContractObligations(): List<ContractObligationDTO>
    fun getContractObligations(linearId: String): ContractObligationDTO
    fun createContractObligation(request: ContractObligationCreateFlowDTO): ContractObligationDTO
    fun updateContractObligation(linearId: String, request: ContractObligationUpdateFlowDTO): ContractObligationDTO
}