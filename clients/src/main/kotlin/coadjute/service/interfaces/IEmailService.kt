package coadjute.service.interfaces

interface IEmailService
{
    fun sendEmail(recipientEmail: String, subject: String, text: String)
}