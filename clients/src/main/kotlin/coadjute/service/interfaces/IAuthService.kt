package coadjute.service.interfaces

import coadjute.dto.AuthResultDTO
import coadjute.dto.AuthenticateDTO
import coadjute.dto.SeedDataDTO

interface IAuthService : IService {
    fun authenticate(request : AuthenticateDTO) : AuthResultDTO
    fun seedData(organizationType : String) : SeedDataDTO
}