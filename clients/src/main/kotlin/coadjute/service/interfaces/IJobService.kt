package coadjute.service.interfaces

import coadjute.common.JobLogActions
import coadjute.dto.*

interface IJobService : IService
{
    fun getJobsByStage(projectStageId: String?): List<JobDTO>
    fun getJobsByProject( projectId: String?): List<JobDTO>
    fun createJob(request: JobCreateFlowDTO): JobDTO
    fun updateJob(linearId: String, request: JobUpdateFlowDTO): JobDTO
    fun doJob(linearId: String, request: DoJobDTO): JobDTO
    fun acceptResponsibleJob(linearId: String): JobDTO
    fun declineResponsibleJob(linearId: String): JobDTO
    fun getAllReviewers(): List<ReviewersDTO>
    fun getReviewersByJobId(jobId: String): List<ReviewersDTO>
    fun getReviewer(linearId: String): ReviewersDTO
    fun createReviewer(request: ReviewersCreateFlowDTO): ReviewersDTO
    fun getAllFollowers(): List<FollowerDTO>
    fun getFollowersByJobId(jobId: String): List<FollowerDTO>
    fun getFollower(linearId: String): FollowerDTO
    fun createFollower(request: FollowerCreateFlowDTO): FollowerDTO
    fun getAllSignatories(): List<SignatoriesDTO>
    fun getSignatoriesByJobId(jobId: String): List<SignatoriesDTO>
    fun getSignatories(linearId: String): SignatoriesDTO
    fun createSignatories(request: SignatoriesCreateFlowDTO): SignatoriesDTO
    fun addToJobLog(jobId: String, content: String, action: JobLogActions, splitJobIds : List<String>?,
                    mergeFromJobIds : List<String>?, updateByCollaboratorId : String?, updateByUserId : String?,
                    updatedByFirstName: String?, updatedByMiddleName: String?, updatedByLastName: String?)
    fun getAllJobLog(jobId : String, pageNumber: Int, pageSize : Int): List<JobLogDTO>

}