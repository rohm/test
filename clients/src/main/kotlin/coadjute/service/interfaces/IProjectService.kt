package coadjute.service.interfaces

import coadjute.dto.*

interface IProjectService : IService
{
    fun createProject(request: ProjectRegisterFlowDTO): ProjectDTO
    fun updateProject(linearId: String, request: ProjectUpdateFlowDTO): ProjectDTO
    fun getAllPrograms(): List<ProgramDTO>
    fun filterProjectsByProgram(withProgram: Boolean): List<ProjectDTO>
    fun getProjectsByOrganization(linearId: String): List<ProjectDTO>
    fun getProgram(linearId: String): ProgramDTO
    fun createProgram(request: ProgramFlowDTO): ProgramDTO
    fun setProgramToProject(linearId: String, request: SetProgramToProjectFlowDTO): ProjectDTO
    fun getAllProcess(): List<ProcessDTO>
    fun getProcess(linearId: String): ProcessDTO
    fun createProcess(request: ProcessFlowDTO): ProcessDTO
    fun setProjectStatus(linearId: String, request: ProjectStatusFlowDTO): ProjectDTO
    fun addToProjectLog(projectId : String, content: String, updatedBy: String?)
    fun getAllProjectLog(projectId : String, pageNumber: Int, pageSize : Int): MutableList<ProjectLogDTO>
}