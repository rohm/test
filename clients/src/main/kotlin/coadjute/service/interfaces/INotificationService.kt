package coadjute.service.interfaces

import coadjute.dto.NotificationFlowDTO
import coadjute.dto.NotificationsDTO

interface INotificationService : IService {

    fun generateToken(): Unit
    fun sendMessage(token: String,notificationFlowDto : NotificationFlowDTO): NotificationsDTO
}