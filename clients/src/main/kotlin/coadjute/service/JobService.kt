package coadjute.service

import coadjute.common.AppConstants
import coadjute.common.JobAction
import coadjute.common.JobLogActions
import coadjute.common.appexceptions.NotFoundException
import coadjute.dto.*
import coadjute.flows.job.*
import coadjute.service.interfaces.IJobService
import coadjute.states.collaborator.CollaboratorState
import coadjute.states.job.*
import coadjute.states.project.ProjectState
import coadjute.webserver.NodeRPCConnection
import coadjute.webserver.utilities.FlowHandlerCompletion
import net.corda.core.messaging.vaultQueryBy
import net.corda.core.node.services.Vault
import net.corda.core.node.services.vault.PageSpecification
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.node.services.vault.Sort
import net.corda.core.node.services.vault.SortAttribute
import org.springframework.stereotype.Service
import java.time.Instant

@Service
class JobService (private val rpc: NodeRPCConnection, private val fhc: FlowHandlerCompletion, private val collaboratorService: CollaboratorService): IJobService
{
    override var currentUserId: String = AppConstants.DEFAULT_USER_ID
    override var currentUserOrgId: String = AppConstants.DEFAULT_ORGANIZATION_ID

    override fun setCurrentUserDetails(userId: String?, organizationId: String?)
    {
        currentUserId = userId ?: currentUserId
        currentUserOrgId = organizationId ?: currentUserOrgId
    }

    override fun getAll(): Any
    {
        val jobStateRef = rpc.proxy.vaultQuery(JobState::class.java).states
        return jobStateRef.map { mapToJobDTO(it.state.data) }
    }

    override fun get(linearId: String): Any
    {
        val jobStateRef = rpc.proxy.vaultQuery(JobState::class.java).states
        val jobState = jobStateRef.find { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Job not found")
        return mapToJobDTO(jobState.state.data)
    }

    override fun getJobsByStage(projectStageId: String?): List<JobDTO>
    {
        val jobStateRef = rpc.proxy.vaultQuery(JobState::class.java).states
        val jobStates = jobStateRef.filter { it.state.data.projectStageId == projectStageId }
        return jobStates.map { mapToJobDTO(it.state.data) }
    }

    override fun getJobsByProject(projectId: String?): List<JobDTO>
    {
        val jobStateRef = rpc.proxy.vaultQuery(JobState::class.java).states
        val jobStates = jobStateRef.filter { it.state.data.projectId == projectId }
        return jobStates.map { mapToJobDTO(it.state.data) }
    }

    override fun createJob(request: JobCreateFlowDTO): JobDTO
    {
        if(request.name.isEmpty() || request.name.isBlank())
            throw Exception("Name cannot be empty")

        if(request.status.isEmpty() || request.status.isBlank())
            throw Exception("Status cannot be empty")

        val projectStateRef = rpc.proxy.vaultQuery(ProjectState::class.java).states
        val foundProject = projectStateRef.find { it.state.data.linearId.toString() == request.projectId } ?: throw NotFoundException("Project not found")

        val selfCollab = collaboratorService.getCollaboratorSelf(foundProject.state.data.linearId.toString())

        //ToDo: insert current user Id below '123CURRENTUSER123'
        val flowReturn = rpc.proxy.startFlowDynamic(
                CreateJobFlow::class.java,
                request.name,
                foundProject.state.data.linearId.toString(),
                request.status,
                JobAction.NEW.name,
                request.instruction,
                request.artifactBit,
                null,
                null,
                null,
                request.remarks,
                request.referenceJobId,
                request.projectStageId,
                request.startAt,
                request.endAt,
                request.responsibleCollaboratorId,
                request.responsibleUserId,
                request.responsibleAcceptedAt,
                request.ownerCollaboratorId,
                request.ownerUserId,
                request.ownerAcceptedAt,
                selfCollab.linearId,
                "123CURRENTUSER123"
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as JobState
        val logContent = "Created job ${flowResult.name}"
        this.addToJobLog(flowResult.linearId.toString(), logContent, JobLogActions.CREATE, null, null,  request.ownerCollaboratorId, null, null, null, null)
        return mapToJobDTO(flowResult)
    }

    override fun updateJob(linearId: String, request: JobUpdateFlowDTO): JobDTO
    {
        val jobStateRef = rpc.proxy.vaultQuery(JobState::class.java).states
        val foundJob = jobStateRef.find { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Job not found")

        val flowReturn = rpc.proxy.startFlowDynamic(
                UpdateJobFlow::class.java,
                request.name,
                request.status,
                JobAction.NEW.name,
                request.referenceJobId,
                request.instruction,
                null,
                null,
                null,
                request.remarks,
                request.projectStageId,
                request.startAt,
                request.endAt,
                request.responsibleCollaboratorId,
                request.responsibleUserId,
                foundJob.state.data.responsibleAcceptedAt,
                request.ownerCollaboratorId,
                request.ownerUserId,
                foundJob.state.data.ownerAcceptedAt,
                foundJob.state.data.linearId.toString()
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as JobState
        val logContent = "Update job ${flowResult.name}"
        this.addToJobLog(flowResult.linearId.toString(), logContent, JobLogActions.UPDATE, null, null,  request.ownerCollaboratorId, null, null, null, null)
        return mapToJobDTO(flowResult)
    }

    override fun getAllReviewers(): List<ReviewersDTO>
    {
        val reviewersStateRef = rpc.proxy.vaultQuery(ReviewersState::class.java).states
        return reviewersStateRef.map { mapToReviewersDTO(it.state.data) }
    }

    override fun getReviewersByJobId(jobId: String): List<ReviewersDTO>
    {
        val reviewersStateRef = rpc.proxy.vaultQuery(ReviewersState::class.java).states
        val reviewersState = reviewersStateRef.filter { it.state.data.jobId == jobId }
        return reviewersState.map { mapToReviewersDTO(it.state.data) }
    }

    override fun getReviewer(linearId: String): ReviewersDTO
    {
        val reviewersStateRef = rpc.proxy.vaultQuery(ReviewersState::class.java).states
        val reviewersState = reviewersStateRef.find { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Reviewer not found")
        return mapToReviewersDTO(reviewersState.state.data)
    }

    override fun createReviewer(request: ReviewersCreateFlowDTO): ReviewersDTO
    {
        val jobStateRef = rpc.proxy.vaultQuery(JobState::class.java).states
        val foundJob = jobStateRef.find { it.state.data.linearId.toString() == request.jobId } ?: throw NotFoundException("Job not found")

        val collaboratorStateRef = rpc.proxy.vaultQuery(CollaboratorState::class.java).states
        val foundCollaborator = collaboratorStateRef.find { it.state.data.linearId.toString() == request.collaboratorId } ?: throw NotFoundException("Collaborator not found")

        val flowReturn = rpc.proxy.startFlowDynamic(
                CreateReviewersFlow::class.java,
                foundJob.state.data.linearId.toString(),
                foundCollaborator.state.data.linearId.toString(),
                request.userId
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as ReviewersState
        val logContent = "Assign collaborator ${foundCollaborator.state.data.name} as a reviewer to job ${foundJob.state.data.name}"
        this.addToJobLog(foundJob.state.data.linearId.toString(), logContent, JobLogActions.UPDATE, null, null,
                foundCollaborator.state.data.linearId.toString(), null, null, null, null)
        return mapToReviewersDTO(flowResult)
    }

    override fun getAllFollowers(): List<FollowerDTO>
    {
        val followerStateRef = rpc.proxy.vaultQuery(FollowerState::class.java).states
        return followerStateRef.map { mapToFollowerDTO(it.state.data) }
    }

    override fun getFollowersByJobId(jobId: String): List<FollowerDTO>
    {
        val followersStateRef = rpc.proxy.vaultQuery(FollowerState::class.java).states
        val followersState = followersStateRef.filter { it.state.data.jobId == jobId }
        return followersState.map { mapToFollowerDTO(it.state.data) }
    }

    override fun getFollower(linearId: String): FollowerDTO
    {
        val followerStateRef = rpc.proxy.vaultQuery(FollowerState::class.java).states
        val followerState = followerStateRef.find { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Follower not found")
        return mapToFollowerDTO(followerState.state.data)
    }

    override fun createFollower(request: FollowerCreateFlowDTO): FollowerDTO
    {
        val jobStateRef = rpc.proxy.vaultQuery(JobState::class.java).states
        val foundJob = jobStateRef.find { it.state.data.linearId.toString() == request.jobId } ?: throw NotFoundException("Job not found")

        val collaboratorStateRef = rpc.proxy.vaultQuery(CollaboratorState::class.java).states
        val foundCollaborator = collaboratorStateRef.find { it.state.data.linearId.toString() == request.collaboratorId } ?: throw NotFoundException("Collaborator not found")

        val flowReturn = rpc.proxy.startFlowDynamic(
                CreateFollowerFlow::class.java,
                foundJob.state.data.linearId.toString(),
                foundCollaborator.state.data.linearId.toString(),
                request.userId
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as FollowerState
        val logContent = "Assign collaborator ${foundCollaborator.state.data.name} as a follower to job ${foundJob.state.data.name}"
        this.addToJobLog(foundJob.state.data.linearId.toString(), logContent, JobLogActions.UPDATE, null, null,
                foundCollaborator.state.data.linearId.toString(), null, null, null, null)
        return mapToFollowerDTO(flowResult)
    }

    override fun getAllSignatories(): List<SignatoriesDTO>
    {
        val signatoriesStateRef = rpc.proxy.vaultQuery(SignatoriesState::class.java).states
        return signatoriesStateRef.map { mapToSignatoriesDTO(it.state.data) }
    }

    override fun getSignatoriesByJobId(jobId: String): List<SignatoriesDTO>
    {
        val signatoriesStateRef = rpc.proxy.vaultQuery(SignatoriesState::class.java).states
        val signatoriesState = signatoriesStateRef.filter { it.state.data.jobId == jobId }
        return signatoriesState.map { mapToSignatoriesDTO(it.state.data) }
    }

    override fun getSignatories(linearId: String): SignatoriesDTO
    {
        val signatoriesStateRef = rpc.proxy.vaultQuery(SignatoriesState::class.java).states
        val signatoriesState = signatoriesStateRef.find { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Signatories not found")
        return mapToSignatoriesDTO(signatoriesState.state.data)
    }

    override fun createSignatories(request: SignatoriesCreateFlowDTO): SignatoriesDTO
    {
        val jobStateRef = rpc.proxy.vaultQuery(JobState::class.java).states
        val foundJob = jobStateRef.find { it.state.data.linearId.toString() == request.jobId } ?: throw NotFoundException("Job not found")

        val collaboratorStateRef = rpc.proxy.vaultQuery(CollaboratorState::class.java).states
        val foundCollaborator = collaboratorStateRef.find { it.state.data.linearId.toString() == request.collaboratorId } ?: throw NotFoundException("Collaborator not found")

        val flowReturn = rpc.proxy.startFlowDynamic(
                CreateSignatoriesFlow::class.java,
                foundJob.state.data.linearId.toString(),
                foundCollaborator.state.data.linearId.toString(),
                request.userId
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as SignatoriesState
        val logContent = "Assign collaborator ${foundCollaborator.state.data.name} as a signatory to job ${foundJob.state.data.name}"
        this.addToJobLog(foundJob.state.data.linearId.toString(), logContent, JobLogActions.UPDATE, null, null,
                foundCollaborator.state.data.linearId.toString(), null, null, null, null)
        return mapToSignatoriesDTO(flowResult)
    }

    override fun addToJobLog(jobId: String, content: String, action: JobLogActions, splitJobIds: List<String>?, mergeFromJobIds: List<String>?,
                             updateByCollaboratorId: String?, updateByUserId: String?, updatedByFirstName: String?,
                             updatedByMiddleName: String?, updatedByLastName: String?) {
        val flowReturn = rpc.proxy.startFlowDynamic(
                CreateOrUpdateJobLogFlow::class.java,
                jobId,
                content,
                action.toString(),
                splitJobIds,
                mergeFromJobIds,
                updateByCollaboratorId,
                updateByUserId,
                updatedByFirstName,
                updatedByMiddleName,
                updatedByLastName
        )
        fhc.flowHandlerCompletion(flowReturn)
    }

    override fun getAllJobLog(jobId: String, pageNumber: Int, pageSize: Int): List<JobLogDTO>
    {
        val jobLogStateRef = rpc.proxy.vaultQuery(JobLogState::class.java).states
        val found = jobLogStateRef.firstOrNull{ it.state.data.jobId == jobId } ?: throw NotFoundException("Job not found")

        val linearIdCriteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(found.state.data.linearId), status = Vault.StateStatus.ALL)
        val sortColumn = Sort.SortColumn(SortAttribute.Standard(Sort.VaultStateAttribute.RECORDED_TIME), Sort.Direction.DESC)
        val sorting = Sort(listOf(sortColumn))
        val logs = rpc.proxy.vaultQueryBy<JobLogState>(linearIdCriteria, paging = PageSpecification(pageNumber = pageNumber, pageSize = pageSize), sorting = sorting)
        return logs.states.map { mapToJobLogDTO(it.state.data) }
    }

    override fun doJob(linearId: String, request: DoJobDTO): JobDTO
    {
        val jobStateRef = rpc.proxy.vaultQuery(JobState::class.java).states
        val foundJob = jobStateRef.find { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Job not found")

        val collaboratorStateRef = rpc.proxy.vaultQuery(CollaboratorState::class.java).states
        val foundCollaborator = collaboratorStateRef.find { it.state.data.linearId.toString() == request.responsibleCollaboratorId } ?: throw NotFoundException("Collaborator not found")

        // Check if the user id will change, accepted at will be null
        val acceptedAt : Instant?
        acceptedAt = if (foundJob.state.data.responsibleUserId != request.responsibleUserId)
            null
        else
            foundJob.state.data.responsibleAcceptedAt

        val flowReturn = rpc.proxy.startFlowDynamic(
                UpdateJobFlow::class.java,
                foundJob.state.data.name,
                foundJob.state.data.status,
                JobAction.DO.name,
                foundJob.state.data.referenceJobId,
                request.instruction,
                null,
                null,
                null,
                foundJob.state.data.remarks,
                foundJob.state.data.projectStageId,
                foundJob.state.data.startAt,
                foundJob.state.data.endAt,
                request.responsibleCollaboratorId,
                request.responsibleUserId,
                acceptedAt,
                foundJob.state.data.ownerCollaboratorId,
                foundJob.state.data.ownerUserId,
                foundJob.state.data.ownerAcceptedAt,
                foundJob.state.data.linearId.toString()
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as JobState
        val logContent = "Assign ${foundCollaborator.state.data.name} to job ${flowResult.name}"
        this.addToJobLog(flowResult.linearId.toString(), logContent, JobLogActions.DO, null, null,  foundJob.state.data.ownerCollaboratorId, null, null, null, null)
        return mapToJobDTO(flowResult)
    }

    override fun acceptResponsibleJob(linearId: String): JobDTO
    {
        val jobStateRef = rpc.proxy.vaultQuery(JobState::class.java).states
        val foundJob = jobStateRef.find { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Job not found")

        val collaboratorStateRef = rpc.proxy.vaultQuery(CollaboratorState::class.java).states
        val foundCollaborator = collaboratorStateRef.find { it.state.data.linearId.toString() == foundJob.state.data.responsibleCollaboratorId } ?: throw NotFoundException("Collaborator not found")

        val flowReturn = rpc.proxy.startFlowDynamic(
                AcceptResponsibleFlow::class.java,
                foundJob.state.data.linearId.toString()
        )
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as JobState
        val logContent = "${foundCollaborator.state.data.name} accepted job ${flowResult.name} as doer "
        this.addToJobLog(flowResult.linearId.toString(), logContent, JobLogActions.DO, null, null,  foundJob.state.data.ownerCollaboratorId, null, null, null, null)
        return mapToJobDTO(flowResult)
    }

    override fun declineResponsibleJob(linearId: String): JobDTO
    {
        val jobStateRef = rpc.proxy.vaultQuery(JobState::class.java).states
        val foundJob = jobStateRef.find { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Job not found")
        val collaboratorStateRef = rpc.proxy.vaultQuery(CollaboratorState::class.java).states
        val foundCollaborator = collaboratorStateRef.find { it.state.data.linearId.toString() == foundJob.state.data.responsibleCollaboratorId } ?: throw NotFoundException("Collaborator not found")

        val flowReturn = rpc.proxy.startFlowDynamic(
                DeclineResponsibleFlow::class.java,
                foundJob.state.data.linearId.toString()
        )
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as JobState
        val logContent = "${foundCollaborator.state.data.name} declined job ${flowResult.name}"
        this.addToJobLog(flowResult.linearId.toString(), logContent, JobLogActions.DO, null, null,  foundJob.state.data.ownerCollaboratorId, null, null, null, null)
        return mapToJobDTO(flowResult)
    }
}