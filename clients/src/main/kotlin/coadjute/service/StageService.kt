package coadjute.service

import coadjute.common.AppConstants
import coadjute.common.appexceptions.NotFoundException
import coadjute.dto.*
import coadjute.flows.project.CreateProcessStageFlow
import coadjute.flows.project.CreateProjectStageFlow
import coadjute.flows.project.UpdateProcessStageFlow
import coadjute.flows.project.UpdateProjectStageFlow
import coadjute.service.interfaces.IStageService
import coadjute.states.project.ProcessStageState
import coadjute.states.project.ProcessState
import coadjute.states.project.ProjectStageState
import coadjute.states.project.ProjectState
import coadjute.webserver.NodeRPCConnection
import coadjute.webserver.utilities.FlowHandlerCompletion
import org.springframework.stereotype.Service

@Service
class StageService (private val rpc: NodeRPCConnection, private val fhc: FlowHandlerCompletion): IStageService
{
    override var currentUserId: String = AppConstants.DEFAULT_USER_ID
    override var currentUserOrgId: String = AppConstants.DEFAULT_ORGANIZATION_ID

    override fun setCurrentUserDetails(userId: String?, organizationId: String?)
    {
        currentUserId = userId ?: currentUserId
        currentUserOrgId = organizationId ?: currentUserOrgId
    }

    override fun getAll(): List<ProjectStageDTO>
    {
        val projectStageStateRef = rpc.proxy.vaultQuery(ProjectStageState::class.java).states
        return projectStageStateRef.map { mapToProjectStageDTO(it.state.data, null) }
    }

    override fun get(linearId: String): ProjectStageDTO
    {
        val projectStageStateRef = rpc.proxy.vaultQuery(ProjectStageState::class.java).states
        val projectStageState = projectStageStateRef.find { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Project stage not found")
        return mapToProjectStageDTO(projectStageState.state.data, null)
    }

    override fun getProjectStagesByProjectId(projectId: String?): MutableList<ProjectStageDTO>
    {
        val projectStageStateRef = rpc.proxy.vaultQuery(ProjectStageState::class.java).states
        val pStagesWithoutGates = projectStageStateRef.filter { it.state.data.projectId == projectId && !it.state.data.gateBit && it.state.data.gateStageId == null}
        val pGates = projectStageStateRef.filter { it.state.data.projectId == projectId && it.state.data.gateBit && it.state.data.gateStageId == null}
        val pStageWithGates = projectStageStateRef.filter { it.state.data.projectId == projectId && !it.state.data.gateBit && it.state.data.gateStageId != null}
        val list = mutableListOf<ProjectStageDTO>()

        pGates.forEach { gate ->
            val gateId = gate.state.data.linearId.toString()
            val stages = pStageWithGates.filter { it.state.data.gateStageId == gateId }
            stages.sortedBy { it.state.data.order }
            list.add(mapToProjectStageDTO(gate.state.data, stages))
        }

        pStagesWithoutGates.forEach{ list.add(mapToProjectStageDTO(it.state.data, null))}
        list.sortBy { it.order }
        return list
    }

    override fun getProjectStages(projectId: String, gateBit: Boolean): List<ProjectStageDTO>
    {
        val projectStageStateRef = rpc.proxy.vaultQuery(ProjectStageState::class.java).states
        val projectStageState = projectStageStateRef.filter { it.state.data.projectId == projectId && it.state.data.gateBit == gateBit }
        return projectStageState.map { mapToProjectStageDTO(it.state.data, null) }
    }

    override fun getProjectGates(projectId: String): MutableList<ProjectStageDTO>
    {
        val projectStageStateRef = rpc.proxy.vaultQuery(ProjectStageState::class.java).states
        val gates = projectStageStateRef.filter { it.state.data.projectId == projectId && it.state.data.gateBit }
        val list = mutableListOf<ProjectStageDTO>()
        gates.forEach { gate ->
            val gateId = gate.state.data.linearId.toString()
            val stages = projectStageStateRef.filter { it.state.data.gateStageId == gateId && !it.state.data.gateBit }
            stages.sortedBy { it.state.data.order }
            list.add(mapToProjectStageDTO(gate.state.data, stages))
        }
        return list
    }

    override fun createProjectStage(request: ProjectStageCreateFlowDTO): ProjectStageDTO
    {
        if(request.projectId.isBlank() || request.projectId.isEmpty())
            throw Exception("Project id cannot be empty")

        if(request.name.isBlank() || request.name.isEmpty())
            throw Exception("Name cannot be empty")

        if(request.gateBit && request.gateStageId != null)
            throw Exception("Cannot assign a gate to another gate")

        val projectStateRef = rpc.proxy.vaultQuery(ProjectState::class.java).states
        val foundProject = projectStateRef.find { it.state.data.linearId.toString() == request.projectId } ?: throw NotFoundException("Project not found")

        var foundProcessStageId: String? = null
        if(request.gateStageId != null) {
            val processStageStateRef = rpc.proxy.vaultQuery(ProcessStageState::class.java).states
            val found = processStageStateRef.find { it.state.data.linearId.toString() == request.processStageId } ?: throw NotFoundException("Process Stage not found")
            foundProcessStageId = found.state.data.linearId.toString()
        }

        var foundGateStageId: String? = null
        if(request.gateStageId != null) {
            val gateStageStateRef = rpc.proxy.vaultQuery(ProjectStageState::class.java).states
            val found = gateStageStateRef.find { it.state.data.linearId.toString() == request.gateStageId } ?: throw NotFoundException("Gate Stage not found")
            foundGateStageId = found.state.data.linearId.toString()
        }

        val flowReturn = rpc.proxy.startFlowDynamic(
                CreateProjectStageFlow::class.java,
                foundProject.state.data.linearId.toString(),
                foundProcessStageId,
                request.name,
                request.gateBit,
                foundGateStageId,
                request.description,
                request.order
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as ProjectStageState
        return mapToProjectStageDTO(flowResult, null)
    }

    override fun updateProjectStage(linearId: String, request: ProjectStageUpdateFlowDTO): ProjectStageDTO
    {
        if(request.name.isBlank() || request.name.isEmpty())
            throw Exception("Name cannot be empty")

        val projectStageStateRef = rpc.proxy.vaultQuery(ProjectStageState::class.java).states
        val foundProjectStage = projectStageStateRef.find { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Project stage not found")

        val flowReturn = rpc.proxy.startFlowDynamic(
                UpdateProjectStageFlow::class.java,
                request.name,
                request.description,
                request.order,
                foundProjectStage.state.data.linearId.toString()
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as ProjectStageState
        return mapToProjectStageDTO(flowResult, null)
    }

    override fun getAllProcessStage(): List<ProcessStageDTO>
    {
        val processStageStateRef = rpc.proxy.vaultQuery(ProcessStageState::class.java).states
        return processStageStateRef.map { mapToProcessStageDTO(it.state.data, null) }
    }

    override fun getProcessStage(linearId: String): ProcessStageDTO
    {
        val processStageStateRef = rpc.proxy.vaultQuery(ProcessStageState::class.java).states
        val processStageState = processStageStateRef.find { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Process stage not found")
        return mapToProcessStageDTO(processStageState.state.data, null)
    }

    override fun createProcessStage(request: ProcessStageCreateFlowDTO): ProcessStageDTO
    {
        if(request.projectId.isEmpty() || request.projectId.isBlank())
            throw Exception("Project id cannot be empty")

        if(request.processId.isEmpty() || request.processId.isBlank())
            throw Exception("Process id cannot be empty")

        if(request.name.isEmpty() || request.name.isBlank())
            throw Exception("Name cannot be empty")

        val projectStateRef = rpc.proxy.vaultQuery(ProjectState::class.java).states
        val foundProject = projectStateRef.find { it.state.data.linearId.toString() == request.projectId } ?: throw NotFoundException("Project not found")

        val processStateRef = rpc.proxy.vaultQuery(ProcessState::class.java).states
        val foundProcess = processStateRef.find { it.state.data.linearId.toString() == request.processId } ?: throw NotFoundException("Process not found")

        var foundGateStageId: String? = null
        if(request.gateStageId != null) {
            val gateStageStateRef = rpc.proxy.vaultQuery(ProcessStageState::class.java).states
            val found = gateStageStateRef.find { it.state.data.linearId.toString() == request.gateStageId } ?: throw NotFoundException("Gate Stage not found")
            foundGateStageId = found.state.data.linearId.toString()
        }

        val flowReturn = rpc.proxy.startFlowDynamic(
                CreateProcessStageFlow::class.java,
                foundProject.state.data.linearId.toString(),
                foundProcess.state.data.linearId.toString(),
                request.name,
                request.gateBit,
                foundGateStageId,
                request.description,
                request.order
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as ProcessStageState
        return mapToProcessStageDTO(flowResult, null)
    }

    override fun updateProcessStage(linearId: String, request: ProcessStageUpdateFlowDTO): ProcessStageDTO
    {
        if(request.name.isEmpty() || request.name.isBlank())
            throw Exception("Name cannot be empty")

        val processStageStateRef = rpc.proxy.vaultQuery(ProcessStageState::class.java).states
        val foundProcessStage = processStageStateRef.find { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Process stage not found")

        val flowReturn = rpc.proxy.startFlowDynamic(
                UpdateProcessStageFlow::class.java,
                request.name,
                request.description,
                request.order,
                foundProcessStage.state.data.linearId.toString()
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as ProcessStageState
        return mapToProcessStageDTO(flowResult, null)
    }
}