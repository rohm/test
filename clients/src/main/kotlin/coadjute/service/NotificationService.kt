package coadjute.service

import coadjute.common.AppConstants
import coadjute.common.appexceptions.NotFoundException
import coadjute.dto.NotificationFlowDTO
import coadjute.dto.NotificationsDTO
import coadjute.dto.mapToNotificationsDTO
import coadjute.flows.project.NotificationFlow
import coadjute.service.interfaces.INotificationService
import coadjute.states.job.JobState
import coadjute.states.project.NotificationState
import coadjute.states.project.ProjectState
import coadjute.webserver.NodeRPCConnection
import coadjute.webserver.utilities.FlowHandlerCompletion
import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions

import java.io.FileInputStream

import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.Message
import org.springframework.stereotype.Service

@Service
class NotificationService(private val rpc: NodeRPCConnection, private val fhc: FlowHandlerCompletion) : INotificationService
{
    override var currentUserId: String = AppConstants.DEFAULT_USER_ID
    override var currentUserOrgId: String = AppConstants.DEFAULT_ORGANIZATION_ID

    override fun setCurrentUserDetails(userId: String?, organizationId: String?)
    {
        currentUserId = userId ?: currentUserId
        currentUserOrgId = organizationId ?: currentUserOrgId
    }

    override fun get(linearId: String): Any {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAll(): Any {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    //Send message for push notification
    override fun sendMessage(token: String, request : NotificationFlowDTO) : NotificationsDTO {
        var jobId = ""
        if(!request.jobId.isNullOrEmpty()){
            val jobStateRef = rpc.proxy.vaultQuery(JobState::class.java).states
            val foundJob = jobStateRef.find { it.state.data.linearId.toString() == request.jobId } ?: throw NotFoundException("Job not found")
            jobId = foundJob.state.data.linearId.toString()
        }

        var projectId = ""
        if(!request.projectId.isNullOrEmpty()){
            val projectStateRef = rpc.proxy.vaultQuery(ProjectState::class.java).states
            val  foundProject = projectStateRef.find { it.state.data.linearId.toString() == request.projectId } ?: throw NotFoundException("Project not found")
            projectId = foundProject.state.data.linearId.toString()
        }

        /*Start of sending message*/
        FirebaseApp.initializeApp();
        val message = Message.builder()
                .putData("name", request.name)
                .putData("content", request.content)
                .setToken(token)
                .build()
        FirebaseMessaging.getInstance().send(message)
        /*End of sending message*/

        val flowReturn = rpc.proxy.startFlowDynamic(
                NotificationFlow::class.java,
                request.name,
                request.content,
                request.recipientCollaboratorId,
                request.recipientUserId,
                request.sentAt,
                request.senderCollaboratorId,
                request.senderUserId,
                projectId,
                jobId
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as NotificationState

        return mapToNotificationsDTO(flowResult)
    }

    //For Local testing purposes only
    override fun generateToken():Unit {

        val refreshToken = FileInputStream("C:/Users/jonathan.panganiban/Downloads/coadjute-qc-firebase-adminsdk-kfyy0-710da7df59.json")

        val options = FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(refreshToken))
                .setDatabaseUrl("https://coadjute-qc.firebaseio.com/")
                .build()

        FirebaseApp.initializeApp(options)
    }

}