package coadjute.service

import coadjute.common.AppConstants
import coadjute.common.AppConstants.Companion.DEFAULT_ORGANIZATION_ID
import coadjute.common.AppConstants.Companion.DEFAULT_USER_ID
import coadjute.common.appexceptions.UnauthorizedException
import coadjute.common.passwordencryption.PasswordEncryption
import coadjute.common.passwordmanager.PasswordManager
import coadjute.dto.*
import coadjute.flows.collaborator.CreateCollaboratorRoleFlow
import coadjute.flows.collaborator.CreateSelfCollaboratorFlow
import coadjute.flows.organization.CreateOrganizationFlow
import coadjute.flows.organization.CreateOrganizationRoleFlow
import coadjute.flows.project.CreateProcessFlow
import coadjute.flows.project.CreateProjectContractFlow
import coadjute.flows.project.CreateProjectFlow
import coadjute.flows.project.CreateProjectStageFlow
import coadjute.flows.user.CreateUserFlow
import coadjute.flows.user.CreateUserRoleFlow
import coadjute.service.interfaces.IAuthService
import coadjute.states.collaborator.CollaboratorRoleState
import coadjute.states.collaborator.CollaboratorState
import coadjute.states.organization.OrganizationRoleState
import coadjute.states.organization.OrganizationState
import coadjute.states.project.ProcessState
import coadjute.states.project.ProjectContractState
import coadjute.states.project.ProjectStageState
import coadjute.states.project.ProjectState
import coadjute.states.user.UserRoleState
import coadjute.states.user.UserState
import coadjute.webserver.NodeRPCConnection
import coadjute.webserver.utilities.FlowHandlerCompletion
import org.springframework.stereotype.Service

@Service
class AuthService(private val rpc: NodeRPCConnection, private val fhc: FlowHandlerCompletion) : IAuthService
{
    override var currentUserId: String = DEFAULT_USER_ID
    override var currentUserOrgId: String = DEFAULT_ORGANIZATION_ID

    override fun setCurrentUserDetails(userId: String?, organizationId: String?)
    {
        currentUserId = userId ?: currentUserId
        currentUserOrgId = organizationId ?: currentUserOrgId
    }

    override fun authenticate(request: AuthenticateDTO): AuthResultDTO
    {
        if(request.email.isBlank() || request.email.isEmpty())
            throw Exception("Email cannot be empty")

        val nEmail = request.email.toLowerCase().trim()
        val regex = Regex(AppConstants.REGEX_EMAIL, RegexOption.IGNORE_CASE)
        if(!regex.matches(nEmail))
            throw Exception("Must use a valid email address")

        if(request.password.isBlank() || request.password.isEmpty())
            throw Exception("Password cannot be empty")

        val userStateRef = rpc.proxy.vaultQuery(UserState::class.java).states
        val foundUser = userStateRef.find { it.state.data.normalizedEmail == nEmail } ?: throw UnauthorizedException("Email or password is incorrect")

        val organizationStateRef = rpc.proxy.vaultQuery(OrganizationState::class.java).states
        val foundOrganization = organizationStateRef.find { it.state.data.linearId.toString() == foundUser.state.data.organizationId } ?: throw UnauthorizedException("Cannot find organization")

        val encryptedPassword = PasswordEncryption(foundUser.state.data.salt, foundOrganization.state.data.encryptKey).encryption.encrypt(request.password)

        if(foundUser.state.data.password != encryptedPassword)
            throw UnauthorizedException("Email or password is incorrect")

        var isNeedsProfileSetup = false

        if(foundOrganization.state.data.addressLine1 == null || foundOrganization.state.data.country == null){
            isNeedsProfileSetup = true
        }

        return AuthResultDTO(
                userLinearId = foundUser.state.data.linearId.toString(),
                firstName = foundUser.state.data.firstName,
                middleName = foundUser.state.data.middleName.orEmpty(),
                lastName = foundUser.state.data.lastName,
                orgLinearId = foundUser.state.data.organizationId,
                orgName = foundOrganization.state.data.name,
                accessToken = "",
                tokenExpiresAt = "",
                needsProfileSetupBit = isNeedsProfileSetup
        )
    }

    override fun seedData(organizationType : String) : SeedDataDTO
    {
        var orgName = "Client"
        when(organizationType.toUpperCase().trim()){
            "PD" -> orgName = "PrincipalDesigner"
            "PC" -> orgName = "PrincipalContractor"
            "SC" -> orgName = "SubContractor"
            "R" -> orgName = "Regulator"
        }

        val orgRoleflowReturn = rpc.proxy.startFlowDynamic(
                CreateOrganizationRoleFlow::class.java,
                orgName
        )
        fhc.flowHandlerCompletion(orgRoleflowReturn)
        val orgRoleflowResult = orgRoleflowReturn.returnValue.get().coreTransaction.outputStates.first() as OrganizationRoleState

        val encryptKey = PasswordManager().generatePassword(true, isWithUppercase = true, isWithNumbers = true, isWithSpecial = false, length = 32)
        val orgflowReturn = rpc.proxy.startFlowDynamic(
                CreateOrganizationFlow::class.java,
                orgName,
                "$orgName@coadjute.com",
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                orgRoleflowResult.linearId.toString(),
                encryptKey
        )
        fhc.flowHandlerCompletion(orgflowReturn)
        val orgflowResult = orgflowReturn.returnValue.get().coreTransaction.outputStates.first() as OrganizationState

        val processRIBAflowReturn = rpc.proxy.startFlowDynamic(
                CreateProcessFlow::class.java,
                "RIBA"
        )
        fhc.flowHandlerCompletion(processRIBAflowReturn)
        val processflowResult = processRIBAflowReturn.returnValue.get().coreTransaction.outputStates.first() as ProcessState

        val processJCAflowReturn = rpc.proxy.startFlowDynamic(
                CreateProcessFlow::class.java,
                "JCA"
        )
        fhc.flowHandlerCompletion(processJCAflowReturn)

        val contractflowReturn = rpc.proxy.startFlowDynamic(
                CreateProjectContractFlow::class.java,
                "JCT"
        )
        fhc.flowHandlerCompletion(contractflowReturn)
        val contractflowResult = contractflowReturn.returnValue.get().coreTransaction.outputStates.first() as ProjectContractState

        val userRoleflowReturn = rpc.proxy.startFlowDynamic(
                CreateUserRoleFlow::class.java,
                "Administrator"
        )
        fhc.flowHandlerCompletion(userRoleflowReturn)
        val userRoleflowResult = userRoleflowReturn.returnValue.get().coreTransaction.outputStates.first() as UserRoleState

        val salt = PasswordManager().generatePassword(true, isWithUppercase = true, isWithNumbers = true, isWithSpecial = false, length = 32)
        val key = orgflowResult.encryptKey
        val encryptedPassword = PasswordEncryption(salt,key).encryption.encrypt("test123")

        val userflowReturn = rpc.proxy.startFlowDynamic(
                CreateUserFlow::class.java,
                orgName,
                null,
                "Administrator",
                "admin@coadjute.com",
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                orgflowResult.linearId.toString(),
                userRoleflowResult.linearId.toString(),
                null,
                encryptedPassword,
                salt
        )
        fhc.flowHandlerCompletion(userflowReturn)

        val projectflowReturn = rpc.proxy.startFlowDynamic(
                CreateProjectFlow::class.java,
                "Project A",
                orgflowResult.linearId.toString(),
                null,
                null,
                null,
                null,
                null,
                "CONSTRUCTION",
                contractflowResult.linearId.toString(),
                processflowResult.linearId.toString(),
                null,
                "ONGOING"
        )
        fhc.flowHandlerCompletion(projectflowReturn)
        val projectflowResult = projectflowReturn.returnValue.get().coreTransaction.outputStates.first() as ProjectState

        val clientCBRflowReturn = rpc.proxy.startFlowDynamic(
            CreateCollaboratorRoleFlow::class.java,
            "Client"
        )
        fhc.flowHandlerCompletion(clientCBRflowReturn)
        val clientCBRflowResult = clientCBRflowReturn.returnValue.get().coreTransaction.outputStates.first() as CollaboratorRoleState

        val pdCBRflowReturn = rpc.proxy.startFlowDynamic(
                CreateCollaboratorRoleFlow::class.java,
                "Principal Designer"
        )
        fhc.flowHandlerCompletion(pdCBRflowReturn)

        val pcCBRflowReturn = rpc.proxy.startFlowDynamic(
                CreateCollaboratorRoleFlow::class.java,
                "Principal Contractor"
        )
        fhc.flowHandlerCompletion(pcCBRflowReturn)

        val scCBRflowReturn = rpc.proxy.startFlowDynamic(
                CreateCollaboratorRoleFlow::class.java,
                "Sub Contractor"
        )
        fhc.flowHandlerCompletion(scCBRflowReturn)

        val rCBRflowReturn = rpc.proxy.startFlowDynamic(
                CreateCollaboratorRoleFlow::class.java,
                "Regulator"
        )
        fhc.flowHandlerCompletion(rCBRflowReturn)

        val sCBflowReturn = rpc.proxy.startFlowDynamic(
                CreateSelfCollaboratorFlow::class.java,
                orgName,
                projectflowResult.linearId.toString(),
                clientCBRflowResult.linearId.toString()
        )
        fhc.flowHandlerCompletion(sCBflowReturn)

        val pStage0flowReturn = rpc.proxy.startFlowDynamic(
                CreateProjectStageFlow::class.java,
                projectflowResult.linearId.toString(),
                null,
                "Stage 0",
                false,
                null,
                "Stage 0 created on seed",
                0
        )
        fhc.flowHandlerCompletion(pStage0flowReturn)
        val pStage5flowReturn = rpc.proxy.startFlowDynamic(
                CreateProjectStageFlow::class.java,
                projectflowResult.linearId.toString(),
                null,
                "Stage 5",
                false,
                null,
                "Stage 5 created on seed",
                5
        )
        fhc.flowHandlerCompletion(pStage5flowReturn)

        val pGateflowReturn = rpc.proxy.startFlowDynamic(
                CreateProjectStageFlow::class.java,
                projectflowResult.linearId.toString(),
                null,
                "Gate A",
                true,
                null,
                "Gate A created on seed",
                1
        )
        fhc.flowHandlerCompletion(pGateflowReturn)
        val pGateFlowResult = pGateflowReturn.returnValue.get().coreTransaction.outputStates.first() as ProjectStageState
        val pStage1flowReturn = rpc.proxy.startFlowDynamic(
                CreateProjectStageFlow::class.java,
                projectflowResult.linearId.toString(),
                null,
                "Stage 1",
                false,
                pGateFlowResult.linearId.toString(),
                "Stage 1 created on seed",
                2
        )
        fhc.flowHandlerCompletion(pStage1flowReturn)
        val pStage2flowReturn = rpc.proxy.startFlowDynamic(
                CreateProjectStageFlow::class.java,
                projectflowResult.linearId.toString(),
                null,
                "Stage 2",
                false,
                pGateFlowResult.linearId.toString(),
                "Stage 2 created on seed",
                3
        )
        fhc.flowHandlerCompletion(pStage2flowReturn)


        val processesStateRef = rpc.proxy.vaultQuery(ProcessState::class.java).states
        val contractStateRef = rpc.proxy.vaultQuery(ProjectContractState::class.java).states
        val usersStateRef = rpc.proxy.vaultQuery(UserState::class.java).states
        val projectStateRef = rpc.proxy.vaultQuery(ProjectState::class.java).states
        val collaboratorsStateRef = rpc.proxy.vaultQuery(CollaboratorState::class.java).states
        val stagesStateRef = rpc.proxy.vaultQuery(ProjectStageState::class.java).states

        return SeedDataDTO(
                organization = mapToOrganizationDTO(orgflowResult),
                organizationRole = mapToOrganizationRoleDTO(orgRoleflowResult),
                processes = processesStateRef.map { mapToProcessDTO(it.state.data) },
                contracts = contractStateRef.map { mapToContractDTO( it.state.data) },
                users = usersStateRef.map { mapToUserDTO(it.state.data) },
                projects = projectStateRef.map { mapToProjectDTO(it.state.data) },
                collaborators = collaboratorsStateRef.map { mapToCollaboratorDTO(it.state.data) },
                stages = stagesStateRef.map { mapToProjectStageDTO(it.state.data, null)}
        )

    }

    override fun get(linearId: String): Any {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAll(): Any {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}