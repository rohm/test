package coadjute.service

import coadjute.common.AppConstants
import coadjute.common.CollaboratorRoles
import coadjute.common.appexceptions.NotFoundException
import coadjute.dto.*
import coadjute.flows.collaborator.CreateCollaboratorFlow
import coadjute.flows.collaborator.CreateCollaboratorRoleFlow
import coadjute.flows.collaborator.CreateSelfCollaboratorFlow
import coadjute.flows.project.*
import coadjute.service.interfaces.IProjectService
import coadjute.states.collaborator.CollaboratorRoleState
import coadjute.states.collaborator.CollaboratorState
import coadjute.states.organization.OrganizationState
import coadjute.states.project.*
import coadjute.states.user.UserState
import coadjute.webserver.NodeRPCConnection
import coadjute.webserver.utilities.FlowHandlerCompletion
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.requireThat
import net.corda.core.messaging.vaultQueryBy
import net.corda.core.node.services.Vault
import net.corda.core.node.services.vault.PageSpecification
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.node.services.vault.Sort
import net.corda.core.node.services.vault.SortAttribute
import org.springframework.stereotype.Service
import java.time.Instant

@Service
class ProjectService (private val rpc: NodeRPCConnection, private val fhc: FlowHandlerCompletion): IProjectService
{
    override var currentUserId: String = AppConstants.DEFAULT_USER_ID
    override var currentUserOrgId: String = AppConstants.DEFAULT_ORGANIZATION_ID

    override fun setCurrentUserDetails(userId: String?, organizationId: String?)
    {
        currentUserId = userId ?: currentUserId
        currentUserOrgId = organizationId ?: currentUserOrgId
    }

    override fun getAll(): Any
    {
        val projectStateRef = rpc.proxy.vaultQuery(ProjectState::class.java).states
        return projectStateRef.map { mapToProjectDTO(it.state.data) }
    }

    override fun getProjectsByOrganization(linearId: String): List<ProjectDTO> {
        val projectStateRef = rpc.proxy.vaultQuery(ProjectState::class.java).states
        val projectStates = projectStateRef.filter { stateAndRef ->
            stateAndRef.state.data.organizationId == linearId
        }
        return projectStates.map { mapToProjectDTO(it.state.data) }
    }

    override fun filterProjectsByProgram(withProgram: Boolean): List<ProjectDTO> {
        val projectStateRef = rpc.proxy.vaultQuery(ProjectState::class.java).states
        val projectStates = projectStateRef.filter { stateAndRef ->
            when {
                withProgram -> stateAndRef.state.data.programId != null
                else -> stateAndRef.state.data.programId == null
            }
        }
        return projectStates.map { mapToProjectDTO(it.state.data) }
    }

    override fun get(linearId: String): Any
    {
        val projectStateRef = rpc.proxy.vaultQuery(ProjectState::class.java).states
        val projectState = projectStateRef.find { stateAndRef ->
            stateAndRef.state.data.linearId.toString() == linearId
        } ?: throw NotFoundException("Project not found")
        return mapToProjectDTO(projectState.state.data)
    }

    override fun getAllProcess(): List<ProcessDTO>
    {
        val processStateRef = rpc.proxy.vaultQuery(ProcessState::class.java).states
        return processStateRef.map { mapToProcessDTO(it.state.data) }
    }

    override fun getProcess(linearId: String): ProcessDTO
    {
        val processStateRef = rpc.proxy.vaultQuery(ProcessState::class.java).states
        val processState = processStateRef.find { stateAndRef ->
            stateAndRef.state.data.linearId.toString() == linearId
        } ?: throw NotFoundException("Process not found")
        return mapToProcessDTO(processState.state.data)
    }

    override fun getAllPrograms(): List<ProgramDTO>
    {
        val programStateRef = rpc.proxy.vaultQuery(ProgramState::class.java).states
        val projectStateRef = rpc.proxy.vaultQuery(ProjectState::class.java).states
        return programStateRef.map {
            val pId = it.state.data.linearId.toString()
            val foundProjects = projectStateRef.filter { item -> item.state.data.programId == pId }
            mapToProgramDTO(it.state.data, foundProjects)
        }
    }

    override fun getProgram(linearId: String): ProgramDTO
    {
        val programStateRef = rpc.proxy.vaultQuery(ProgramState::class.java).states
        val projectStateRef = rpc.proxy.vaultQuery(ProjectState::class.java).states
        val programState = programStateRef.find { stateAndRef ->
            stateAndRef.state.data.linearId.toString() == linearId
        } ?: throw NotFoundException("Program not found")
        val foundProjects = projectStateRef.filter { item -> item.state.data.programId == linearId }
        return mapToProgramDTO(programState.state.data, foundProjects)
    }

    override fun createProject(request: ProjectRegisterFlowDTO): ProjectDTO
    {
        if(request.name.isBlank() || request.name.isEmpty())
            throw Exception("Name cannot be empty")

        if(request.organizationId.isBlank() || request.organizationId.isEmpty())
            throw Exception("Organization id cannot be empty")

        val industryType = "CONSTRUCTION"
        val status = "ONGOING"

        if(request.startAt != null && request.endAt != null)
        {
            val start = Instant.parse(request.startAt) ?: throw Exception("Incorrect start date")
            val end = Instant.parse(request.endAt) ?: throw Exception("Incorrect end date")
            if(start > end)
                throw Exception("Start date cannot be grater than end date")
        }

        val organizationStateRef = rpc.proxy.vaultQuery(OrganizationState::class.java).states
        val foundOrganization = organizationStateRef.find { it.state.data.linearId.toString() == request.organizationId } ?: throw NotFoundException("Organization not found")

        val projectFlowReturn = rpc.proxy.startFlowDynamic(
                CreateProjectFlow::class.java,
                request.name,
                foundOrganization.state.data.linearId.toString(),
                request.startAt,
                request.endAt,
                request.budgetCurrency,
                request.budgetAmount,
                request.image,
                industryType,
                request.contractId,
                request.processId,
                request.programId,
                status
        )
        fhc.flowHandlerCompletion(projectFlowReturn)
        val projectFlowResult = projectFlowReturn.returnValue.get().coreTransaction.outputStates.first() as ProjectState
        val logContent = "Created project ${projectFlowResult.name}"
        this.addToProjectLog(projectFlowResult.linearId.toString(), logContent, null)

        // Automatic add collaborator as self
        val cRoleId: String
        val cRoleStateRef = rpc.proxy.vaultQuery(CollaboratorRoleState::class.java).states
        val foundCRole = cRoleStateRef.find { it.state.data.name.toUpperCase() == CollaboratorRoles.CLIENT.toString() }
        cRoleId = if(foundCRole != null)
        {
            foundCRole.state.data.linearId.toString()
        } else
        {
            val cRoleFlowReturn = rpc.proxy.startFlowDynamic(
                    CreateCollaboratorRoleFlow::class.java,
                    "Client"
            )
            fhc.flowHandlerCompletion(cRoleFlowReturn)
            val cRoleFlowResult = cRoleFlowReturn.returnValue.get().coreTransaction.outputStates.first() as CollaboratorRoleState
            cRoleFlowResult.linearId.toString()
        }

        val collabFlowReturn = rpc.proxy.startFlowDynamic(
                CreateSelfCollaboratorFlow::class.java,
                foundOrganization.state.data.name,
                projectFlowResult.linearId.toString(),
                cRoleId
        )
        fhc.flowHandlerCompletion(collabFlowReturn)
        val collaboratorLogContent = "Added organization ${foundOrganization.state.data.name} as a Client to project ${projectFlowResult.name}"
        this.addToProjectLog(projectFlowResult.linearId.toString(), collaboratorLogContent, null)
        return mapToProjectDTO(projectFlowResult)
    }

    override fun updateProject(linearId: String, request: ProjectUpdateFlowDTO): ProjectDTO
    {
        if(request.name.isBlank() || request.name.isEmpty())
            throw Exception("Name cannot be empty")

        if(request.endAt != null && request.startAt == null)
            throw Exception("Cannot have an end date without a start date")

        val projectStateRef = rpc.proxy.vaultQuery(ProjectState::class.java).states
        val foundProject = projectStateRef.firstOrNull { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Project not found")

        val flowReturn = rpc.proxy.startFlowDynamic(
                UpdateProjectFlow::class.java,
                request.name,
                foundProject.state.data.organizationId,
                request.startAt,
                request.endAt,
                request.budgetCurrency,
                request.budgetAmount,
                request.image,
                foundProject.state.data.industryType,
                request.contractId,
                request.processId,
                foundProject.state.data.programId,
                foundProject.state.data.status,
                foundProject.state.data.linearId.toString()
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as ProjectState
        val logContent = "Updated project ${flowResult.name}"
        this.addToProjectLog(flowResult.linearId.toString(), logContent, null)
        return mapToProjectDTO(flowResult)
    }

    override fun createProcess(request: ProcessFlowDTO): ProcessDTO
    {
        if(request.name.isBlank() || request.name.isEmpty())
            throw Exception("Name cannot be empty.")

        val flowReturn = rpc.proxy.startFlowDynamic(
                CreateProcessFlow::class.java,
                request.name
        )
        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as ProcessState
        return mapToProcessDTO(flowResult)
    }

    override fun createProgram(request: ProgramFlowDTO): ProgramDTO
    {
        if(request.name.isBlank() || request.name.isEmpty())
            throw Exception("Name cannot be empty")

        val projects = mutableListOf<StateAndRef<ProjectState>>()

        if(request.projectIds != null && request.projectIds.isNotEmpty()) {
            val projectStateRef = rpc.proxy.vaultQuery(ProjectState::class.java).states
            request.projectIds.forEach { item ->
                val found = projectStateRef.find { it.state.data.linearId.toString() == item }
                        ?: throw NotFoundException("Project with id $item not found")
                projects.add(found)
            }
        }

        val flowReturn = rpc.proxy.startFlowDynamic(
                CreateProgramFlow::class.java,
                request.name,
                request.description
        )

        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as ProgramState

        if(projects.isNotEmpty()){
            projects.forEach {
                val projectFlowReturn = rpc.proxy.startFlowDynamic(
                        UpdateProjectFlow::class.java,
                        it.state.data.name,
                        it.state.data.organizationId,
                        it.state.data.startAt,
                        it.state.data.endAt,
                        it.state.data.budgetCurrency,
                        it.state.data.budgetAmount,
                        it.state.data.image,
                        it.state.data.industryType,
                        it.state.data.contractId,
                        it.state.data.processId,
                        flowResult.linearId.toString(),
                        it.state.data.status,
                        it.state.data.linearId.toString()
                )

                fhc.flowHandlerCompletion(projectFlowReturn)
                val logContent = "Set project ${it.state.data.name}'s program to ${flowResult.name}"
                this.addToProjectLog(it.state.data.linearId.toString(), logContent, null)
            }
        }

        return mapToProgramDTO(flowResult, projects)
    }

    override fun setProgramToProject(linearId: String, request: SetProgramToProjectFlowDTO): ProjectDTO
    {
        if(linearId.isBlank() || linearId.isEmpty())
            throw Exception("Project id cannot be empty")

        val projectStateRef = rpc.proxy.vaultQuery(ProjectState::class.java).states
        val programStateRef = rpc.proxy.vaultQuery(ProgramState::class.java).states
        val foundProject = projectStateRef.find { it.state.data.linearId.toString() == linearId} ?: throw NotFoundException("Project not found")

        var pId: String? = null
        var programName = "blank"
        if(request.programId != null) {
            val foundProgram = programStateRef.find { it.state.data.linearId.toString() == request.programId} ?: throw NotFoundException("Program not found")
            pId = foundProgram.state.data.linearId.toString()
            programName = foundProgram.state.data.name
        }

        val flowReturn = rpc.proxy.startFlowDynamic(
                UpdateProjectFlow::class.java,
                foundProject.state.data.name,
                foundProject.state.data.organizationId,
                foundProject.state.data.startAt,
                foundProject.state.data.endAt,
                foundProject.state.data.budgetCurrency,
                foundProject.state.data.budgetAmount,
                foundProject.state.data.image,
                foundProject.state.data.industryType,
                foundProject.state.data.contractId,
                foundProject.state.data.processId,
                pId,
                foundProject.state.data.status,
                foundProject.state.data.linearId.toString()
        )

        fhc.flowHandlerCompletion(flowReturn)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as ProjectState
        val logContent = "Set project ${foundProject.state.data.name}'s program to $programName"
        this.addToProjectLog(linearId, logContent, null)
        return mapToProjectDTO(flowResult)
    }

    override fun setProjectStatus(linearId: String, request: ProjectStatusFlowDTO): ProjectDTO
    {
        if(request.status.isBlank() || request.status.isEmpty())
            throw Exception("Status cannot be empty")

        val projectStateRef = rpc.proxy.vaultQuery(ProjectState::class.java).states
        val found = projectStateRef.firstOrNull { it.state.data.linearId.toString() == linearId } ?: throw NotFoundException("Not found")

        val flowReturn = rpc.proxy.startFlowDynamic(
                UpdateProjectFlow::class.java,
                found.state.data.name,
                found.state.data.organizationId,
                found.state.data.startAt,
                found.state.data.endAt,
                found.state.data.budgetCurrency,
                found.state.data.budgetAmount,
                found.state.data.image,
                found.state.data.industryType,
                found.state.data.contractId,
                found.state.data.processId,
                found.state.data.programId,
                request.status.toUpperCase().trim(),
                found.state.data.linearId.toString()
        )
        fhc.flowHandlerCompletion(flowReturn)
        val logContent = "Set project ${found.state.data.name}'s status to ${request.status.toUpperCase().trim()}"
        this.addToProjectLog(linearId, logContent, null)
        val flowResult = flowReturn.returnValue.get().coreTransaction.outputStates.first() as ProjectState
        return mapToProjectDTO(flowResult)
    }

    override fun addToProjectLog(projectId: String, content: String, updatedBy: String?) {
        val flowReturn = rpc.proxy.startFlowDynamic(
                CreateOrUpdateProjectLogFlow::class.java,
                projectId,
                content,
                updatedBy
        )
        fhc.flowHandlerCompletion(flowReturn)
    }

    override fun getAllProjectLog(projectId: String, pageNumber: Int, pageSize : Int): MutableList<ProjectLogDTO>
    {
        val projectLogStateRef = rpc.proxy.vaultQuery(ProjectLogState::class.java).states
        val found = projectLogStateRef.find{ it.state.data.projectId == projectId } ?: throw NotFoundException("Project not found")

        val linearIdCriteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(found.state.data.linearId), status = Vault.StateStatus.ALL)
        val sortColumn = Sort.SortColumn(SortAttribute.Standard(Sort.VaultStateAttribute.RECORDED_TIME), Sort.Direction.DESC)
        val sorting = Sort(listOf(sortColumn))
        val logs = rpc.proxy.vaultQueryBy<ProjectLogState>(linearIdCriteria, paging = PageSpecification(pageNumber = pageNumber, pageSize = pageSize), sorting = sorting)
        val list = mutableListOf<ProjectLogDTO>()
        if(logs.states.isNotEmpty())
        {
            val userStateRef = rpc.proxy.vaultQuery(UserState::class.java).states
            logs.states.forEach {
                if(it.state.data.updatedBy != null){
                    val userFound = userStateRef.firstOrNull{ user -> user.state.data.linearId.toString() == it.state.data.updatedBy}
                    list.add(mapToProjectLogDTO(it.state.data, userFound!!.state.data))
                }else{
                    list.add(mapToProjectLogDTO(it.state.data, null))
                }
            }
        }
        return list
    }
}
