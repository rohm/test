package coadjute.common.appexceptions

class NotFoundException(message: String) : Exception(message)
class UnauthorizedException(message: String) : Exception(message)
class MailerException(message: String): Exception(message)