package coadjute.common

enum class JobLogActions {
    CREATE, UPDATE, DO, REVIEW, SIGN_OFF, SPLIT, MERGE, ARCHIVE
}

enum class ProjectStatus {
    ONGOING, COMPLETED
}

enum class JobStatus {
    NEW, PENDING, IN_PROGRESS, BLOCKED, PENDING_SIGN_OFF, COMPLETED
}

enum class JobAction {
    NEW, DO, FOR_REVIEW, FOR_SIGNOFF
}

enum class CollaboratorStatus {
    PENDING, ACCEPTED, DECLINED
}

enum class CollaboratorRoles {
    CLIENT, REGULATOR
}
