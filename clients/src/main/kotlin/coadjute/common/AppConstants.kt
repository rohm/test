package coadjute.common

class AppConstants
{
    companion object
    {
        const val DEFAULT_USER_ID = "SYSTEM"
        const val DEFAULT_ORGANIZATION_ID = "SYSTEM"

        const val REGEX_EMAIL = "^[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}\$" //No + : ^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}
        const val REGEX_NAME = "^[^'][A-Za-z0-9'\\s]+[^']\$"

        const val EMAIL_TEMPLATES_PATH = "/opt/corda/email-templates"
        const val FILE_STORAGE_PATH = "/opt/corda/file-storage"
    }
}
