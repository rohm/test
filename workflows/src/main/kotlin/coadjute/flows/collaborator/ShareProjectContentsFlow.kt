package coadjute.flows.collaborator

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.project.*
import coadjute.functions.FlowFunctions
import coadjute.functions.ShareFlow
import coadjute.states.project.*
import net.corda.core.contracts.Command
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@InitiatingFlow
class ShareProjectContentsFlow (private val projectCollaboratorId: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val ptx = verifyAndSign(transaction())
        val projectCollaborator = getProjectCollaboratorRefByLinearId(projectCollaboratorId).state.data
        val session = initiateFlow(stringToParty(projectCollaborator.nodeO))
        val transactionSignedByParties = subFlow(CollectSignaturesFlow(ptx, listOf(session)))
        return subFlow(FinalityFlow(transactionSignedByParties, listOf(session))).also {
            subFlow(ShareFlow(stringToParty(projectCollaborator.nodeO), it))
        }
    }

    private fun projectState(): ProjectState
    {
        val projectCollaborator = getProjectCollaboratorRefByLinearId(projectCollaboratorId).state.data
        val project = getProjectRefByLinearId(projectCollaborator.projectId).state.data
        return ProjectState(
                name = project.name,
                organizationId = project.organizationId,
                startAt = project.startAt,
                endAt = project.endAt,
                budgetCurrency = project.budgetCurrency,
                budgetAmount = project.budgetAmount,
                image = project.image,
                industryType = project.industryType,
                contractId = project.contractId,
                processId = project.processId,
                programId = project.programId,
                status = project.status,
                linearId = project.linearId,
                participants = listOf(ourIdentity)
        )
    }

    private fun projectStagesState(): List<ProjectStageState>
    {
        val projectStage = getProjectStagesRefByProjectId(projectState().linearId.toString())!!.map { it.state.data }
        return projectStage.map {
            ProjectStageState(
                    projectId = it.projectId,
                    processStageId = it.processStageId,
                    name = it.name,
                    gateBit = it.gateBit,
                    gateStageId = it.gateStageId,
                    description = it.description,
                    order = it.order,
                    linearId = it.linearId,
                    participants = it.participants
            )
        }
    }

    private fun projectContractState(): ProjectContractState
    {
        val projectContract = getProjectContractRefByLinearId(projectState().contractId!!).state.data
        return ProjectContractState(
                name = projectContract.name,
                linearId = projectContract.linearId,
                participants = listOf(ourIdentity)
        )
    }

    private fun contractObligationState(): ContractObligationState
    {
        val contractObligation = getContractObligationRefByContractId(projectState().contractId!!)!!.state.data
        return ContractObligationState(
                contractId = contractObligation.contractId,
                name = contractObligation.name,
                description = contractObligation.description,
                linearId = contractObligation.linearId,
                participants = contractObligation.participants
        )
    }

    private fun projectLogState(): ProjectLogState
    {
        val projectLog = getProjectLogRefByProjectId(projectState().linearId.toString())!!.state.data
        return ProjectLogState(
                projectId = projectLog.projectId,
                content = projectLog.content,
                updatedBy = projectLog.updatedBy,
                updatedAt = projectLog.updatedAt,
                linearId = projectLog.linearId,
                participants = projectLog.participants
        )
    }

    private fun processState(): ProcessState
    {
        val process = getProcessRefByLinearId(projectState().processId!!).state.data
        return ProcessState(
                name = process.name,
                linearId = process.linearId,
                participants = process.participants
        )
    }

    private fun processStageStates(): List<ProcessStageState>
    {
        val processStage = getProcessStagesRefByProjectId(projectState().linearId.toString())!!.map { it.state.data }
        return processStage.map {
            ProcessStageState(
                    projectId = it.projectId,
                    processId = it.processId,
                    name = it.name,
                    gateBit = it.gateBit,
                    gateStageId = it.gateStageId,
                    description = it.description,
                    order = it.order,
                    linearId = it.linearId,
                    participants = it.participants
            )
        }
    }

    private fun programState(): ProgramState
    {
        val program = getProgramRefByLinearId(projectState().programId!!).state.data
        return ProgramState(
                name = program.name,
                description = program.description,
                linearId = program.linearId,
                participants = program.participants
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val projectRef = getProjectRefByLinearId(projectState().linearId.toString())

        // Commands
        val projectCmd = Command(
                ProjectContract.Commands.Collaborate(),
                listOf(ourIdentity.owningKey))
        val projectContractCmd = Command(
                ProjectContractContract.Commands.Collaborate(),
                listOf(ourIdentity.owningKey))
        val projectLogCmd = Command(
                ProjectLogContract.Commands.Collaborate(),
                listOf(ourIdentity.owningKey))
        val projectStageCmd = Command(
                ProjectStageContract.Commands.Collaborate(),
                listOf(ourIdentity.owningKey))
        val contractObligationCmd = Command(
                ContractObligationContract.Commands.Collaborate(),
                listOf(ourIdentity.owningKey))
        val processCmd = Command(
                ProcessContract.Commands.Collaborate(),
                listOf(ourIdentity.owningKey))
        val processStageCmd = Command(
                ProcessStageContract.Commands.Collaborate(),
                listOf(ourIdentity.owningKey))
        val programCmd = Command(
                ProgramContract.Commands.Collaborate(),
                listOf(ourIdentity.owningKey))

        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)

        // Share Project
        builder.addInputState(projectRef)
        builder.addOutputState(projectState(), ProjectContract.PROJECT_ID)
        builder.addCommand(projectCmd)

        // Share Project Logs
        if (getProjectLogRefByProjectId(projectState().linearId.toString()) != null )
        {
            val projectLogRef = getProjectLogRefByProjectId(projectState().linearId.toString())!!
            builder.addInputState(projectLogRef)
            builder.addOutputState(projectLogState(), ProjectLogContract.PLCONTRACT_ID)
            builder.addCommand(projectLogCmd)
        }

        // Share Project Contract
        if (projectState().contractId != null)
        {
            val projectContractRef = getProjectContractRefByLinearId(projectState().contractId!!)
            builder.addInputState(projectContractRef)
            builder.addOutputState(projectContractState(), ProjectContractContract.PROJECTCONTRACT_ID)
            builder.addCommand(projectContractCmd)

            // Share Contract Obligation
            if (getContractObligationRefByContractId(projectContractState().linearId.toString()) != null)
            {
                val contractObligationRef = getContractObligationRefByContractId(projectState().contractId!!)!!
                builder.addInputState(contractObligationRef)
                builder.addOutputState(contractObligationState(), ContractObligationContract.COBLIGATION_ID)
                builder.addCommand(contractObligationCmd)
            }
        }

        // Share Project Stage
        if (getProjectStagesRefByProjectId(projectState().linearId.toString()) != null)
        {
            val projectStageRef = getProjectStagesRefByProjectId(projectState().linearId.toString())!!
            projectStageRef.map { builder.addInputState(it) }
            projectStagesState().map { builder.addOutputState(it, ProjectStageContract.PROJSCONTRACT_ID) }
            builder.addCommand(projectStageCmd)
        }

        // Share Process
        if (projectState().processId != null)
        {
            val process = getProcessRefByLinearId(projectState().processId!!).state.data
            val processRef = getProcessRefByLinearId(process.linearId.toString())
            builder.addInputState(processRef)
            builder.addOutputState(processState(), ProcessContract.PROCESS_ID)
            builder.addCommand(processCmd)

            // Share Process Stage
            if (getProcessStagesRefByProjectId(projectState().linearId.toString()) != null)
            {
                val listOfProcessStageRef = getProcessStagesRefByProjectId(projectState().processId!!)!!
                listOfProcessStageRef.map { builder.addInputState(it) }
                processStageStates().map { builder.addOutputState(it, ProcessStageContract.PROCSCONTRACT_ID) }
                builder.addCommand(processStageCmd)
            }
        }

        // Share Program
        if (projectState().programId != null)
        {
            val program = getProgramRefByLinearId(projectState().programId!!).state.data
            val programRef = getProgramRefByLinearId(program.linearId.toString())
            builder.addInputState(programRef)
            builder.addOutputState(programState(), ProgramContract.PROGRAM_ID)
            builder.addCommand(programCmd)
        }
        return builder

    }
}

@InitiatedBy(ShareProjectContentsFlow::class)
class ShareProjectContentsFlowResponder(private val flowSession: FlowSession): FlowLogic<SignedTransaction>()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        subFlow(object : SignTransactionFlow(flowSession)
        {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
            }
        })
        return subFlow(ReceiveFinalityFlow(otherSideSession = flowSession))
    }
}

