package coadjute.flows.collaborator

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.collaborator.CollaboratorContract
import coadjute.functions.FlowFunctions
import coadjute.states.collaborator.CollaboratorState
import coadjute.states.job.JobState
import coadjute.states.project.ProjectState
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.unwrap

@InitiatingFlow
@StartableByRPC
class UpdateCollaboratorFlow (private val status: String,
                              private val remarks: String?,
                              private val projectCollaboratorId: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val ptx = verifyAndSign(transaction())
        val session = (outState().participants - ourIdentity).map { initiateFlow(it) }
        val counterParty = session.map { it }.first()
        counterParty.send(outState())
        val transactionSignedByParties = subFlow(CollectSignaturesFlow(ptx, session))
        return subFlow(FinalityFlow(transactionSignedByParties, session))
    }

    private fun outState(): CollaboratorState
    {
        val projectCollaborator = getProjectCollaboratorRefByLinearId(projectCollaboratorId).state.data
        val collaborator = stringToParty(projectCollaborator.nodeO)
        if (ourIdentity != collaborator)
            throw FlowException("Initiator cannot update this state.")

        when (status.toUpperCase())
        {
            "ACCEPTED" -> return CollaboratorState(
                    name = projectCollaborator.name,
                    projectId = projectCollaborator.projectId,
                    collaboratorRoleId = projectCollaborator.collaboratorRoleId,
                    nodeO = projectCollaborator.nodeO,
                    nodeCN = collaborator.name.commonName,
                    nodeOU = collaborator.name.organisationUnit,
                    nodeL = collaborator.name.locality,
                    nodeC = collaborator.name.country,
                    status = status.toUpperCase(),
                    remarks = remarks,
                    linearId = projectCollaborator.linearId,
                    participants = projectCollaborator.participants
            )
            "DECLINED" -> return CollaboratorState(
                    name = projectCollaborator.name,
                    projectId = projectCollaborator.projectId,
                    collaboratorRoleId = projectCollaborator.collaboratorRoleId,
                    nodeO = projectCollaborator.nodeO,
                    nodeCN = null,
                    nodeOU = null,
                    nodeL = null,
                    nodeC = null,
                    status = status.toUpperCase(),
                    remarks = remarks,
                    linearId = projectCollaborator.linearId,
                    participants = projectCollaborator.participants
            )
            else -> throw FlowException("Status is not valid (ACCEPTED | DECLINED)")
        }
    }

    private fun transaction(): TransactionBuilder
    {
        val projectCollaborator = getProjectCollaboratorRefByLinearId(projectCollaboratorId).state.data
        val projectCollaboratorRef = getProjectCollaboratorRefByLinearId(projectCollaboratorId)
        val collaboratorCmd = Command(
                CollaboratorContract.Commands.Update(),
                projectCollaborator.participants.map { it.owningKey })
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addInputState(projectCollaboratorRef)
        builder.addOutputState(outState(), CollaboratorContract.PCCONTRACT_ID)
        builder.addCommand(collaboratorCmd)
        return builder
    }
}

@InitiatedBy(UpdateCollaboratorFlow::class)
class UpdateCollaboratorFlowResponder(private val flowSession: FlowSession): FlowLogic<SignedTransaction>()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val projectCollaborator = flowSession.receive<CollaboratorState>().unwrap { it }
        when (projectCollaborator.status.toUpperCase())
        {
             "ACCEPTED" -> {
                subFlow(ShareProjectContentsFlow(projectCollaborator.linearId.toString()))
                 if (getJobsRefByProjectId(projectCollaborator.projectId)!!.isNotEmpty())
                 {
                     subFlow(ShareJobContentsFlow(projectCollaborator.linearId.toString()))
                 }
                subFlow(object : SignTransactionFlow(flowSession) {
                    override fun checkTransaction(stx: SignedTransaction) = requireThat {
                    }
                })
                return subFlow(ReceiveFinalityFlow(otherSideSession = flowSession))
            }
            "DECLINED" -> {
                subFlow(object : SignTransactionFlow(flowSession) {
                    override fun checkTransaction(stx: SignedTransaction) = requireThat {
                    }
                })
                return subFlow(ReceiveFinalityFlow(otherSideSession = flowSession))
            }
            else -> throw Exception("Status is not valid (ACCEPTED | DECLINED)")
        }
    }

    private fun getJobsRefByProjectId(projectId: String): List<StateAndRef<JobState>>?
    {
        val criteria = QueryCriteria.VaultQueryCriteria()
        return serviceHub.vaultService.queryBy<JobState>(criteria = criteria).states.filter {
            it.state.data.projectId == projectId
        }
    }
}