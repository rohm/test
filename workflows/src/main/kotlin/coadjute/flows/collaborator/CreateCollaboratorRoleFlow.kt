package coadjute.flows.collaborator

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.collaborator.CollaboratorRoleContract
import coadjute.contracts.collaborator.CollaboratorRoleContract.Companion.CBROLE_ID
import coadjute.functions.FlowFunctions
import coadjute.states.collaborator.CollaboratorRoleState
import net.corda.core.contracts.Command
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@StartableByRPC
class CreateCollaboratorRoleFlow (private val name: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val signedTransaction = verifyAndSign(transaction())
        return subFlow(FinalityFlow(signedTransaction, listOf()))
    }

    private fun outState(): CollaboratorRoleState
    {
        return CollaboratorRoleState(
                name = name,
                linearId = UniqueIdentifier(),
                participants = listOf(ourIdentity)
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val cmd = Command(CollaboratorRoleContract.Commands.Role(), ourIdentity.owningKey)
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addOutputState(outState(), CBROLE_ID)
        builder.addCommand(cmd)
        return builder
    }
}