package coadjute.flows.collaborator

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.collaborator.CollaboratorContract
import coadjute.contracts.collaborator.CollaboratorContract.Companion.PCCONTRACT_ID
import coadjute.functions.FlowFunctions
import coadjute.states.collaborator.CollaboratorState
import net.corda.core.contracts.Command
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.*
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@InitiatingFlow
@StartableByRPC
class CreateSelfCollaboratorFlow (private val name: String,
                                  private val projectId: String,
                                private val collaboratorRoleId: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val signedTransaction = verifyAndSign(transaction())
        return subFlow(FinalityFlow(signedTransaction, listOf()))
    }

    private fun outState(): CollaboratorState
    {
        return CollaboratorState(
                name = name,
                projectId = projectId,
                collaboratorRoleId = collaboratorRoleId,
                nodeO = ourIdentity.name.organisation,
                nodeCN = null,
                nodeOU = null,
                nodeL = null,
                nodeC = null,
                status = "ACCEPTED",
                remarks = "Project Client",
                linearId = UniqueIdentifier(),
                participants = listOf(ourIdentity)
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val cmd = Command(CollaboratorContract.Commands.Create(), ourIdentity.owningKey)
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addOutputState(outState(), PCCONTRACT_ID)
        builder.addCommand(cmd)
        return builder
    }
}