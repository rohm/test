package coadjute.flows.collaborator

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.job.FollowerContract
import coadjute.contracts.job.JobContract
import coadjute.contracts.job.JobLogContract
import coadjute.functions.FlowFunctions
import coadjute.states.job.FollowerState
import coadjute.states.job.JobLogState
import coadjute.states.job.JobState
import net.corda.core.contracts.Command
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@InitiatingFlow
class ShareJobContentsFlow (private val projectCollaboratorId: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val projectCollaborator = getProjectCollaboratorRefByLinearId(projectCollaboratorId).state.data
        val ptx = verifyAndSign(transaction())
        val session = initiateFlow(stringToParty(projectCollaborator.nodeO))
        val transactionSignedByParties = subFlow(CollectSignaturesFlow(ptx, listOf(session)))
        return subFlow(FinalityFlow(transactionSignedByParties, listOf(session)))
    }

    private fun jobStates(): List<JobState>
    {
        val projectCollaborator = getProjectCollaboratorRefByLinearId(projectCollaboratorId).state.data
        val jobs = getJobsRefByProjectId(projectCollaborator.projectId)!!.map { it.state.data }
        return jobs.map {
            JobState(
                    name = it.name,
                    projectId = it.projectId,
                    status = it.status,
                    action = it.action,
                    instruction = it.instruction,
                    artifactBit = it.artifactBit,
                    url = it.url,
                    fileSize = it.fileSize,
                    hash = it.hash,
                    remarks = it.remarks,
                    referenceJobId = it.referenceJobId,
                    projectStageId = it.projectStageId,
                    startAt = it.startAt,
                    endAt = it.endAt,
                    responsibleCollaboratorId = it.responsibleCollaboratorId,
                    responsibleUserId = it.responsibleUserId,
                    responsibleAcceptedAt = it.responsibleAcceptedAt,
                    ownerCollaboratorId = it.ownerCollaboratorId,
                    ownerUserId = it.ownerUserId,
                    ownerAcceptedAt = it.ownerAcceptedAt,
                    creatorCollaboratorId = it.creatorCollaboratorId,
                    creatorUserId = it.creatorUserId,
                    linearId = it.linearId,
                    participants = listOf(ourIdentity, stringToParty(projectCollaborator.nodeO))
            )
        }
    }

    private fun jobLogStates(): List<JobLogState>
    {
        val projectCollaborator = getProjectCollaboratorRefByLinearId(projectCollaboratorId).state.data
        val jobs = getJobsRefByProjectId(projectCollaborator.projectId)!!.map { it.state.data }
        val jobIds = jobs.map { it.linearId.toString() }
        val listOfJobLogs = jobIds.map { getJobLogsByJobId(it)!! }.map { it.state.data }
        return listOfJobLogs.map {
            JobLogState(
                    jobId = it.jobId,
                    content = it.content,
                    action = it.action,
                    splitJobIds = it.splitJobIds,
                    mergeFromJobIds = it.mergeFromJobIds,
                    updatedByCollaboratorId = it.updatedByCollaboratorId,
                    updatedByUserId = it.updatedByUserId,
                    updatedByUserFirstName = it.updatedByUserFirstName,
                    updatedByUserMiddleName = it.updatedByUserMiddleName,
                    updatedByUserLastName = it.updatedByUserLastName,
                    updatedAt = it.updatedAt,
                    linearId = it.linearId,
                    participants = listOf(ourIdentity, stringToParty(projectCollaborator.nodeO))
            )
        }
    }

//    private fun followerStates(): List<FollowerState>
//    {
//        val projectCollaborator = getProjectCollaboratorRefByLinearId(projectCollaboratorId).state.data
//        val jobs = getJobsRefByProjectId(projectCollaborator.projectId)!!.map { it.state.data }
//        val jobIds = jobs.map { it.linearId.toString() }
//        val followers = jobIds.map { getFollowerByJobId(it) }.map { it!!.state.data }
//        return followers.map {
//            FollowerState(
//                    jobId = it.jobId,
//                    collaboratorId = it.collaboratorId,
//                    userId = it.userId,
//                    linearId = it.linearId,
//                    participants = listOf(ourIdentity, stringToParty(projectCollaborator.nodeO))
//            )
//        }
//    }

    private fun transaction(): TransactionBuilder
    {
        val projectCollaborator = getProjectCollaboratorRefByLinearId(projectCollaboratorId).state.data

        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)

        // Share Jobs
        if (getJobsRefByProjectId(projectCollaborator.projectId) != null)
        {
            val jobRef = getJobsRefByProjectId(projectCollaborator.projectId)!!
            val jobCmd = Command(JobContract.Commands.Collaborate(), listOf(ourIdentity, stringToParty(projectCollaborator.nodeO)).map { it.owningKey })
            jobRef.map { builder.addInputState(it) }
            jobStates().map { builder.addOutputState(it, JobContract.JOB_ID) }
            builder.addCommand(jobCmd)

            // Share Job Logs
            val jobs = getJobsRefByProjectId(projectCollaborator.projectId)!!.map { it.state.data }
            val jobIds = jobs.map { it.linearId.toString() }
            val listOfJobLogsRef = jobIds.map { getJobLogsByJobId(it)!! }
            val jobLogCmd = Command(JobLogContract.Commands.Collaborate(), listOf(ourIdentity, stringToParty(projectCollaborator.nodeO)).map { it.owningKey })
            listOfJobLogsRef.map { builder.addInputState(it) }
            jobLogStates().map { builder.addOutputState(it, JobLogContract.JOBLOG_ID) }
            builder.addCommand(jobLogCmd)

//            // Share Followers
//            val jobs = getJobsRefByProjectId(projectCollaborator.projectId)!!.map { it.state.data }
//            val jobIds = jobs.map { it.linearId.toString() }
//            val followers = jobIds.map { getFollowerByJobId(it) }.map { it!!.state.data }
//
//            if (followers.isNotEmpty())
//            {
//                val followerRef = jobIds.map { getFollowerByJobId(it)!! }
//                val followerCmd = Command(FollowerContract.Commands.Collaborate(), listOf(ourIdentity, stringToParty(projectCollaborator.nodeO)).map { it.owningKey })
//                followerRef.map { builder.addInputState(it) }
//                followerStates().map { builder.addOutputState(it, FollowerContract.FOLLOWER_ID) }
//                builder.addCommand(followerCmd)
//            }
        }

        return builder
    }
}

@InitiatedBy(ShareJobContentsFlow::class)
class ShareJobContentsFlowResponder(private val flowSession: FlowSession): FlowLogic<SignedTransaction>()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        subFlow(object : SignTransactionFlow(flowSession)
        {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
            }
        })
        return subFlow(ReceiveFinalityFlow(otherSideSession = flowSession))
    }
}