package coadjute.flows.collaborator

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.collaborator.CollaboratorContract
import coadjute.contracts.collaborator.CollaboratorContract.Companion.PCCONTRACT_ID
import coadjute.contracts.collaborator.CollaboratorRoleContract
import coadjute.contracts.collaborator.CollaboratorRoleContract.Companion.CBROLE_ID
import coadjute.functions.FlowFunctions
import coadjute.states.collaborator.CollaboratorRoleState
import coadjute.states.collaborator.CollaboratorState
import net.corda.core.contracts.Command
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@InitiatingFlow
@StartableByRPC
class CreateCollaboratorFlow (private val collaboratorName: String,
                              private val collaboratorNodeO: String,
                              private val projectId: String,
                              private val collaboratorRoleId: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val ptx = verifyAndSign(transaction())
        val session = initiateFlow(stringToParty(collaboratorNodeO))
        val transactionSignedByParties = subFlow(CollectSignaturesFlow(ptx, listOf(session)))
        return subFlow(FinalityFlow(transactionSignedByParties, listOf(session)))
    }

    private fun outState(): CollaboratorState
    {
        return CollaboratorState(
                name = collaboratorName,
                projectId = projectId,
                collaboratorRoleId = collaboratorRoleId,
                nodeO = collaboratorNodeO,
                nodeCN = null,
                nodeOU = null,
                nodeL = null,
                nodeC = null,
                status = "PENDING",
                remarks = null,
                linearId = UniqueIdentifier(),
                participants = listOf(ourIdentity, stringToParty(collaboratorNodeO))
        )
    }

    private fun collaboratorRoleState(): CollaboratorRoleState
    {
        val collaboratorRole = getCollaboratorRoleRefByLinearId(collaboratorRoleId).state.data
        return CollaboratorRoleState(
                name = collaboratorRole.name,
                linearId = collaboratorRole.linearId,
                participants = collaboratorRole.participants + stringToParty(collaboratorNodeO)
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val collaboratorCmd = Command(
                CollaboratorContract.Commands.Create(),
                listOf(ourIdentity.owningKey, stringToParty(collaboratorNodeO).owningKey))
        val collaboratorRoleCmd = Command(
                CollaboratorRoleContract.Commands.Collaborate(),
                listOf(ourIdentity.owningKey, stringToParty(collaboratorNodeO).owningKey))
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addOutputState(outState(), PCCONTRACT_ID)
        builder.addOutputState(collaboratorRoleState(), CBROLE_ID)
        builder.addCommand(collaboratorCmd)
        builder.addCommand(collaboratorRoleCmd)
        return builder
    }
}

@InitiatedBy(CreateCollaboratorFlow::class)
class CreateCollaboratorFlowResponder(private val flowSession: FlowSession): FlowLogic<SignedTransaction>()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        subFlow(object : SignTransactionFlow(flowSession)
            {
                override fun checkTransaction(stx: SignedTransaction) = requireThat {
                }
            })
        return subFlow(ReceiveFinalityFlow(otherSideSession = flowSession))
    }
}