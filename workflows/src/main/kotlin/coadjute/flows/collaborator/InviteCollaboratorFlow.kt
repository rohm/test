package coadjute.flows.collaborator

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.collaborator.CollaboratorInviteContract
import coadjute.contracts.collaborator.CollaboratorInviteContract.Companion.PCINVITE_ID
import coadjute.functions.FlowFunctions
import coadjute.states.collaborator.CollaboratorInviteState
import net.corda.core.contracts.Command
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@StartableByRPC
class InviteCollaboratorFlow (private val projectId: String,
                              private val name: String,
                              private val email: String,
                              private val contactName: String,
                              private val contactNumber: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val signedTransaction = verifyAndSign(transaction())
        return subFlow(FinalityFlow(signedTransaction, listOf()))
    }

    private fun outState(): CollaboratorInviteState
    {
        return CollaboratorInviteState(
                projectId = projectId,
                name = name,
                email = email,
                normalizedEmail = email.toLowerCase().trim(),
                contactName = contactName,
                contactNumber = contactNumber,
                nodeO = null,
                linearId = UniqueIdentifier(),
                participants = listOf(ourIdentity)
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val cmd = Command(CollaboratorInviteContract.Commands.Invite(), ourIdentity.owningKey)
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addOutputState(outState(), PCINVITE_ID)
        builder.addCommand(cmd)
        return builder
    }
}