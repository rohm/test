package coadjute.flows.job

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.job.JobContract
import coadjute.functions.FlowFunctions
import coadjute.states.job.JobState
import net.corda.core.contracts.Command
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import java.time.Instant

@InitiatingFlow
@StartableByRPC
class AcceptResponsibleFlow (private val jobId: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        var signedTransaction = verifyAndSign(transaction())
        val job = getJobRefByLinearId(jobId).state.data
        val sessions = (job.participants - ourIdentity).map { initiateFlow(it) }
        return if (getAllCollaboratorsByProjectId(job.projectId) != null)
        {
            signedTransaction = subFlow(CollectSignaturesFlow(signedTransaction, sessions))
            subFlow(FinalityFlow(signedTransaction, sessions))
        }
        else
        {
            subFlow(FinalityFlow(signedTransaction, listOf()))
        }
    }

    private fun outState(): JobState
    {
        val job = getJobRefByLinearId(jobId).state.data
        return JobState(
                name = job.name,
                projectId = job.projectId,
                status = job.status,
                action = job.action,
                instruction = job.instruction,
                artifactBit = job.artifactBit,
                url = job.url,
                fileSize = job.fileSize,
                hash = job.hash,
                remarks = job.remarks,
                referenceJobId = job.referenceJobId,
                projectStageId = job.projectStageId,
                startAt = job.startAt,
                endAt = job.endAt,
                responsibleCollaboratorId = job.responsibleCollaboratorId,
                responsibleUserId = job.responsibleUserId,
                responsibleAcceptedAt = Instant.now(),
                ownerCollaboratorId = job.ownerCollaboratorId,
                ownerUserId = job.ownerUserId,
                ownerAcceptedAt = Instant.now(),
                creatorCollaboratorId = job.creatorCollaboratorId,
                creatorUserId = job.creatorUserId,
                linearId = job.linearId,
                participants = job.participants
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val jobStateRef = getJobRefByLinearId(jobId)
        val jobState = getJobRefByLinearId(jobId).state.data
        val cmd = Command(JobContract.Commands.Accept(), jobState.participants.map { it.owningKey })
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addInputState(jobStateRef)
        builder.addOutputState(outState(), JobContract.JOB_ID)
        builder.addCommand(cmd)
        return builder
    }
}

@InitiatedBy(AcceptResponsibleFlow::class)
class AcceptResponsibleFlowResponder(private val flowSession: FlowSession): FlowLogic<SignedTransaction>()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        subFlow(object : SignTransactionFlow(flowSession)
        {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
            }
        })
        return subFlow(ReceiveFinalityFlow(otherSideSession = flowSession))
    }
}

@InitiatingFlow
@StartableByRPC
class DeclineResponsibleFlow (private val jobId: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        var signedTransaction = verifyAndSign(transaction())
        val job = getJobRefByLinearId(jobId).state.data
        val sessions = (job.participants - ourIdentity).map { initiateFlow(it) }
        return if (getAllCollaboratorsByProjectId(job.projectId) != null)
        {
            signedTransaction = subFlow(CollectSignaturesFlow(signedTransaction, sessions))
            subFlow(FinalityFlow(signedTransaction, sessions))
        }
        else
        {
            subFlow(FinalityFlow(signedTransaction, listOf()))
        }
    }

    private fun outState(): JobState
    {
        val job = getJobRefByLinearId(jobId).state.data
        return JobState(
                name = job.name,
                projectId = job.projectId,
                status = job.status,
                action = job.action,
                instruction = job.instruction,
                artifactBit = job.artifactBit,
                url = job.url,
                fileSize = job.fileSize,
                hash = job.hash,
                remarks = job.remarks,
                referenceJobId = job.referenceJobId,
                projectStageId = job.projectStageId,
                startAt = job.startAt,
                endAt = job.endAt,
                responsibleCollaboratorId = null,
                responsibleUserId = null,
                responsibleAcceptedAt = null,
                ownerCollaboratorId = null,
                ownerUserId = null,
                ownerAcceptedAt = null,
                creatorCollaboratorId = job.creatorCollaboratorId,
                creatorUserId = job.creatorUserId,
                linearId = job.linearId,
                participants = job.participants
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val jobStateRef = getJobRefByLinearId(jobId)
        val jobState = getJobRefByLinearId(jobId).state.data
        val cmd = Command(JobContract.Commands.Decline(), jobState.participants.map { it.owningKey })
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addInputState(jobStateRef)
        builder.addOutputState(outState(), JobContract.JOB_ID)
        builder.addCommand(cmd)
        return builder
    }
}

@InitiatedBy(DeclineResponsibleFlow::class)
class DeclineResponsibleFlowResponder(private val flowSession: FlowSession): FlowLogic<SignedTransaction>()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        subFlow(object : SignTransactionFlow(flowSession)
        {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
            }
        })
        return subFlow(ReceiveFinalityFlow(otherSideSession = flowSession))
    }
}