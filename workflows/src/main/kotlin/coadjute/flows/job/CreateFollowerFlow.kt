package coadjute.flows.job

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.job.FollowerContract
import coadjute.contracts.job.FollowerContract.Companion.FOLLOWER_ID
import coadjute.functions.FlowFunctions
import coadjute.states.job.FollowerState
import net.corda.core.contracts.Command
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@InitiatingFlow
@StartableByRPC
class CreateFollowerFlow (private val jobId: String,
                          private val collaboratorId: String,
                          private val userId: String?): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        var signedTransaction = verifyAndSign(transaction())
        val job = getJobRefByLinearId(jobId).state.data
        val sessions = (job.participants - ourIdentity).map { initiateFlow(it) }
        return if (getAllCollaboratorsByProjectId(job.projectId) != null)
        {
            signedTransaction = subFlow(CollectSignaturesFlow(signedTransaction, sessions))
            subFlow(FinalityFlow(signedTransaction, sessions))
        }
        else
        {
            subFlow(FinalityFlow(signedTransaction, listOf()))
        }
    }

    private fun outState(): FollowerState
    {
        val job = getJobRefByLinearId(jobId).state.data
        var participants = mutableListOf<Party>()
        if (getAllCollaboratorsByProjectId(job.projectId) != null)
        {
            val projectCollaborator = getAllCollaboratorsByProjectId(job.projectId)!!.map { it.state.data }
            projectCollaborator.map {
                participants.add(stringToParty(it.nodeO))
            }
        }
        else
        {
            participants = mutableListOf(ourIdentity)
        }

        return FollowerState(
                jobId = jobId,
                collaboratorId = collaboratorId,
                userId = userId,
                linearId = UniqueIdentifier(),
                participants = participants
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val cmd = Command(FollowerContract.Commands.Create(), outState().participants.map { it.owningKey })
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addOutputState(outState(), FOLLOWER_ID)
        builder.addCommand(cmd)
        return builder
    }
}

@InitiatedBy(CreateFollowerFlow::class)
class CreateFollowerFlowResponder(private val flowSession: FlowSession): FlowLogic<SignedTransaction>()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        subFlow(object : SignTransactionFlow(flowSession)
        {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
            }
        })
        return subFlow(ReceiveFinalityFlow(otherSideSession = flowSession))
    }
}