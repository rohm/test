package coadjute.flows.job

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.job.JobContract
import coadjute.contracts.job.JobContract.Companion.JOB_ID
import coadjute.functions.FlowFunctions
import coadjute.states.job.JobState
import net.corda.core.contracts.Command
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.contracts.requireThat
import net.corda.core.crypto.SecureHash
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@InitiatingFlow
@StartableByRPC
class CreateJobFlow (private val name: String,
                    private val projectId: String,
                    private val status: String,
                    private val action: String,
                    private val instruction: String?,
                    private val artifactBit: Boolean,
                    private val url: String?,
                    private val fileSize: Double?,
                    private val hash: String?,
                    private val remarks: String?,
                    private val referenceJobId: String?,
                    private val projectStageId: String?,
                    private val startAt: String?,
                    private val endAt: String?,
                    private val responsibleCollaboratorId: String?,
                    private val responsibleUserId: String?,
                    private val responsibleAcceptedAt: String?,
                    private val ownerCollaboratorId: String?,
                    private val ownerUserId: String?,
                    private val ownerAcceptedAt: String?,
                    private val creatorCollaboratorId: String,
                    private val creatorUserId: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        var signedTransaction = verifyAndSign(transaction())
        return if (getAllCollaboratorsByProjectId(projectId) != null)
        {
            val listOfCollaboratorStateRef = getAllCollaboratorsByProjectId(projectId)!!
            val listOfCollaboratorState = listOfCollaboratorStateRef.map { it.state.data }
            val participants = listOfCollaboratorState.map { stringToParty(it.nodeO) } - ourIdentity
            val sessions = participants.map { initiateFlow(it) }
            signedTransaction = subFlow(CollectSignaturesFlow(signedTransaction, sessions))
            subFlow(FinalityFlow(signedTransaction, sessions))
        }
        else
        {
            subFlow(FinalityFlow(signedTransaction, listOf()))
        }

    }

    private fun outState(): JobState
    {
        var participants = mutableListOf<Party>()
        if (getAllCollaboratorsByProjectId(projectId) != null)
        {
            val projectCollaborator = getAllCollaboratorsByProjectId(projectId)!!.map { it.state.data }
            projectCollaborator.map {
                participants.add(stringToParty(it.nodeO))
            }
        }
        else
        {
            participants = mutableListOf(ourIdentity)
        }

        return JobState(
                name = name,
                projectId = projectId,
                action = action,
                status = status,
                instruction = instruction,
                artifactBit = artifactBit,
                url = url,
                fileSize = fileSize,
                hash = formatSecureHash(hash),
                remarks = remarks,
                referenceJobId = referenceJobId,
                projectStageId = projectStageId,
                startAt = parseInstantToString(startAt),
                endAt = parseInstantToString(endAt),
                responsibleCollaboratorId = responsibleCollaboratorId,
                responsibleUserId = responsibleUserId,
                responsibleAcceptedAt = parseInstantToString(responsibleAcceptedAt),
                ownerCollaboratorId = ownerCollaboratorId,
                ownerUserId = ownerUserId,
                ownerAcceptedAt = parseInstantToString(ownerAcceptedAt),
                creatorCollaboratorId = creatorCollaboratorId,
                creatorUserId = creatorUserId,
                linearId = UniqueIdentifier(),
                participants = participants
        )
    }

    private fun transaction(): TransactionBuilder
    {
        if (getAllCollaboratorsByProjectId(projectId) != null)
        {
            val cmd = Command(JobContract.Commands.Share(), outState().participants.map { it.owningKey })
            val notary = serviceHub.networkMapCache.notaryIdentities.first()
            val builder = TransactionBuilder(notary = notary)
            builder.addOutputState(outState(), JOB_ID)
            builder.addCommand(cmd)
            return builder
        }
        else
        {
            val cmd = Command(JobContract.Commands.Create(), ourIdentity.owningKey)
            val notary = serviceHub.networkMapCache.notaryIdentities.first()
            val builder = TransactionBuilder(notary = notary)
            builder.addOutputState(outState(), JOB_ID)
            builder.addCommand(cmd)
            return builder
        }
    }
}

@InitiatedBy(CreateJobFlow::class)
class CreateJobFlowResponder(private val flowSession: FlowSession): FlowLogic<SignedTransaction>()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        subFlow(object : SignTransactionFlow(flowSession)
        {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
            }
        })
        return subFlow(ReceiveFinalityFlow(otherSideSession = flowSession))
    }
}

@InitiatingFlow
@StartableByRPC
class UpdateJobFlow (private val name: String,
                     private val status: String,
                     private val action: String,
                     private val referenceJobId: String?,
                     private val instruction: String?,
                     private val url: String?,
                     private val fileSize: Double?,
                     private val hash: String?,
                     private val remarks: String?,
                     private val projectStageId: String?,
                     private val startAt: String?,
                     private val endAt: String?,
                     private val responsibleCollaboratorId: String?,
                     private val responsibleUserId: String?,
                     private val responsibleAcceptedAt: String?,
                     private val ownerCollaboratorId: String?,
                     private val ownerUserId: String?,
                     private val ownerAcceptedAt: String?,
                     private val jobId: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        var signedTransaction = verifyAndSign(transaction())
        val job = getJobRefByLinearId(jobId).state.data
        val sessions = (job.participants - ourIdentity).map { initiateFlow(it) }
        return if (getAllCollaboratorsByProjectId(job.projectId) != null)
        {
            signedTransaction = subFlow(CollectSignaturesFlow(signedTransaction, sessions))
            subFlow(FinalityFlow(signedTransaction, sessions))
        }
        else
        {
            subFlow(FinalityFlow(signedTransaction, listOf()))
        }
    }

    private fun outState(): JobState
    {
        val job = getJobRefByLinearId(jobId).state.data
        return JobState(
                name = name,
                projectId = job.projectId,
                status = status,
                action = action,
                instruction = instruction,
                artifactBit = job.artifactBit,
                url = url,
                fileSize = fileSize,
                hash = formatSecureHash(hash),
                remarks = remarks,
                referenceJobId = referenceJobId,
                projectStageId = projectStageId,
                startAt = parseInstantToString(startAt),
                endAt = parseInstantToString(endAt),
                responsibleCollaboratorId = responsibleCollaboratorId,
                responsibleUserId = responsibleUserId,
                responsibleAcceptedAt = parseInstantToString(responsibleAcceptedAt),
                ownerCollaboratorId = ownerCollaboratorId,
                ownerUserId = ownerUserId,
                ownerAcceptedAt = parseInstantToString(ownerAcceptedAt),
                creatorCollaboratorId = job.creatorCollaboratorId,
                creatorUserId = job.creatorUserId,
                linearId = job.linearId,
                participants = job.participants
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val jobStateRef = getJobRefByLinearId(jobId)
        val jobState = getJobRefByLinearId(jobId).state.data
        val cmd = Command(JobContract.Commands.Update(), jobState.participants.map { it.owningKey })
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addInputState(jobStateRef)
        builder.addOutputState(outState(), JOB_ID)
        builder.addCommand(cmd)
        return builder
    }
}

@InitiatedBy(UpdateJobFlow::class)
class UpdateJobFlowResponder(private val flowSession: FlowSession): FlowLogic<SignedTransaction>()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        subFlow(object : SignTransactionFlow(flowSession)
        {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
            }
        })
        return subFlow(ReceiveFinalityFlow(otherSideSession = flowSession))
    }
}