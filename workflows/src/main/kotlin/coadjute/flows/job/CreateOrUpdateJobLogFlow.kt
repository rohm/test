package coadjute.flows.job

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.job.JobLogContract
import coadjute.contracts.job.JobLogContract.Companion.JOBLOG_ID
import coadjute.functions.FlowFunctions
import coadjute.states.job.JobLogState
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import java.security.PublicKey
import java.time.Instant

@InitiatingFlow
@StartableByRPC
class CreateOrUpdateJobLogFlow(private val jobId: String,
                               private val content: String,
                               private val action: String,
                               private val splitJobIds: List<String>?,
                               private val mergeFromJobIds: List<String>?,
                               private val updatedByCollaboratorId: String?,
                               private val updatedByUserId: String?,
                               private val updatedByUserFirstName: String?,
                               private val updatedByUserMiddleName: String?,
                               private val updatedByUserLastName: String?): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        var signedTransaction = verifyAndSign(transaction())
        val job = getJobRefByLinearId(jobId).state.data
        val sessions = (job.participants - ourIdentity).map { initiateFlow(it) }
        return if (getAllCollaboratorsByProjectId(job.projectId) != null)
        {
            signedTransaction = subFlow(CollectSignaturesFlow(signedTransaction, sessions))
            subFlow(FinalityFlow(signedTransaction, sessions))
        }
        else
        {
            subFlow(FinalityFlow(signedTransaction, listOf()))
        }
    }

    private fun outState(jobLogStateRef : StateAndRef<JobLogState>?): JobLogState
    {
        val job = getJobRefByLinearId(jobId).state.data
        var participants = mutableListOf<Party>()
        if (getAllCollaboratorsByProjectId(job.projectId) != null)
        {
            val projectCollaborator = getAllCollaboratorsByProjectId(job.projectId)!!.map { it.state.data }
            projectCollaborator.map {
                participants.add(stringToParty(it.nodeO))
            }
        }
        else
        {
            participants = mutableListOf(ourIdentity)
        }

        if(jobLogStateRef == null)
        {
            return JobLogState(
                    jobId = jobId,
                    content = content,
                    action = action,
                    splitJobIds = splitJobIds,
                    mergeFromJobIds = mergeFromJobIds,
                    updatedByCollaboratorId = updatedByCollaboratorId,
                    updatedByUserId = updatedByUserId,
                    updatedByUserFirstName = updatedByUserFirstName,
                    updatedByUserMiddleName = updatedByUserMiddleName,
                    updatedByUserLastName = updatedByUserLastName,
                    updatedAt = Instant.now(),
                    linearId = UniqueIdentifier(),
                    participants = participants
            )
        }

        else
        {
            return JobLogState(
                    jobId = jobLogStateRef.state.data.jobId,
                    content = content,
                    action = action,
                    splitJobIds = splitJobIds,
                    mergeFromJobIds = mergeFromJobIds,
                    updatedByCollaboratorId = updatedByCollaboratorId,
                    updatedByUserId = updatedByUserId,
                    updatedByUserFirstName = updatedByUserFirstName,
                    updatedByUserMiddleName = updatedByUserMiddleName,
                    updatedByUserLastName = updatedByUserLastName,
                    updatedAt = Instant.now(),
                    linearId = jobLogStateRef.state.data.linearId,
                    participants = jobLogStateRef.state.data.participants
            )
        }

    }

    private fun transaction(): TransactionBuilder
    {
        val jobLogRef = getJobLogRefByJobId(jobId)
        val job = getJobRefByLinearId(jobId).state.data
        val owningKeys: List<PublicKey>
        owningKeys = if (getAllCollaboratorsByProjectId(job.projectId) != null)
        {
            outState(jobLogRef).participants.map { it.owningKey }
        }
        else
        {
            listOf(ourIdentity.owningKey)
        }

        val command = Command(JobLogContract.Commands.CreateOrUpdate(), owningKeys)
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        if (jobLogRef != null)
        {
            builder.addInputState(jobLogRef)
        }
        builder.addOutputState(outState(jobLogRef), JOBLOG_ID)
        builder.addCommand(command)
        return builder

    }
}

@InitiatedBy(CreateOrUpdateJobLogFlow::class)
class CreateOrUpdateJobLogFlowResponder(private val flowSession: FlowSession): FlowLogic<SignedTransaction>()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        subFlow(object : SignTransactionFlow(flowSession)
        {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
            }
        })
        return subFlow(ReceiveFinalityFlow(otherSideSession = flowSession))
    }
}