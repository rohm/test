package coadjute.flows.project

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.project.NotificationContract
import coadjute.functions.FlowFunctions
import coadjute.states.project.NotificationState
import net.corda.core.contracts.Command
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import java.time.Instant

@StartableByRPC
class NotificationFlow (private val name: String,
                        private val content: String,
                        private val recipientCollaboratorId: String?,
                        private val recipientUserId: String,
                        private val sentAt: Instant?,
                        private val senderCollaboratorId: String?,
                        private val senderUserId: String?,
                        private val projectId: String?,
                        private val jobId: String?): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val stx = verifyAndSign(transaction())
        return subFlow(FinalityFlow(stx, listOf()))

    }


    private fun outState(): NotificationState
    {

        return NotificationState(
                name = name,
                content = content,
                recipientCollaboratorId = recipientCollaboratorId,
                recipientUserId = recipientUserId,
                sentAt = sentAt,
                senderCollaboratorId = senderCollaboratorId,
                senderUserId = senderUserId,
                projectId = projectId,
                jobId = jobId,
                linearId = UniqueIdentifier(),
                participants = listOf(ourIdentity)
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val cmd = Command(NotificationContract.Commands.Create(), ourIdentity.owningKey)
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addOutputState(outState(), NotificationContract.USER_ID)
        builder.addCommand(cmd)
        return builder
    }
}