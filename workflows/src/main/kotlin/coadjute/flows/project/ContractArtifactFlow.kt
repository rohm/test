package coadjute.flows.project

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.project.ContractArtifactContract
import coadjute.contracts.project.ContractArtifactContract.Companion.CARTIFACT_ID
import coadjute.functions.FlowFunctions
import coadjute.states.project.ContractArtifactState
import net.corda.core.contracts.Command
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.crypto.SecureHash
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@StartableByRPC
class CreateContractArtifactFlow (private val name: String,
                                private val contractId: String,
                                private val description: String?,
                                private val url: String?,
                                private val fileSize: Double?,
                                private val hash: SecureHash?): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val signedTransaction = verifyAndSign(transaction())
        return subFlow(FinalityFlow(signedTransaction, listOf()))
    }

    private fun outState(): ContractArtifactState
    {
        return ContractArtifactState(
                name = name,
                contractId = contractId,
                description = description,
                url = url,
                fileSize = fileSize,
                hash = hash,
                linearId = UniqueIdentifier(),
                participants = listOf(ourIdentity)
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val cmd = Command(ContractArtifactContract.Commands.Create(), ourIdentity.owningKey)
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addOutputState(outState(), CARTIFACT_ID)
        builder.addCommand(cmd)
        return builder
    }
}

@StartableByRPC
class UpdateContractArtifactFlow (private val name: String,
                                  private val description: String?,
                                  private val url: String?,
                                  private val fileSize: Double?,
                                  private val hash: SecureHash?,
                                    private val contactArtifactId: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val signedTransaction = verifyAndSign(transaction())
        return subFlow(FinalityFlow(signedTransaction, listOf()))
    }

    private fun outState(): ContractArtifactState
    {
        val cArtifact = getContractArtifactRefByLinearId(contactArtifactId).state.data
        return ContractArtifactState(
                name = name,
                contractId = cArtifact.contractId,
                description = description,
                url = url,
                fileSize = fileSize,
                hash = hash,
                linearId = cArtifact.linearId,
                participants = cArtifact.participants
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val cArtifactStateRef = getContractArtifactRefByLinearId(contactArtifactId)
        val cmd = Command(ContractArtifactContract.Commands.Update(), ourIdentity.owningKey)
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addInputState(cArtifactStateRef)
        builder.addOutputState(outState(), CARTIFACT_ID)
        builder.addCommand(cmd)
        return builder
    }
}