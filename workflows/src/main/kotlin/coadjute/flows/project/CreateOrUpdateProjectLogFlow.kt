package coadjute.flows.project

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.project.ProjectLogContract
import coadjute.contracts.project.ProjectLogContract.Companion.PLCONTRACT_ID
import coadjute.functions.FlowFunctions
import coadjute.functions.ShareFlow
import coadjute.states.project.ProjectLogState
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import java.time.Instant

@StartableByRPC
class CreateOrUpdateProjectLogFlow (private val projectId: String,
                                    private val content: String,
                                    private val updatedBy: String?): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val signedTransaction = verifyAndSign(transaction())
        if (getAllCollaboratorsByProjectId(projectId) != null)
        {
            val listOfCollaboratorStateRef = getAllCollaboratorsByProjectId(projectId)!!
            val listOfCollaboratorState = listOfCollaboratorStateRef.map { it.state.data }
            val participants = listOfCollaboratorState.map { stringToParty(it.nodeO) } - ourIdentity
            return subFlow(FinalityFlow(signedTransaction, listOf())).also {
                if (participants.isNotEmpty())
                {
                    for (parties in participants)
                        subFlow(ShareFlow(parties, it))
                }
            }
        }
        else
        {
            return subFlow(FinalityFlow(signedTransaction, listOf()))
        }
    }

    private fun outState(projectLogStateRef : StateAndRef<ProjectLogState>?): ProjectLogState
    {
        if(projectLogStateRef == null)
        {
            return ProjectLogState(
                    projectId = projectId,
                    content = content,
                    updatedBy = updatedBy,
                    updatedAt = Instant.now(),
                    linearId = UniqueIdentifier(),
                    participants = listOf(ourIdentity)
            )
        }
        else
        {
            return ProjectLogState(
                    projectId = projectLogStateRef.state.data.projectId,
                    content = content,
                    updatedBy = updatedBy,
                    updatedAt = Instant.now(),
                    linearId = projectLogStateRef.state.data.linearId,
                    participants = projectLogStateRef.state.data.participants
            )
        }

    }

    private fun transaction(): TransactionBuilder
    {
        val projectLogRef = getProjectLogRefByProjectId(projectId)
        val command = Command(ProjectLogContract.Commands.CreateOrUpdate(), ourIdentity.owningKey)
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)

        if (projectLogRef != null)
        {
            builder.addInputState(projectLogRef)
        }
        builder.addOutputState(outState(projectLogRef), PLCONTRACT_ID)
        builder.addCommand(command)
        return builder
    }
}