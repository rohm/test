package coadjute.flows.project

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.project.*
import coadjute.contracts.project.ProjectContract.Companion.PROJECT_ID
import coadjute.functions.FlowFunctions
import coadjute.functions.ShareFlow
import coadjute.states.project.*
import net.corda.core.contracts.Command
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@InitiatingFlow
@StartableByRPC
class UpdateProjectFlow (private val name: String,
                         private val organizationId: String,
                         private val startAt: String?,
                         private val endAt: String?,
                         private val budgetCurrency: String?,
                         private val budgetAmount: Double?,
                         private val image: String?,
                         private val industryType: String,
                         private val contractId: String?,
                         private val processId: String?,
                         private val programId: String?,
                         private val status: String,
                         private val projectId: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val signedTransaction = verifyAndSign(transaction())
        if (getAllCollaboratorsByProjectId(projectId)!!.isNotEmpty())
        {
            val listOfCollaboratorStateRef = getAllCollaboratorsByProjectId(projectId)!!
            val listOfCollaboratorState = listOfCollaboratorStateRef.map { it.state.data }
            val participants = listOfCollaboratorState.map { stringToParty(it.nodeO) } - ourIdentity
            println(participants)
            return subFlow(FinalityFlow(signedTransaction, listOf())).also {
                if (participants.isNotEmpty())
                {
                    for (parties in participants)
                        subFlow(ShareFlow(parties, it))
                }
            }
        }
        else
        {
            return subFlow(FinalityFlow(signedTransaction, listOf()))
        }
    }

    private fun outState(): ProjectState
    {
        /**
         * TODO - check contractId, programId, processId
         */
        val project = getProjectRefByLinearId(projectId).state.data
        return ProjectState(
                name = name,
                organizationId = organizationId,
                startAt = parseInstantToString(startAt),
                endAt = parseInstantToString(endAt),
                budgetCurrency = budgetCurrency,
                budgetAmount = formatDouble(budgetAmount),
                image = image,
                industryType = industryType,
                contractId = contractId,
                processId = processId,
                programId = programId,
                status = status.toUpperCase(),
                linearId = project.linearId,
                participants = project.participants
        )
    }

    private fun contractState(): ProjectContractState
    {
        val contract = getProjectContractRefByLinearId(contractId!!).state.data
        return ProjectContractState(
                name = contract.name,
                linearId = contract.linearId,
                participants = contract.participants
        )
    }

    private fun projectStagesState(): List<ProjectStageState>
    {
        val projectStage = getProjectStagesRefByProjectId(projectId)!!.map { it.state.data }
        return projectStage.map {
            ProjectStageState(
                    projectId = it.projectId,
                    processStageId = it.processStageId,
                    name = it.name,
                    gateBit = it.gateBit,
                    gateStageId = it.gateStageId,
                    description = it.description,
                    order = it.order,
                    linearId = it.linearId,
                    participants = it.participants
            )
        }
    }

    private fun processState(): ProcessState
    {
        val process = getProcessRefByLinearId(processId!!).state.data
        return ProcessState(
                name = process.name,
                linearId = process.linearId,
                participants = process.participants
        )
    }

    private fun processStagesState(): List<ProcessStageState>
    {
        val processStage = getProcessStagesRefByProjectId(projectId)!!.map { it.state.data }
        return processStage.map {
            ProcessStageState(
                    projectId = it.projectId,
                    processId = it.processId,
                    name = it.name,
                    gateBit = it.gateBit,
                    gateStageId = it.gateStageId,
                    description = it.description,
                    order = it.order,
                    linearId = it.linearId,
                    participants = it.participants
            )
        }
    }

    private fun programState(): ProgramState
    {
        val program = getProgramRefByLinearId(programId!!).state.data
        return ProgramState(
                name = program.name,
                description = program.description,
                linearId = program.linearId,
                participants = program.participants
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val projectStateRef = getProjectRefByLinearId(projectId)
        val cmd = Command(ProjectContract.Commands.Update(), ourIdentity.owningKey)
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)

        // Project
        builder.addInputState(projectStateRef)
        builder.addOutputState(outState(), PROJECT_ID)
        builder.addCommand(cmd)

        // Project Contract
        if (outState().contractId != null)
        {
            val contractRef = getProjectContractRefByLinearId(outState().contractId!!)
            val contractCmd = Command(ProjectContractContract.Commands.Share(), contractState().participants.map { it.owningKey })
            builder.addInputState(contractRef)
            builder.addOutputState(contractState(), ProjectContractContract.PROJECTCONTRACT_ID)
            builder.addCommand(contractCmd)
        }

        // Project Stage
        if (getProjectStagesRefByProjectId(projectId) != null)
        {
            val projectStageRef = getProjectStagesRefByProjectId(projectId)!!
            val projectStageCmd = Command(ProjectStageContract.Commands.Share(), ourIdentity.owningKey)

            projectStageRef.map { builder.addInputState(it) }
            projectStagesState().map { builder.addOutputState(it, ProjectStageContract.PROJSCONTRACT_ID) }
            builder.addCommand(projectStageCmd)
        }

        // Process
        if (outState().processId != null)
        {
            val processRef = getProcessRefByLinearId(outState().processId!!)
            val processCmd = Command(ProcessContract.Commands.Share(), processState().participants.map { it.owningKey })
            builder.addInputState(processRef)
            builder.addOutputState(processState(), ProcessContract.PROCESS_ID)
            builder.addCommand(processCmd)
        }

        // Process Stages
        if (getProcessStagesRefByProjectId(projectId) != null)
        {
            val processStageRef = getProcessStagesRefByProjectId(projectId)!!
            val processStageCmd = Command(ProcessStageContract.Commands.Share(), ourIdentity.owningKey)

            processStageRef.map { builder.addInputState(it) }
            processStagesState().map { builder.addOutputState(it, ProcessStageContract.PROCSCONTRACT_ID) }
            builder.addCommand(processStageCmd)
        }

        // Program
        if (outState().programId != null)
        {
            val programRef = getProgramRefByLinearId(outState().programId!!)
            val programCmd = Command(ProgramContract.Commands.Share(), programState().participants.map { it.owningKey })
            builder.addInputState(programRef)
            builder.addOutputState(programState(), ProgramContract.PROGRAM_ID)
            builder.addCommand(programCmd)
        }

        return builder
    }
}

@InitiatedBy(UpdateProjectFlow::class)
class UpdateProjectFlowResponder(private val flowSession: FlowSession): FlowLogic<SignedTransaction>()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        subFlow(object : SignTransactionFlow(flowSession)
        {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
            }
        })
        return subFlow(ReceiveFinalityFlow(otherSideSession = flowSession))
    }
}