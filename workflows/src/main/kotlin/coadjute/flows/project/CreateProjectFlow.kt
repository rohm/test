package coadjute.flows.project

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.project.ProjectContract
import coadjute.contracts.project.ProjectContract.Companion.PROJECT_ID
import coadjute.functions.FlowFunctions
import coadjute.states.project.ProjectState
import net.corda.core.contracts.Command
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@StartableByRPC
class CreateProjectFlow (private val name: String,
                         private val organizationId: String,
                         private val startAt: String?,
                         private val endAt: String?,
                         private val budgetCurrency: String?,
                         private val budgetAmount: Double?,
                         private val image: String?,
                         private val industryType: String,
                         private val contractId: String?,
                         private val processId: String?,
                         private val programId: String?,
                         private val status: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val signedTransaction = verifyAndSign(transaction())
        return subFlow(FinalityFlow(signedTransaction, listOf()))
    }

    private fun outState(): ProjectState
    {
        return ProjectState(
                name = name,
                organizationId = organizationId,
                startAt = parseInstantToString(startAt),
                endAt = parseInstantToString(endAt),
                budgetCurrency = budgetCurrency,
                budgetAmount = formatDouble(budgetAmount),
                image = image,
                industryType = industryType,
                contractId = contractId,
                processId = processId,
                programId = programId,
                status = status.toUpperCase(),
                linearId = UniqueIdentifier(),
                participants = listOf(ourIdentity)
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val cmd = Command(ProjectContract.Commands.Create(), listOf(ourIdentity.owningKey))
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addOutputState(outState(), PROJECT_ID)
        builder.addCommand(cmd)
        return builder
    }
}