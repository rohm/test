package coadjute.flows.project

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.project.ProjectStageContract
import coadjute.contracts.project.ProjectStageContract.Companion.PROJSCONTRACT_ID
import coadjute.functions.FlowFunctions
import coadjute.functions.ShareFlow
import coadjute.states.project.ProjectStageState
import net.corda.core.contracts.Command
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@InitiatingFlow
@StartableByRPC
class UpdateProjectStageFlow (private val name: String,
                              private val description: String?,
                              private val order: Int?,
                              private val projectStageId: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val projectStage = getProjectStageRefByLinearId(projectStageId).state.data
        val listOfCollaboratorStateRef = getAllCollaboratorsByProjectId(projectStage.projectId)!!
        val listOfCollaboratorState = listOfCollaboratorStateRef.map { it.state.data }
        val participants = listOfCollaboratorState.map { stringToParty(it.nodeO) } - ourIdentity

        val signedTransaction = verifyAndSign(transaction())
        return subFlow(FinalityFlow(signedTransaction, listOf())).also {
            if (participants.isNotEmpty())
            {
                for (parties in participants)
                    subFlow(ShareFlow(parties, it))
            }
        }
    }

    private fun outState(): ProjectStageState
    {
        val projectStage = getProjectStageRefByLinearId(projectStageId).state.data
        return ProjectStageState(
                projectId = projectStage.projectId,
                processStageId = projectStage.processStageId,
                name = name,
                description = description,
                order = order,
                gateBit = projectStage.gateBit,
                gateStageId = projectStage.gateStageId,
                linearId = projectStage.linearId,
                participants = projectStage.participants
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val projectStageStateRef = getProjectStageRefByLinearId(projectStageId)
        val cmd = Command(ProjectStageContract.Commands.Update(), ourIdentity.owningKey)
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addInputState(projectStageStateRef)
        builder.addOutputState(outState(), PROJSCONTRACT_ID)
        builder.addCommand(cmd)
        return builder
    }
}

@InitiatedBy(UpdateProjectStageFlow::class)
class UpdateProjectStageFlowResponder(private val flowSession: FlowSession): FlowLogic<SignedTransaction>()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        subFlow(object : SignTransactionFlow(flowSession)
        {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
            }
        })
        return subFlow(ReceiveFinalityFlow(otherSideSession = flowSession))
    }
}