package coadjute.flows.project

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.project.ContractObligationContract
import coadjute.contracts.project.ContractObligationContract.Companion.COBLIGATION_ID
import coadjute.functions.FlowFunctions
import coadjute.functions.ShareFlow
import coadjute.states.project.ContractObligationState
import net.corda.core.contracts.Command
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@StartableByRPC
class CreateContractObligationFlow (private val contractId: String,
                                    private val name: String,
                                    private val description: String?): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val signedTransaction = verifyAndSign(transaction())
        val projectContract = getProjectContractRefByLinearId(contractId).state.data
        if (getProjectRefByContractId(projectContract.linearId.toString()) != null)
        {
            val project = getProjectRefByContractId(projectContract.linearId.toString())!!.state.data
            val listOfCollaboratorStateRef = getAllCollaboratorsByProjectId(project.linearId.toString())!!
            val listOfCollaboratorState = listOfCollaboratorStateRef.map { it.state.data }
            val participants = listOfCollaboratorState.map { stringToParty(it.nodeO) } - ourIdentity
            return subFlow(FinalityFlow(signedTransaction, listOf())).also {
                if (participants.isNotEmpty())
                {
                    for (parties in participants)
                        subFlow(ShareFlow(parties, it))
                }
            }
        }
        else
        {
            return subFlow(FinalityFlow(signedTransaction, listOf()))
        }
    }

    private fun outState(): ContractObligationState
    {
        return ContractObligationState(
                contractId = contractId,
                name = name,
                description = description,
                linearId = UniqueIdentifier(),
                participants = listOf(ourIdentity)
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val cmd = Command(ContractObligationContract.Commands.Create(), ourIdentity.owningKey)
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addOutputState(outState(), COBLIGATION_ID)
        builder.addCommand(cmd)
        return builder
    }
}