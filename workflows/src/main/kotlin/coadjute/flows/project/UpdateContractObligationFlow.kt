package coadjute.flows.project

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.project.ContractObligationContract
import coadjute.contracts.project.ContractObligationContract.Companion.COBLIGATION_ID
import coadjute.functions.FlowFunctions
import coadjute.functions.ShareFlow
import coadjute.states.project.ContractObligationState
import net.corda.core.contracts.Command
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@StartableByRPC
class UpdateContractObligationFlow (private val name: String,
                                    private val description: String?,
                                    private val contractObligationId: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val contractObligation = getContractObligationRefByLinearId(contractObligationId).state.data
        var stx = verifyAndSign(transaction())
        if (getProjectRefByContractId(contractObligation.contractId) != null)
        {
            val project = getProjectRefByContractId(contractObligation.contractId)!!.state.data
            val listOfCollaboratorStateRef = getAllCollaboratorsByProjectId(project.linearId.toString())!!
            val listOfCollaboratorState = listOfCollaboratorStateRef.map { it.state.data }
            val participants = listOfCollaboratorState.map { stringToParty(it.nodeO) } - ourIdentity
            stx = subFlow(FinalityFlow(stx, listOf())).also {
                if (participants.isNotEmpty())
                {
                    for (parties in participants)
                        subFlow(ShareFlow(parties, it))
                }
            }
        }
        return stx
    }

    private fun outState(): ContractObligationState
    {
        val contractObligation = getContractObligationRefByLinearId(contractObligationId).state.data
        return ContractObligationState(
                contractId = contractObligation.contractId,
                name = name,
                description = description,
                linearId = contractObligation.linearId,
                participants = contractObligation.participants
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val contractObligationStateRef = getContractObligationRefByLinearId(contractObligationId)
        val cmd = Command(ContractObligationContract.Commands.Update(), ourIdentity.owningKey)
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addInputState(contractObligationStateRef)
        builder.addOutputState(outState(), COBLIGATION_ID)
        builder.addCommand(cmd)
        return builder
    }
}