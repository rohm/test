package coadjute.flows.project

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.project.ProcessStageContract
import coadjute.contracts.project.ProcessStageContract.Companion.PROCSCONTRACT_ID
import coadjute.functions.FlowFunctions
import coadjute.functions.ShareFlow
import coadjute.states.project.ProcessStageState
import net.corda.core.contracts.Command
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@StartableByRPC
class UpdateProcessStageFlow (private val name: String,
                              private val description: String?,
                              private val order: Int?,
                              private val processStageId: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val processStage = getProcessStageRefByLinearId(processStageId).state.data
        val listOfCollaboratorStateRef = getAllCollaboratorsByProjectId(processStage.projectId)!!
        val listOfCollaboratorState = listOfCollaboratorStateRef.map { it.state.data }
        val participants = listOfCollaboratorState.map { stringToParty(it.nodeO) } - ourIdentity

        val signedTransaction = verifyAndSign(transaction())
        return subFlow(FinalityFlow(signedTransaction, listOf())).also {
            if (participants.isNotEmpty())
            {
                for (parties in participants)
                    subFlow(ShareFlow(parties, it))
            }
        }
    }

    private fun outState(): ProcessStageState
    {
        val processStage = getProcessStageRefByLinearId(processStageId).state.data
        return ProcessStageState(
                projectId = processStage.projectId,
                processId = processStage.processId,
                name = name,
                description = description,
                order = order,
                gateBit = processStage.gateBit,
                gateStageId = processStage.gateStageId,
                linearId = processStage.linearId,
                participants = processStage.participants
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val processStageStateRef = getProcessStageRefByLinearId(processStageId)
        val cmd = Command(ProcessStageContract.Commands.Update(), ourIdentity.owningKey)
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addInputState(processStageStateRef)
        builder.addOutputState(outState(), PROCSCONTRACT_ID)
        builder.addCommand(cmd)
        return builder
    }
}