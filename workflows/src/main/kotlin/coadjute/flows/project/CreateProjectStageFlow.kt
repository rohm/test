package coadjute.flows.project

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.project.ProjectStageContract
import coadjute.contracts.project.ProjectStageContract.Companion.PROJSCONTRACT_ID
import coadjute.functions.FlowFunctions
import coadjute.functions.ShareFlow
import coadjute.states.project.ProjectStageState
import net.corda.core.contracts.Command
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@StartableByRPC
class CreateProjectStageFlow (private val projectId: String,
                              private val processStageId: String?,
                              private val name: String,
                              private val gateBit: Boolean,
                              private val gateStageId: String?,
                              private val description: String?,
                              private val order: Int?): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        var signedTransaction = verifyAndSign(transaction())
        if (getProjectStagesRefByProjectId(projectId) != null)
        {
            val listOfCollaboratorStateRef = getAllCollaboratorsByProjectId(projectId)!!
            val listOfCollaboratorState = listOfCollaboratorStateRef.map { it.state.data }
            val participants = listOfCollaboratorState.map { stringToParty(it.nodeO) } - ourIdentity
            signedTransaction = subFlow(FinalityFlow(signedTransaction, listOf())).also {
                if (participants.isNotEmpty())
                {
                    for (parties in participants)
                        subFlow(ShareFlow(parties, it))
                }
            }
            return signedTransaction
        }
        else
        {
            return subFlow(FinalityFlow(signedTransaction, listOf()))
        }
    }

    private fun outState(): ProjectStageState
    {
        return ProjectStageState(
                projectId = projectId,
                processStageId = processStageId,
                name = name,
                gateBit = gateBit,
                gateStageId = gateStageId,
                description = description,
                order = order,
                linearId = UniqueIdentifier(),
                participants = listOf(ourIdentity)
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val cmd = Command(ProjectStageContract.Commands.Create(), ourIdentity.owningKey)
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addOutputState(outState(), PROJSCONTRACT_ID)
        builder.addCommand(cmd)
        return builder
    }
}