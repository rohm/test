package coadjute.flows.project

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.project.ProjectContractContract
import coadjute.contracts.project.ProjectContractContract.Companion.PROJECTCONTRACT_ID
import coadjute.functions.FlowFunctions
import coadjute.states.project.ProjectContractState
import net.corda.core.contracts.Command
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@StartableByRPC
class CreateProjectContractFlow (private val name: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val signedTransaction = verifyAndSign(transaction())
        return subFlow(FinalityFlow(signedTransaction, listOf()))
    }

    private fun outState(): ProjectContractState
    {
        return ProjectContractState(
                name = name,
                linearId = UniqueIdentifier(),
                participants = listOf(ourIdentity)
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val cmd = Command(ProjectContractContract.Commands.Create(), ourIdentity.owningKey)
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addOutputState(outState(), PROJECTCONTRACT_ID)
        builder.addCommand(cmd)
        return builder
    }
}