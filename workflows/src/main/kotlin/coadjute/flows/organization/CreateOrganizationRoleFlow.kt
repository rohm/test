package coadjute.flows.organization

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.organization.OrganizationRoleContract
import coadjute.contracts.organization.OrganizationRoleContract.Companion.ORGROLE_ID
import coadjute.functions.FlowFunctions
import coadjute.states.organization.OrganizationRoleState
import net.corda.core.contracts.Command
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@StartableByRPC
class CreateOrganizationRoleFlow (private val name: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val signedTransaction = verifyAndSign(transaction())
        return subFlow(FinalityFlow(signedTransaction, listOf()))
    }

    private fun outState(): OrganizationRoleState
    {
        return OrganizationRoleState(
                name = name,
                linearId = UniqueIdentifier(),
                participants = listOf(ourIdentity)
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val cmd = Command(OrganizationRoleContract.Commands.Register(), ourIdentity.owningKey)
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addOutputState(outState(), ORGROLE_ID)
        builder.addCommand(cmd)
        return builder
    }
}