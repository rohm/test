package coadjute.flows.organization

import coadjute.functions.*
import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.organization.OrganizationContract
import coadjute.contracts.organization.OrganizationContract.Companion.ORG_ID
import coadjute.states.organization.OrganizationState
import net.corda.core.contracts.Command
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.*
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import java.time.Instant

@StartableByRPC
class CreateOrganizationFlow(private val name: String,
                             private val email: String,
                             private val contactNumber: String?,
                             private val addressLine1: String?,
                             private val addressLine2: String?,
                             private val town: String?,
                             private val city: String?,
                             private val province: String?,
                             private val state: String?,
                             private val country: String?,
                             private val postalCode: String?,
                             private val image: String?,
                             private val organizationRoleId: String,
                             private val encryptKey: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val signedTransaction = verifyAndSign(transaction())
        return subFlow(FinalityFlow(signedTransaction, listOf()))
    }

    private fun outState(): OrganizationState
    {
        return OrganizationState(
                name = name,
                email = email,
                normalizedEmail = email.toLowerCase().trim(),
                contactNumber = contactNumber,
                registeredAt = Instant.now(),
                addressLine1 = addressLine1,
                addressLine2 = addressLine2,
                town = town,
                city = city,
                province = province,
                state = state,
                country = country,
                postalCode = postalCode,
                image = image,
                organizationRoleId = organizationRoleId,
                encryptKey = encryptKey,
                nodeO = ourIdentity.name.organisation,
                nodeCN = ourIdentity.name.organisation,
                nodeOU = ourIdentity.name.organisation,
                nodeL = ourIdentity.name.organisation,
                nodeC = ourIdentity.name.organisation,
                linearId = UniqueIdentifier(),
                participants = listOf(ourIdentity)
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val registerCommand = Command(OrganizationContract.Commands.Register(), ourIdentity.owningKey)
        val builder = TransactionBuilder(notary)
        builder.addOutputState(outState(), ORG_ID)
        builder.addCommand(registerCommand)
        return builder
    }

}