package coadjute.flows.organization

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.organization.OrganizationContract
import coadjute.contracts.organization.OrganizationContract.Companion.ORG_ID
import coadjute.functions.FlowFunctions
import coadjute.states.organization.OrganizationState
import net.corda.core.contracts.Command
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@StartableByRPC
class UpdateOrganizationFlow (private val contactNumber: String?,
                              private val addressLine1: String?,
                              private val addressLine2: String?,
                              private val town: String?,
                              private val city: String?,
                              private val province: String?,
                              private val state: String?,
                              private val country: String?,
                              private val postalCode: String?,
                              private val image: String?,
                              private val organizationRoleId: String,
                              private val encryptKey: String,
                              private val organizationId: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val signedTransaction = verifyAndSign(transaction())
        return subFlow(FinalityFlow(signedTransaction, listOf()))
    }

    private fun outState(): OrganizationState
    {
        val organization = getOrganizationRefByLinearId(organizationId).state.data
        return OrganizationState(
                name = organization.name,
                email = organization.email,
                normalizedEmail = organization.normalizedEmail,
                contactNumber = contactNumber,
                registeredAt = organization.registeredAt,
                addressLine1 = addressLine1,
                addressLine2 = addressLine2,
                town = town,
                city = city,
                province = province,
                state = state,
                country = country,
                postalCode = postalCode,
                image = image,
                organizationRoleId = organizationRoleId,
                encryptKey = encryptKey,
                nodeO = organization.nodeO,
                nodeCN = organization.nodeCN,
                nodeOU = organization.nodeOU,
                nodeL = organization.nodeL,
                nodeC = organization.nodeC,
                linearId = organization.linearId,
                participants = listOf(ourIdentity)
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val organizationRef = getOrganizationRefByLinearId(organizationId)
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val registerCommand = Command(OrganizationContract.Commands.Update(), ourIdentity.owningKey)
        val builder = TransactionBuilder(notary)
        builder.addInputState(organizationRef)
        builder.addOutputState(outState(), ORG_ID)
        builder.addCommand(registerCommand)
        return builder
    }
}