package coadjute.flows.user

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.user.UserNotificationContract
import coadjute.functions.FlowFunctions
import coadjute.states.user.UserNotificationState
import net.corda.core.contracts.Command
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import java.time.Instant

@StartableByRPC
class UserNotificationFlow (private val userId: String,
                            private val notificationToken: String?,
                            private val issuedAt: Instant?): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val stx = verifyAndSign(transaction())
        return subFlow(FinalityFlow(stx, listOf()))
    }

    private fun outState(): UserNotificationState
    {
        return UserNotificationState(
                userId = userId,
                notificationToken = notificationToken,
                issuedAt = Instant.now(),
                linearId = UniqueIdentifier(),
                participants = listOf(ourIdentity)
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val cmd = Command(UserNotificationContract.Commands.Create(), ourIdentity.owningKey)
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addOutputState(outState(), UserNotificationContract.USER_ID)
        builder.addCommand(cmd)
        return builder
    }
}