package coadjute.flows.user

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.user.UserContract
import coadjute.contracts.user.UserContract.Companion.USER_ID
import coadjute.functions.FlowFunctions
import coadjute.states.user.UserState
import net.corda.core.contracts.Command
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@StartableByRPC
class UpdateUserProfileFlow (private val firstName: String,
                             private val middleName: String?,
                             private val lastName: String,
                             private val email: String,
                             private val contactNumber: String?,
                             private val addressLine1: String?,
                             private val addressLine2: String?,
                             private val town: String?,
                             private val city: String?,
                             private val province: String?,
                             private val state: String?,
                             private val country: String?,
                             private val postalCode: String?,
                             private val roleId: String,
                             private val image: String?,
                             private val password: String,
                             private val userId: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val signedTransaction = verifyAndSign(transaction())
        return subFlow(FinalityFlow(signedTransaction, listOf()))
    }

    private fun outState(): UserState
    {
        val user = getUserRefByLinearId(userId).state.data
        return UserState(
                firstName = firstName,
                middleName = middleName,
                lastName = lastName,
                email = email,
                normalizedEmail = email.toLowerCase().trim(),
                contactNumber = contactNumber,
                addressLine1 = addressLine1,
                addressLine2 = addressLine2,
                town = town,
                city = city,
                province = province,
                state = state,
                country = country,
                postalCode = postalCode,
                organizationId = user.organizationId,
                roleId = roleId,
                image = image,
                password = password,
                salt = user.salt,
                createdAt = user.createdAt,
                linearId = user.linearId,
                participants = user.participants
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val userStateRef = getUserRefByLinearId(userId)
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val registerCommand = Command(UserContract.Commands.Update(), ourIdentity.owningKey)
        val builder = TransactionBuilder(notary)
        builder.addInputState(userStateRef)
        builder.addOutputState(outState(), USER_ID)
        builder.addCommand(registerCommand)
        return builder
    }
}