package coadjute.flows.user

import co.paralleluniverse.fibers.Suspendable
import coadjute.contracts.user.UserRoleContract
import coadjute.contracts.user.UserRoleContract.Companion.USERORG_ID
import coadjute.functions.FlowFunctions
import coadjute.states.user.UserRoleState
import net.corda.core.contracts.Command
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@StartableByRPC
class CreateUserRoleFlow (private val name: String): FlowFunctions()
{
    @Suspendable
    override fun call(): SignedTransaction
    {
        val stx = verifyAndSign(transaction())
        return subFlow(FinalityFlow(stx, listOf()))
    }

    private fun outState(): UserRoleState
    {
        return UserRoleState(
                name = name,
                linearId = UniqueIdentifier(),
                participants = listOf(ourIdentity)
        )
    }

    private fun transaction(): TransactionBuilder
    {
        val cmd = Command(UserRoleContract.Commands.Role(), ourIdentity.owningKey)
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val builder = TransactionBuilder(notary = notary)
        builder.addOutputState(outState(), USERORG_ID)
        builder.addCommand(cmd)
        return builder
    }
}

