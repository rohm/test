package coadjute.functions

import net.corda.core.identity.Party
import co.paralleluniverse.fibers.Suspendable
import net.corda.core.flows.*
import net.corda.core.node.StatesToRecord
import net.corda.core.transactions.SignedTransaction

@InitiatingFlow
class ShareFlow(private val sharedParty: Party, private val stx: SignedTransaction) : FlowLogic<Unit>()
{
    @Suspendable
    override fun call()
    {
        val session = initiateFlow(sharedParty)
        subFlow(SendTransactionFlow(session, stx))
    }
}

@InitiatedBy(ShareFlow::class)
class ShareFlowResponder(private val session: FlowSession) : FlowLogic<Unit>()
{
    @Suspendable
    override fun call()
    {
        subFlow(ReceiveTransactionFlow(session, statesToRecord = StatesToRecord.ALL_VISIBLE))
    }
}
