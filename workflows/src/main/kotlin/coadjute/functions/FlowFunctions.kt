package coadjute.functions

import co.paralleluniverse.fibers.Suspendable
import coadjute.states.collaborator.CollaboratorRoleState
import coadjute.states.collaborator.CollaboratorState
import coadjute.states.job.FollowerState
import coadjute.states.job.JobLogState
import coadjute.states.job.JobState
import coadjute.states.organization.OrganizationState
import coadjute.states.project.*
import coadjute.states.user.UserState
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.crypto.SecureHash
import net.corda.core.flows.CollectSignaturesFlow
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.FlowSession
import net.corda.core.identity.Party
import net.corda.core.node.services.VaultQueryException
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import java.math.BigInteger
import java.security.MessageDigest
import java.time.Instant

abstract class FlowFunctions : FlowLogic<SignedTransaction>()
{
    override val progressTracker = ProgressTracker(INITIALIZING, BUILDING, SIGNING, COLLECTING, FINALIZING)

    fun verifyAndSign(transaction: TransactionBuilder): SignedTransaction
    {
        progressTracker.currentStep = SIGNING
        transaction.verify(serviceHub)
        return serviceHub.signInitialTransaction(transaction)
    }

    fun formatDouble(x: Double?): Double?
    {
        return if (x == null)
        {
            null
        }
        else
        {
            return "%.2f".format(x).toDouble()
        }

    }

    fun formatSecureHash(hash: String?): SecureHash?
    {
        return if (hash == null)
        {
            null
        }
       else
        {
            return SecureHash.sha256(hash)
        }
    }

    @Suspendable
    fun collectSignature(
            transaction: SignedTransaction,
            sessions: List<FlowSession>
    ): SignedTransaction = subFlow(CollectSignaturesFlow(transaction, sessions))

    @Suspendable
    fun recordTransactionWithOtherParty(transaction: SignedTransaction, sessions: List<FlowSession>): SignedTransaction
    {
        progressTracker.currentStep = FINALIZING
        return subFlow(FinalityFlow(transaction, sessions))
    }

    @Suspendable
    fun recordTransactionWithoutOtherParty(transaction: SignedTransaction) : SignedTransaction
    {
        progressTracker.currentStep = FINALIZING
        return subFlow(FinalityFlow(transaction, emptyList()))
    }

    fun Client(): Party
    {
        return serviceHub.identityService.partiesFromName("Client", false).singleOrNull()
                ?: throw IllegalArgumentException("No match found for Regulator")
    }

    fun PrincipalDesigner(): Party
    {
        return serviceHub.identityService.partiesFromName("Principal Designer", false).singleOrNull()
                ?: throw IllegalArgumentException("No match found for Regulator")
    }

    fun PrincipalConstructor(): Party
    {
        return serviceHub.identityService.partiesFromName("Principal Constructor", false).singleOrNull()
                ?: throw IllegalArgumentException("No match found for Regulator")
    }

    fun RegulatorNode(): Party
    {
        return serviceHub.identityService.partiesFromName("Regulator", false).singleOrNull()
                ?: throw IllegalArgumentException("No match found for Regulator")
    }

    fun stringToParty(name: String): Party
    {
        return serviceHub.identityService.partiesFromName(name, false).singleOrNull()
                ?: throw IllegalArgumentException("No match found for $name")
    }

    private fun stringToUniqueIdentifier(id: String): UniqueIdentifier
    {
        return UniqueIdentifier.fromString(id)
    }

    fun parseInstantToString(date: String?): Instant?
    {
        return if (date == null)
        {
            null
        }
        else
        {
            Instant.parse(date)
        }
    }

    fun getOrganizationRefByLinearId(id: String): StateAndRef<OrganizationState>
    {
        val linearId = stringToUniqueIdentifier(id)
        val criteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(linearId))
        return serviceHub.vaultService.queryBy<OrganizationState>(criteria = criteria).states.single()
    }

    fun getUserRefByLinearId(id: String): StateAndRef<UserState>
    {
        val linearId = stringToUniqueIdentifier(id)
        val criteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(linearId))
        return serviceHub.vaultService.queryBy<UserState>(criteria = criteria).states.single()
    }

    fun getProjectRefByLinearId(id: String): StateAndRef<ProjectState>
    {
        val linearId = stringToUniqueIdentifier(id)
        val criteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(linearId))
        return serviceHub.vaultService.queryBy<ProjectState>(criteria = criteria).states.single()
    }

    fun getProjectRefByContractId(contractId: String): StateAndRef<ProjectState>?
    {
        val criteria = QueryCriteria.VaultQueryCriteria()
        return serviceHub.vaultService.queryBy<ProjectState>(criteria = criteria).states.find {
            it.state.data.contractId == contractId
        }
    }

    fun getProjectContractRefByLinearId(id: String): StateAndRef<ProjectContractState>
    {
        val linearId = stringToUniqueIdentifier(id)
        val criteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(linearId))
        return serviceHub.vaultService.queryBy<ProjectContractState>(criteria = criteria).states.single()
    }

    fun getProgramRefByLinearId(id: String): StateAndRef<ProgramState>
    {
        val linearId = stringToUniqueIdentifier(id)
        val criteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(linearId))
        return serviceHub.vaultService.queryBy<ProgramState>(criteria = criteria).states.single()
    }

    fun getProjectCollaboratorRefByLinearId(id: String): StateAndRef<CollaboratorState>
    {
        val linearId = stringToUniqueIdentifier(id)
        val criteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(linearId))
        return serviceHub.vaultService.queryBy<CollaboratorState>(criteria = criteria).states.single()
    }

    fun getProjectStageRefByLinearId(id: String): StateAndRef<ProjectStageState>
    {
        val linearId = stringToUniqueIdentifier(id)
        val criteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(linearId))
        return serviceHub.vaultService.queryBy<ProjectStageState>(criteria = criteria).states.single()
    }

    fun getProjectStagesRefByProjectId(projectId: String): List<StateAndRef<ProjectStageState>>?
    {
        val criteria = QueryCriteria.VaultQueryCriteria()
        return serviceHub.vaultService.queryBy<ProjectStageState>(criteria = criteria).states.filter {
            it.state.data.projectId == projectId
        }
    }

    fun getProcessRefByLinearId(id: String): StateAndRef<ProcessState>
    {
        val linearId = stringToUniqueIdentifier(id)
        val criteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(linearId))
        return serviceHub.vaultService.queryBy<ProcessState>(criteria = criteria).states.single()
    }

    fun getProcessStageRefByLinearId(id: String): StateAndRef<ProcessStageState>
    {
        val linearId = stringToUniqueIdentifier(id)
        val criteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(linearId))
        return serviceHub.vaultService.queryBy<ProcessStageState>(criteria = criteria).states.single()
    }

    fun getProcessStagesRefByProjectId(projectId: String): List<StateAndRef<ProcessStageState>>?
    {
        val criteria = QueryCriteria.VaultQueryCriteria()
        return serviceHub.vaultService.queryBy<ProcessStageState>(criteria = criteria).states.filter {
            it.state.data.projectId == projectId
        }
    }

    fun getContractObligationRefByLinearId(id: String): StateAndRef<ContractObligationState>
    {
        val linearId = stringToUniqueIdentifier(id)
        val criteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(linearId))
        return serviceHub.vaultService.queryBy<ContractObligationState>(criteria = criteria).states.single()
    }


    fun getContractObligationRefByContractId(contractId: String?): StateAndRef<ContractObligationState>?
    {
        val criteria = QueryCriteria.VaultQueryCriteria()
        return serviceHub.vaultService.queryBy<ContractObligationState>(criteria = criteria).states.find {
            it.state.data.contractId == contractId
        } ?: return null
    }

    fun getJobRefByLinearId(id: String): StateAndRef<JobState>
    {
        val linearId = stringToUniqueIdentifier(id)
        val criteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(linearId))
        return serviceHub.vaultService.queryBy<JobState>(criteria = criteria).states.single()
    }

    fun getJobsRefByProjectId(projectId: String): List<StateAndRef<JobState>>?
    {
        val criteria = QueryCriteria.VaultQueryCriteria()
        return serviceHub.vaultService.queryBy<JobState>(criteria = criteria).states.filter {
            it.state.data.projectId == projectId
        }
    }

    fun getProjectLogRefByProjectId(projectId: String): StateAndRef<ProjectLogState>?
    {
        val projectStateRef = serviceHub.vaultService.queryBy<ProjectLogState>().states
        return projectStateRef.find { it.state.data.projectId == projectId }
    }

    fun getCollaboratorRoleRefByLinearId(id: String): StateAndRef<CollaboratorRoleState>
    {
        val linearId = stringToUniqueIdentifier(id)
        val criteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(linearId))
        return serviceHub.vaultService.queryBy<CollaboratorRoleState>(criteria = criteria).states.single()
    }

    fun getAllCollaboratorsByProjectId(projectId: String): List<StateAndRef<CollaboratorState>>?
    {
        val criteria = QueryCriteria.VaultQueryCriteria()
        return serviceHub.vaultService.queryBy<CollaboratorState>(criteria = criteria).states.filter {
            it.state.data.projectId == projectId && it.state.data.status.toUpperCase() == "ACCEPTED"
        }
    }

    fun getContractArtifactRefByLinearId(id: String): StateAndRef<ContractArtifactState>
    {
        val linearId = stringToUniqueIdentifier(id)
        val criteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(linearId))
        return serviceHub.vaultService.queryBy<ContractArtifactState>(criteria = criteria).states.single()
    }

    fun getJobLogRefByJobId(jobId: String): StateAndRef<JobLogState>?
    {
        val jobStateRef = serviceHub.vaultService.queryBy<JobLogState>().states
        return jobStateRef.find { it.state.data.jobId == jobId }
    }

    fun getJobLogsByJobId(jobId: String): StateAndRef<JobLogState>?
    {
        val criteria = QueryCriteria.VaultQueryCriteria()
        return serviceHub.vaultService.queryBy<JobLogState>(criteria = criteria).states.find {
            it.state.data.jobId == jobId
        }
    }

//    fun getFollowerByJobId(jobId: String): StateAndRef<FollowerState>?
//    {
//        val criteria = QueryCriteria.VaultQueryCriteria()
//        return serviceHub.vaultService.queryBy<FollowerState>(criteria = criteria).states.find {
//            it.state.data.jobId == jobId
//        }
//    }
}