package coadjute.states.collaborator

import coadjute.contracts.collaborator.CollaboratorRoleContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party

@BelongsToContract(CollaboratorRoleContract::class)
data class CollaboratorRoleState(val name: String,
                                 override val linearId: UniqueIdentifier,
                                 override val participants: List<Party>): LinearState