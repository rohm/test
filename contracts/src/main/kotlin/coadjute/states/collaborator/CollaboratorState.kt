package coadjute.states.collaborator

import coadjute.contracts.collaborator.CollaboratorContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party

@BelongsToContract(CollaboratorContract::class)
data class CollaboratorState(val name: String,
                             val projectId: String,
                             val collaboratorRoleId: String,
                             val nodeO: String,
                             val nodeCN: String?,
                             val nodeOU: String?,
                             val nodeL: String?,
                             val nodeC: String?,
                             val status: String,
                             val remarks: String?,
                             override val linearId: UniqueIdentifier,
                             override val participants: List<Party>): LinearState