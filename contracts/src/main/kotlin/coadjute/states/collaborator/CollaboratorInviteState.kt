package coadjute.states.collaborator

import coadjute.contracts.collaborator.CollaboratorInviteContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party

@BelongsToContract(CollaboratorInviteContract::class)
data class CollaboratorInviteState(val projectId: String,
                                   val name: String,
                                   val email: String,
                                   val normalizedEmail: String,
                                   val contactName: String,
                                   val contactNumber: String,
                                   val nodeO: String?,
                                   override val linearId: UniqueIdentifier,
                                   override val participants: List<Party>): LinearState