package coadjute.states.project

import coadjute.contracts.project.ProjectStageContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party

@BelongsToContract(ProjectStageContract::class)
data class ProjectStageState(val projectId: String,
                             val processStageId: String?,
                             val name: String,
                             val gateBit: Boolean,
                             val gateStageId: String?,
                             val description: String?,
                             val order: Int?,
                             override val linearId: UniqueIdentifier,
                             override val participants: List<Party>): LinearState