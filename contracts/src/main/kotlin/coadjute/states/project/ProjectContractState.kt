package coadjute.states.project

import coadjute.contracts.project.ProjectContractContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party

@BelongsToContract(ProjectContractContract::class)
data class ProjectContractState(val name: String,
                                override val linearId: UniqueIdentifier,
                                override val participants: List<Party>): LinearState