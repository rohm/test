package coadjute.states.project

import coadjute.contracts.project.ProjectLogContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party
import java.time.Instant

@BelongsToContract(ProjectLogContract::class)
data class ProjectLogState(val projectId: String,
                           val content: String,
                           val updatedBy: String?,
                           val updatedAt: Instant,
                           override val linearId: UniqueIdentifier,
                           override val participants: List<Party>): LinearState