package coadjute.states.project

import coadjute.contracts.project.ProcessContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party

@BelongsToContract(ProcessContract::class)
data class ProcessState(val name: String,
                        override val linearId: UniqueIdentifier,
                        override val participants: List<Party>): LinearState