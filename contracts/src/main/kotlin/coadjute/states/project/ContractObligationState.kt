package coadjute.states.project

import coadjute.contracts.project.ContractObligationContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party

@BelongsToContract(ContractObligationContract::class)
data class ContractObligationState(val contractId: String,
                                   val name: String,
                                   val description: String?,
                                   override val linearId: UniqueIdentifier,
                                   override val participants: List<Party>): LinearState