package coadjute.states.project

import coadjute.contracts.project.NotificationContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party
import java.time.Instant

@BelongsToContract(NotificationContract::class)
data class NotificationState(val name: String,
                             val content: String,
                             val recipientCollaboratorId: String?,
                             val recipientUserId: String,
                             val sentAt: Instant?,
                             val senderCollaboratorId: String?,
                             val senderUserId: String?,
                             val projectId: String?,
                             val jobId: String?,
                             override val linearId: UniqueIdentifier,
                             override val participants: List<Party>): LinearState