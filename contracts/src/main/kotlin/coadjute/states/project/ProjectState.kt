package coadjute.states.project

import coadjute.contracts.project.ProjectContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party
import java.time.Instant

@BelongsToContract(ProjectContract::class)
data class ProjectState(val name: String,
                        val organizationId: String,
                        val startAt: Instant?,
                        val endAt: Instant?,
                        val budgetCurrency: String?,
                        val budgetAmount: Double?,
                        val image: String?,
                        val industryType: String,
                        val contractId: String?,
                        val processId: String?,
                        val programId: String?,
                        val status: String,
                        override val linearId: UniqueIdentifier,
                        override val participants: List<Party>): LinearState