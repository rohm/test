package coadjute.states.project

import coadjute.contracts.project.ContractArtifactContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.crypto.SecureHash
import net.corda.core.identity.Party

@BelongsToContract(ContractArtifactContract::class)
data class ContractArtifactState(val name: String,
                                 val contractId: String,
                                 val description: String?,
                                 val url: String?,
                                 val fileSize: Double?,
                                 val hash: SecureHash?,
                                 override val linearId: UniqueIdentifier,
                                 override val participants: List<Party>): LinearState