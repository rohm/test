package coadjute.states.project

import coadjute.contracts.project.ProgramContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party

@BelongsToContract(ProgramContract::class)
data class ProgramState(val name: String,
                        val description: String?,
                        override val linearId: UniqueIdentifier,
                        override val participants: List<Party>): LinearState