package coadjute.states.organization

import coadjute.contracts.organization.OrganizationContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party
import java.time.Instant

@BelongsToContract(OrganizationContract::class)
data class OrganizationState (val name: String,
                              val email: String,
                              val normalizedEmail: String,
                              val contactNumber: String?,
                              val registeredAt: Instant,
                              val addressLine1: String?,
                              val addressLine2: String?,
                              val town: String?,
                              val city: String?,
                              val province: String?,
                              val state: String?,
                              val country: String?,
                              val postalCode: String?,
                              val image: String?,
                              val organizationRoleId: String,
                              val encryptKey: String,
                              val nodeO: String,
                              val nodeCN: String?,
                              val nodeOU: String?,
                              val nodeL: String?,
                              val nodeC: String?,
                              override val linearId: UniqueIdentifier,
                              override val participants: List<Party>): LinearState
