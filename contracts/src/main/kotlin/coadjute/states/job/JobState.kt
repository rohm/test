package coadjute.states.job

import coadjute.contracts.job.JobContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.crypto.SecureHash
import net.corda.core.identity.Party
import java.time.Instant

@BelongsToContract(JobContract::class)
data class JobState(val name: String,
                    val projectId: String,
                    val status: String,
                    val action: String,
                    val instruction: String?,
                    val artifactBit: Boolean,
                    val url: String?,
                    val fileSize: Double?,
                    val hash: SecureHash?,
                    val remarks: String?,
                    val referenceJobId: String?,
                    val projectStageId: String?,
                    val startAt: Instant?,
                    val endAt: Instant?,
                    val responsibleCollaboratorId: String?,
                    val responsibleUserId: String?,
                    val responsibleAcceptedAt: Instant?,
                    val ownerCollaboratorId: String?,
                    val ownerUserId: String?,
                    val ownerAcceptedAt: Instant?,
                    val creatorCollaboratorId: String,
                    val creatorUserId: String,
                    override val linearId: UniqueIdentifier,
                    override val participants: List<Party>): LinearState