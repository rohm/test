package coadjute.states.job

import coadjute.contracts.job.JobLogContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party
import java.time.Instant

@BelongsToContract(JobLogContract::class)
data class JobLogState(val jobId: String,
                       val content: String,
                       val action: String,
                       val splitJobIds: List<String>?,
                       val mergeFromJobIds: List<String>?,
                       val updatedByCollaboratorId: String?,
                       val updatedByUserId: String?,
                       val updatedByUserFirstName: String?,
                       val updatedByUserMiddleName: String?,
                       val updatedByUserLastName: String?,
                       val updatedAt: Instant,
                       override val linearId: UniqueIdentifier,
                       override val participants: List<Party>): LinearState