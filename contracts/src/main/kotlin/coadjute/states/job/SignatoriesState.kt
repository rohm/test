package coadjute.states.job

import coadjute.contracts.job.SignatoriesContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party
import java.time.Instant

@BelongsToContract(SignatoriesContract::class)
data class SignatoriesState(val jobId: String,
                            val collaboratorId: String,
                            val acceptedAt: Instant?,
                            val userId: String?,
                            override val linearId: UniqueIdentifier,
                            override val participants: List<Party>): LinearState