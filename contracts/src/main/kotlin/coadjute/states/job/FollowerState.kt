package coadjute.states.job

import coadjute.contracts.job.FollowerContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party

@BelongsToContract(FollowerContract::class)
data class FollowerState(val jobId: String,
                         val collaboratorId: String,
                         val userId: String?,
                         override val linearId: UniqueIdentifier,
                         override val participants: List<Party>): LinearState