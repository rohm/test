package coadjute.states.user

import coadjute.contracts.user.UserNotificationContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party
import java.time.Instant

@BelongsToContract(UserNotificationContract::class)
class UserNotificationState(val userId: String,
                            val notificationToken: String?,
                            val issuedAt: Instant?,
                            override val linearId: UniqueIdentifier,
                            override val participants: List<Party>): LinearState