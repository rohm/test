package coadjute.states.user

import coadjute.contracts.user.UserRoleContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party

@BelongsToContract(UserRoleContract::class)
data class UserRoleState(val name: String, // ADMINISTRATOR, USER
                         override val linearId: UniqueIdentifier,
                         override val participants: List<Party>): LinearState