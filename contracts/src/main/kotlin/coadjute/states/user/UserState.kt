package coadjute.states.user

import coadjute.contracts.user.UserContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party
import java.time.Instant

@BelongsToContract(UserContract::class)
data class UserState (val firstName: String,
                      val middleName: String?,
                      val lastName: String,
                      val email: String,
                      val normalizedEmail: String,
                      val contactNumber: String?,
                      val addressLine1: String?,
                      val addressLine2: String?,
                      val town: String?,
                      val city: String?,
                      val province: String?,
                      val state: String?,
                      val country: String?,
                      val postalCode: String?,
                      val organizationId: String,
                      val roleId: String,
                      val image: String?,
                      val password: String,
                      val salt: String,
                      val createdAt: Instant,
                      override val linearId: UniqueIdentifier,
                      override val participants: List<Party>): LinearState