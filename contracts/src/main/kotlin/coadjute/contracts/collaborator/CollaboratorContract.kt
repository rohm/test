package coadjute.contracts.collaborator

import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.TypeOnlyCommandData
import net.corda.core.transactions.LedgerTransaction

class CollaboratorContract : Contract
{
    companion object
    {
        const val PCCONTRACT_ID = "coadjute.contracts.collaborator.CollaboratorContract"
    }

    override fun verify(tx: LedgerTransaction)
    {

    }

    interface Commands : CommandData
    {
        class Create : TypeOnlyCommandData(), Commands
        class Update : TypeOnlyCommandData(), Commands
    }
}