package coadjute.contracts.collaborator

import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.TypeOnlyCommandData
import net.corda.core.transactions.LedgerTransaction

class CollaboratorInviteContract : Contract
{
    companion object
    {
        const val PCINVITE_ID = "coadjute.contracts.collaborator.CollaboratorInviteContract"
    }

    override fun verify(tx: LedgerTransaction)
    {

    }

    interface Commands : CommandData
    {
        class Invite : TypeOnlyCommandData(), Commands
    }
}