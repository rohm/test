package coadjute.contracts.collaborator

import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.TypeOnlyCommandData
import net.corda.core.transactions.LedgerTransaction

class CollaboratorRoleContract : Contract
{
    companion object
    {
        const val CBROLE_ID = "coadjute.contracts.collaborator.CollaboratorRoleContract"
    }

    override fun verify(tx: LedgerTransaction)
    {

    }

    interface Commands : CommandData
    {
        class Role : TypeOnlyCommandData(), Commands
        class Collaborate : TypeOnlyCommandData(), Commands
    }
}