package coadjute.contracts.project

import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.TypeOnlyCommandData
import net.corda.core.transactions.LedgerTransaction

class ProcessStageContract : Contract
{
    companion object
    {
        const val PROCSCONTRACT_ID = "coadjute.contracts.project.ProcessStageContract"
    }

    interface Commands : CommandData
    {
        class Create : TypeOnlyCommandData(), Commands
        class Update : TypeOnlyCommandData(), Commands
        class Collaborate : TypeOnlyCommandData(), Commands
        class Share : TypeOnlyCommandData(), Commands
    }

    override fun verify(tx: LedgerTransaction)
    {

    }
}