package coadjute.contracts.project

import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.TypeOnlyCommandData
import net.corda.core.transactions.LedgerTransaction

class ProjectStageContract : Contract
{
    companion object
    {
        const val PROJSCONTRACT_ID = "coadjute.contracts.project.ProjectStageContract"
    }

    override fun verify(tx: LedgerTransaction)
    {

    }

    interface Commands : CommandData
    {
        class Create : TypeOnlyCommandData(), Commands
        class Update : TypeOnlyCommandData(), Commands
        class Collaborate : TypeOnlyCommandData(), Commands
        class Share : TypeOnlyCommandData(), Commands
    }
}