package coadjute.contracts.project

import coadjute.states.project.ProgramState
import net.corda.core.contracts.*
import net.corda.core.transactions.LedgerTransaction

class ProgramContract : Contract
{
    companion object
    {
        const val PROGRAM_ID = "coadjute.contracts.project.ProgramContract"
    }

    interface Commands : CommandData
    {
        class Create : TypeOnlyCommandData(), Commands
        class Collaborate : TypeOnlyCommandData(), Commands
        class Share : TypeOnlyCommandData(), Commands
    }

    override fun verify(tx: LedgerTransaction)
    {
        val command = tx.commands.requireSingleCommand<Commands>()
        when (command.value)
        {
            is Commands.Create -> requireThat {
                "No inputs should be consumed when creating a program." using (tx.inputs.isEmpty())
                "Only one output state should be created when creating a program." using (tx.outputs.size == 1)
                val createProgram = tx.outputStates.single() as ProgramState
                "Program is required" using (createProgram.name.isNotBlank() )
//                "Description is required" using (createProgram.description.isNotBlank() )
                "Only the current user may sign the transaction." using
                        (command.signers.toSet() == createProgram.participants.map { it.owningKey }.toSet())
            }
        }
    }
}