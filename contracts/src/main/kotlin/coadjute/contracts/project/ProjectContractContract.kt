package coadjute.contracts.project

import coadjute.states.project.ProjectContractState
import net.corda.core.contracts.*
import net.corda.core.transactions.LedgerTransaction

class ProjectContractContract : Contract
{
    companion object
    {
        const val PROJECTCONTRACT_ID = "coadjute.contracts.project.ProjectContractContract"
    }

    interface Commands : CommandData
    {
        class Create : TypeOnlyCommandData(), Commands
        class Collaborate : TypeOnlyCommandData(), Commands
        class Share : TypeOnlyCommandData(), Commands
    }

    override fun verify(tx: LedgerTransaction)
    {
        val command = tx.commands.requireSingleCommand<Commands>()
        when (command.value)
        {
            is Commands.Create -> requireThat {
                "No inputs should be consumed when creating a Project Contract." using (tx.inputs.isEmpty())
                "Only one output state should be created when creating a Project Contract." using (tx.outputs.size == 1)
                val createProjectContract = tx.outputStates.single() as ProjectContractState
                "Project Contract title is required" using (createProjectContract.name.isNotBlank() )
                "Only the current user may sign the transaction." using
                        (command.signers.toSet() == createProjectContract.participants.map { it.owningKey }.toSet())
            }
        }
    }
}