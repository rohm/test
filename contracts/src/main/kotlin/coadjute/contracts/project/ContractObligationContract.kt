package coadjute.contracts.project

import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.TypeOnlyCommandData
import net.corda.core.transactions.LedgerTransaction

class ContractObligationContract : Contract
{
    companion object
    {
        const val COBLIGATION_ID = "coadjute.contracts.project.ContractObligationContract"
    }

    interface Commands : CommandData
    {
        class Create : TypeOnlyCommandData(), Commands
        class Update : TypeOnlyCommandData(), Commands
        class Collaborate : TypeOnlyCommandData(), Commands
    }

    override fun verify(tx: LedgerTransaction)
    {

    }
}