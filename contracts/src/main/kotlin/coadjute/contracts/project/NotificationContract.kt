package coadjute.contracts.project

import coadjute.states.project.NotificationState
import net.corda.core.contracts.*
import net.corda.core.transactions.LedgerTransaction

class NotificationContract : Contract {
        companion object
    {
        const val USER_ID = "coadjute.contracts.project.NotificationContract"
    }

        interface Commands : CommandData
        {
            class Create : TypeOnlyCommandData(), Commands
        }

        override fun verify(tx: LedgerTransaction)
        {
            val command = tx.commands.requireSingleCommand<Commands>()
            when (command.value)
            {
                is Commands.Create -> requireThat {
                    "No inputs should be consumed when issuing an IOU." using (tx.inputs.isEmpty())
                    "Only one output state should be created when issuing an IOU." using (tx.outputs.size == 1)
                    val sendMsg = tx.outputStates.single() as NotificationState
                    "Only the current user may sign the transaction." using
                            (command.signers.toSet() == sendMsg.participants.map { it.owningKey }.toSet())
                }
            }
        }


    }
