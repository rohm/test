package coadjute.contracts.project

import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.TypeOnlyCommandData
import net.corda.core.transactions.LedgerTransaction

class ProjectLogContract : Contract
{
    companion object
    {
        const val PLCONTRACT_ID = "coadjute.contracts.project.ProjectLogContract"
    }

    override fun verify(tx: LedgerTransaction)
    {

    }

    interface Commands : CommandData
    {
        class CreateOrUpdate : TypeOnlyCommandData(), Commands
        class Collaborate : TypeOnlyCommandData(), Commands
    }
}