package coadjute.contracts.project

import coadjute.states.project.ProjectState
import net.corda.core.contracts.*
import net.corda.core.transactions.LedgerTransaction

class ProjectContract : Contract
{
    companion object
    {
        const val PROJECT_ID = "coadjute.contracts.project.ProjectContract"
    }

    interface Commands : CommandData
    {
        class Create : TypeOnlyCommandData(), Commands
        class Update : TypeOnlyCommandData(), Commands
        class Collaborate : TypeOnlyCommandData(), Commands
    }

    override fun verify(tx: LedgerTransaction)
    {
//        val command = tx.commands.requireSingleCommand<Commands>()
//        when (command.value)
//        {
//            is Commands.Create -> requireThat {
//                "No inputs should be consumed when creating a project." using (tx.inputs.isEmpty())
//                "Only one output state should be created when creating a project." using (tx.outputs.size == 1)
//                val createProject = tx.outputStates.single() as ProjectState
//                "Title of Project is required" using (createProject.name.isNotBlank() )
////                "Organization ID is required" using (createProject.organizationId.isNotBlank() )
////                "Start Date is required" using (createProject.startDate.isNotBlank() )
////                "End Date is required" using (createProject.endDate.isNotBlank() )
////                "Budget Currency is required" using (createProject.budgetCurrency.isNotBlank() )
////                val minBudget = createProject.budgetAmount.coerceAtLeast(0.0)
////                "Budget Amount is required" using (minBudget > 0.0 )
////                "Budget Amount should be a number NAN" using (!createProject.budgetAmount.isNaN())
//////                val NUM_REGEX = "^[-,0-9]+\$".toRegex()
//////                "Budget Amount should be a number" using (createProject.budgetAmount.equals(NUM_REGEX))
////                "Image is required" using (createProject.image.isNotBlank() )
//                "Industry Type is required" using (createProject.industryType.isNotBlank() )
//                "Status is required" using (createProject.status.isNotBlank() )
//                val startDate = createProject.startAt?.epochSecond
//                val endDate = createProject.endAt?.epochSecond
//
//                if (endDate != null) {
//                    val startDateInt = startDate!!.toInt()
//                    "There should be a valid Starting Date" using (startDateInt > 0)
//                }
//
//                if (startDate != null) {
//                    "End Date should be greater than Starting Date" using (endDate!! > startDate)
//                }
//
////                // \.(?i:)(?:jpg|gif)$
////                val IMG_REGEX = "(.*\\.jpe?g|.*\\.JPE?G|.*\\.png|.*\\.PNG|)".toRegex()
////                "Image should be on a jpeg/jpg/png format" using (createProject.image.contains(regex = IMG_REGEX))
//
//                "Only the current user may sign the transaction." using
//                        (command.signers.toSet() == createProject.participants.map { it.owningKey }.toSet())
//            }
//            is Commands.Update -> requireThat {
//                "Only one input should be consumed when updating a project." using (tx.inputs.size == 1)
//                "Only one output state should be created when updating a project." using (tx.outputs.size == 1)
//                val createProject = tx.outputStates.single() as ProjectState
//                "Title of Project is required" using (createProject.name.isNotBlank() )
////                "Organization ID is required" using (createProject.organizationId.isNotBlank() )
////                "Start Date is required" using (createProject.startDate.isNotBlank() )
////                "End Date is required" using (createProject.endDate.isNotBlank() )
////                "Budget Currency is required" using (createProject.budgetCurrency.isNotBlank() )
////                val minBudget = createProject.budgetAmount.coerceAtLeast(0.0)
////                "Budget Amount is required" using (minBudget > 0.0 )
////                "Budget Amount should be a number NAN" using (!createProject.budgetAmount.isNaN())
//////                val NUM_REGEX = "^[-,0-9]+\$".toRegex()
//////                "Budget Amount should be a number" using (createProject.budgetAmount.equals(NUM_REGEX))
////                "Image is required" using (createProject.image.isNotBlank() )
//                "Industry Type is required" using (createProject.industryType.isNotBlank() )
//                "Status is required" using (createProject.status.isNotBlank() )
//                val startDate = createProject.startAt?.epochSecond
//                val endDate = createProject.endAt?.epochSecond
//
//                if (endDate != null) {
//                    val startDateInt = startDate!!.toInt()
//                    "There should be a valid Starting Date" using (startDateInt > 0)
//                }
//
//                if (startDate != null) {
//                    "End Date should be greater than Starting Date" using (endDate!! > startDate)
//                }
//
////                // \.(?i:)(?:jpg|gif)$
////                val IMG_REGEX = "(.*\\.jpe?g|.*\\.JPE?G|.*\\.png|.*\\.PNG|)".toRegex()
////                "Image should be on a jpeg/jpg/png format" using (createProject.image.contains(regex = IMG_REGEX))
//
//                "Only the current user may sign the transaction." using
//                        (command.signers.toSet() == createProject.participants.map { it.owningKey }.toSet())
//            }
//
//        }
    }
}