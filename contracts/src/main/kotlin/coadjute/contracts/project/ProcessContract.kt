package coadjute.contracts.project

import coadjute.states.project.ProcessState
import net.corda.core.contracts.*
import net.corda.core.transactions.LedgerTransaction

class ProcessContract : Contract
{
    companion object
    {
        const val PROCESS_ID = "coadjute.contracts.project.ProcessContract"
    }

    interface Commands : CommandData
    {
        class Create : TypeOnlyCommandData(), Commands
        class Collaborate : TypeOnlyCommandData(), Commands
        class Share : TypeOnlyCommandData(), Commands
    }

    override fun verify(tx: LedgerTransaction)
    {
        val command = tx.commands.requireSingleCommand<Commands>()
        when (command.value)
        {
            is Commands.Create -> requireThat {
                "No inputs should be consumed when creating a process." using (tx.inputs.isEmpty())
                "Only one output state should be created when creating a process." using (tx.outputs.size == 1)
                val createProcess = tx.outputStates.single() as ProcessState
                "Process is required" using (createProcess.name.isNotBlank() )
                "Only the current user may sign the transaction." using
                        (command.signers.toSet() == createProcess.participants.map { it.owningKey }.toSet())
            }
        }
    }
}