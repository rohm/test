package coadjute.contracts.user

import net.corda.core.contracts.*
import net.corda.core.transactions.LedgerTransaction

class UserNotificationContract : Contract {
    companion object
    {
        const val USER_ID = "coadjute.contracts.user.UserNotificationContract"
    }

    interface Commands : CommandData
    {
        class Create : TypeOnlyCommandData(), Commands
    }

    override fun verify(tx: LedgerTransaction)
    {
        val command = tx.commands.requireSingleCommand<Commands>()

    }

}
