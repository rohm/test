package coadjute.contracts.user

import coadjute.states.user.UserState
import net.corda.core.contracts.*
import net.corda.core.transactions.LedgerTransaction

class UserContract : Contract
{
    companion object
    {
        const val USER_ID = "coadjute.contracts.user.UserContract"
    }

    interface Commands : CommandData
    {
        class Register : TypeOnlyCommandData(), Commands
        class Update : TypeOnlyCommandData(), Commands
    }

    override fun verify(tx: LedgerTransaction)
    {
        val command = tx.commands.requireSingleCommand<Commands>()
        when (command.value) {
            is Commands.Register -> requireThat {
                "No inputs should be consumed when registering a User." using (tx.inputs.isEmpty())
                "Only one output state should be created when registering a User." using (tx.outputs.size == 1)
                val registerUser = tx.outputStates.single() as UserState
//                "Username is required" using (registerUser.emailAddress.isNotBlank() )
//                "Password is required" using (registerUser.password.isNotBlank())
//                val EMAIL_REGEX = "^[A-Za-z](.*)([@]{1})(.{1,})(\\.)(.{1,})".toRegex()
//                "Username should be a valid E-mail Address" using (registerUser.emailAddress.contains(regex = EMAIL_REGEX))
                "First Name is required" using (registerUser.firstName.isNotBlank())
                "Last Name is required" using (registerUser.lastName.isNotBlank())
                "E-mail Address is required" using (registerUser.email.isNotBlank())
//                "Contact Number is required" using (registerUser.contactNumber.isNotBlank())
//                "Address Line 1 is required" using (registerUser.addressLine1.isNotBlank())
//                "Address Line 2 is required" using (registerUser.addressLine2.isNotBlank())
//                "Town is required" using (registerUser.town.isNotBlank())
//                "City is required" using (registerUser.city.isNotBlank())
//                "Province is required" using (registerUser.province.isNotBlank())
//                "State is required" using (registerUser.state.isNotBlank())
//                "Country Code is required" using (registerUser.country.isNotBlank())
//                "Postal Code is required" using (registerUser.postalCode.isNotBlank())
//                "Organization ID is required" using (registerUser.organizationId.isNotBlank())
//                "Role ID is required" using (registerUser.roleId.isNotBlank())
//                "Image is required" using (registerUser.image.isNotBlank())
                "Password is required" using (registerUser.password.isNotBlank())

                val NAME_REGEX = "^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*\$".toRegex()
                "First Name should be a valid name" using (registerUser.firstName.contains(regex = NAME_REGEX))
                "Last Name should be a valid name" using (registerUser.lastName.contains(regex = NAME_REGEX))

                val EMAIL_REGEX = "^[A-Za-z](.*)([@]{1})(.{1,})(\\.)(.{1,})".toRegex()
                "E-mail should be a valid E-mail Address" using (registerUser.email.contains(regex = EMAIL_REGEX))

//                val PNUM_REGEX = "^([\\+][0-9]{1,3}([ \\.\\-])?)?([\\(]{1}[0-9]{3}[\\)])?([0-9A-Z \\.\\-]{1,32})((x|ext|extension)?[0-9]{1,4}?)\$".toRegex()
////                val UKNUM_REGEX = "^((\\+44\\s?\\d{4}|\\(?\\d{5}\\)?)\\s?\\d{6})|((\\+44\\s?|0)7\\d{3}\\s?\\d{6})\$".toRegex()
//                "Contact Number should be a valid Phone Number" using (registerUser.contactNumber.contains(regex = PNUM_REGEX))

//                val NUM_REGEX = "^[-,0-9]+\$".toRegex()
//                val ALPHA3_REGEX = "^[A-Z]{3}$".toRegex()
//                "Country Code should be valid" using (registerUser.country.contains(regex = ALPHA3_REGEX))
//                "Postal Code should be valid" using (registerUser.postalCode.contains(regex = NUM_REGEX))

                "Only the current user may sign the transaction." using
                        (command.signers.toSet() == registerUser.participants.map { it.owningKey }.toSet())

            }

            is Commands.Update -> requireThat {
                "Only one input should be consumed when updating a User's Profile." using (tx.inputs.size == 1)
                "Only one output state should be created when updating a User's Profile." using (tx.outputs.size == 1)
                val registerUser = tx.outputStates.single() as UserState
//                "Username is required" using (registerUser.emailAddress.isNotBlank() )
//                "Password is required" using (registerUser.password.isNotBlank())
//                val EMAIL_REGEX = "^[A-Za-z](.*)([@]{1})(.{1,})(\\.)(.{1,})".toRegex()
//                "Username should be a valid E-mail Address" using (registerUser.emailAddress.contains(regex = EMAIL_REGEX))
                "First Name is required" using (registerUser.firstName.isNotBlank())
                "Last Name is required" using (registerUser.lastName.isNotBlank())
                "E-mail Address is required" using (registerUser.email.isNotBlank())
//                "Contact Number is required" using (registerUser.contactNumber.isNotBlank())
//                "Address Line 1 is required" using (registerUser.addressLine1.isNotBlank())
//                "Address Line 2 is required" using (registerUser.addressLine2.isNotBlank())
//                "Town is required" using (registerUser.town.isNotBlank())
//                "City is required" using (registerUser.city.isNotBlank())
//                "Province is required" using (registerUser.province.isNotBlank())
//                "State is required" using (registerUser.state.isNotBlank())
//                "Country Code is required" using (registerUser.country.isNotBlank())
//                "Postal Code is required" using (registerUser.postalCode.isNotBlank())
//                "Organization ID is required" using (registerUser.organizationId.isNotBlank())
//                "Role ID is required" using (registerUser.roleId.isNotBlank())
//                "Image is required" using (registerUser.image.isNotBlank())
                "Password is required" using (registerUser.password.isNotBlank())

                val NAME_REGEX = "^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*\$".toRegex()
                "First Name should be a valid name" using (registerUser.firstName.contains(regex = NAME_REGEX))
                "Last Name should be a valid name" using (registerUser.lastName.contains(regex = NAME_REGEX))

                val EMAIL_REGEX = "^[A-Za-z](.*)([@]{1})(.{1,})(\\.)(.{1,})".toRegex()
                "E-mail should be a valid E-mail Address" using (registerUser.email.contains(regex = EMAIL_REGEX))

//                val PNUM_REGEX = "^([\\+][0-9]{1,3}([ \\.\\-])?)?([\\(]{1}[0-9]{3}[\\)])?([0-9A-Z \\.\\-]{1,32})((x|ext|extension)?[0-9]{1,4}?)\$".toRegex()
////                val UKNUM_REGEX = "^((\\+44\\s?\\d{4}|\\(?\\d{5}\\)?)\\s?\\d{6})|((\\+44\\s?|0)7\\d{3}\\s?\\d{6})\$".toRegex()
//                "Contact Number should be a valid Phone Number" using (registerUser.contactNumber.contains(regex = PNUM_REGEX))

//                val NUM_REGEX = "^[-,0-9]+\$".toRegex()
//
//                val ALPHA3_REGEX = "^[A-Z]{3}$".toRegex()
//                "Country Code should be valid" using (registerUser.country.contains(regex = ALPHA3_REGEX))
//                "Postal Code should be valid" using (registerUser.postalCode.contains(regex = NUM_REGEX))

                "Only the current user may sign the transaction." using
                        (command.signers.toSet() == registerUser.participants.map { it.owningKey }.toSet())

            }

        }





}}