package coadjute.contracts.user

import coadjute.states.user.UserRoleState
import net.corda.core.contracts.*
import net.corda.core.transactions.LedgerTransaction

class UserRoleContract : Contract
{
    companion object
    {
        const val USERORG_ID = "coadjute.contracts.user.UserRoleContract"
    }

    interface Commands : CommandData
    {
        class Role : TypeOnlyCommandData(), Commands
    }

    override fun verify(tx: LedgerTransaction)
    {
        val command = tx.commands.requireSingleCommand<Commands>()
        when (command.value)
        {
            is Commands.Role -> requireThat {
                "No inputs should be consumed when issuing an IOU." using (tx.inputs.isEmpty())
                "Only one output state should be created when issuing an IOU." using (tx.outputs.size == 1)
                val registerUserRole = tx.outputStates.single() as UserRoleState
                "Role is required" using (registerUserRole.name.isNotBlank() )

                "Only the current user may sign the transaction." using
                        (command.signers.toSet() == registerUserRole.participants.map { it.owningKey }.toSet())
            }
        }
    }


}
