package coadjute.contracts.job

import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.TypeOnlyCommandData
import net.corda.core.transactions.LedgerTransaction

class JobLogContract: Contract
{
    companion object
    {
        const val JOBLOG_ID = "coadjute.contracts.job.JobLogContract"
    }

    override fun verify(tx: LedgerTransaction)
    {
    }

    interface Commands : CommandData
    {
        class CreateOrUpdate : TypeOnlyCommandData(), Commands
        class Collaborate : TypeOnlyCommandData(), Commands
    }
}