package coadjute.contracts.job

import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.TypeOnlyCommandData
import net.corda.core.transactions.LedgerTransaction

class SignatoriesContract : Contract
{
    companion object
    {
        const val SIGNATORIES_ID = "coadjute.contracts.job.SignatoriesContract"
    }

    interface Commands : CommandData
    {
        class Create : TypeOnlyCommandData(), Commands
    }

    override fun verify(tx: LedgerTransaction)
    {

    }
}