package coadjute.contracts.job

import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.TypeOnlyCommandData
import net.corda.core.transactions.LedgerTransaction

class ReviewersContract : Contract
{
    companion object
    {
        const val REVIEWER_ID = "coadjute.contracts.job.ReviewersContract"
    }

    interface Commands : CommandData
    {
        class Create : TypeOnlyCommandData(), Commands
    }

    override fun verify(tx: LedgerTransaction)
    {

    }
}