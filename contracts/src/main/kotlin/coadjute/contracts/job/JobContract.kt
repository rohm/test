package coadjute.contracts.job

import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.TypeOnlyCommandData
import net.corda.core.transactions.LedgerTransaction

class JobContract : Contract
{
    companion object
    {
        const val JOB_ID = "coadjute.contracts.job.JobContract"
    }

    interface Commands : CommandData
    {
        class Create : TypeOnlyCommandData(), Commands
        class Update : TypeOnlyCommandData(), Commands
        class Accept : TypeOnlyCommandData(), Commands
        class Decline : TypeOnlyCommandData(), Commands
        class Collaborate : TypeOnlyCommandData(), Commands
        class Share : TypeOnlyCommandData(), Commands
    }

    override fun verify(tx: LedgerTransaction)
    {

    }
}