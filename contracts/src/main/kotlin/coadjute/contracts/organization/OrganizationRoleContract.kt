package coadjute.contracts.organization

import coadjute.states.organization.OrganizationRoleState
import net.corda.core.contracts.*
import net.corda.core.transactions.LedgerTransaction

class OrganizationRoleContract : Contract
{
    companion object
    {
        const val ORGROLE_ID = "coadjute.contracts.organization.OrganizationRoleContract"
    }

    interface Commands : CommandData
    {
        class Register : TypeOnlyCommandData(), Commands
    }

    override fun verify(tx: LedgerTransaction)
    {
        val command = tx.commands.requireSingleCommand<Commands>()
        when (command.value)
        {
            is Commands.Register -> requireThat {
                "No inputs should be consumed when registering an Organization Role." using (tx.inputs.isEmpty())
                "Only one output state should be created when registering an Organization Role." using (tx.outputs.size == 1)
                val registerOrgRole = tx.outputStates.single() as OrganizationRoleState
                "Role name is required" using (registerOrgRole.name.isNotBlank() )

                "Only the current user may sign the transaction." using
                        (command.signers.toSet() == registerOrgRole.participants.map { it.owningKey }.toSet())
            }
        }
    }


}