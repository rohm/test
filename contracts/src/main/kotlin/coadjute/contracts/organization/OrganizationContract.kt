package coadjute.contracts.organization

import coadjute.states.organization.OrganizationState
import net.corda.core.contracts.*
import net.corda.core.transactions.LedgerTransaction

// ************
// * Contract *
// ************
class OrganizationContract : Contract
{
    companion object
    {
        // Used to identify our contract when building a transaction.
        const val ORG_ID = "coadjute.contracts.organization.OrganizationContract"
    }

    interface Commands : CommandData
    {
        class Register : TypeOnlyCommandData(), Commands
        class Update : TypeOnlyCommandData(), Commands
    }

    override fun verify(tx: LedgerTransaction)
    {
        val command = tx.commands.requireSingleCommand<Commands>()
        when (command.value)
        {
            is Commands.Register -> requireThat {
                "No inputs should be consumed when registering an Organization." using (tx.inputs.isEmpty())
                "Only one output state should be created when registering an Organization." using (tx.outputs.size == 1)
                val registerOrganization = tx.outputStates.single() as OrganizationState
                "Name is required" using (registerOrganization.name.isNotBlank() )
                "Email is required" using (registerOrganization.email.isNotBlank() )
                "Organization Role ID is required" using (registerOrganization.organizationRoleId.isNotBlank() )

                //Name of Company can be random, but this is working for a Person's Name
//                val NAME_REGEX = "^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*\$".toRegex()
//                "Name should be a valid name" using (registerOrganization.name.contains(regex = NAME_REGEX))

                val EMAIL_REGEX = "^[A-Za-z](.*)([@]{1})(.{1,})(\\.)(.{1,})".toRegex()
                "E-mail should be a valid E-mail Address" using (registerOrganization.email.contains(regex = EMAIL_REGEX))

//                val PNUM_REGEX = "^([\\+][0-9]{1,3}([ \\.\\-])?)?([\\(]{1}[0-9]{3}[\\)])?([0-9A-Z \\.\\-]{1,32})((x|ext|extension)?[0-9]{1,4}?)\$".toRegex()
//                val UKNUM_REGEX = "^((\\+44\\s?\\d{4}|\\(?\\d{5}\\)?)\\s?\\d{6})|((\\+44\\s?|0)7\\d{3}\\s?\\d{6})\$".toRegex()

//                //([+]?\d{1,2}[.-\s]?)?(\d{3}[.-]?){2}\d{4}
//                val UKNUM_REGEX = "^((\\+44\\s?\\d{4}|\\(?\\d{5}\\)?)\\s?\\d{6})|((\\+44\\s?|0)7\\d{3}\\s?\\d{6})\$".toRegex()
//                "Contact Number should be a valid UK Phone Number" using (registerOrganization.contactNumber.contains(regex = UKNUM_REGEX))

//                val NUM_REGEX = "^[-,0-9]+\$".toRegex()
//                val ALPHA3_REGEX = "^[A-Z]{3}$".toRegex()
                "Only the current user may sign the transaction." using
                        (command.signers.toSet() == registerOrganization.participants.map { it.owningKey }.toSet())
            }

            is Commands.Update -> requireThat {
                "Only one input should be consumed when updating an Organization." using (tx.inputs.size == 1)
                "Only one output state should be created when updating an Organization." using (tx.outputs.size == 1)
                val registerOrganization = tx.outputStates.single() as OrganizationState
                "Name is required" using (registerOrganization.name.isNotBlank() )
                "Email is required" using (registerOrganization.email.isNotBlank() )
                "Organization Role ID is required" using (registerOrganization.organizationRoleId.isNotBlank() )

                val EMAIL_REGEX = "^[A-Za-z](.*)([@]{1})(.{1,})(\\.)(.{1,})".toRegex()
                "E-mail should be a valid E-mail Address" using (registerOrganization.email.contains(regex = EMAIL_REGEX))

//                val PNUM_REGEX = "^([\\+][0-9]{1,3}([ \\.\\-])?)?([\\(]{1}[0-9]{3}[\\)])?([0-9A-Z \\.\\-]{1,32})((x|ext|extension)?[0-9]{1,4}?)\$".toRegex()
//                val UKNUM_REGEX = "^((\\+44\\s?\\d{4}|\\(?\\d{5}\\)?)\\s?\\d{6})|((\\+44\\s?|0)7\\d{3}\\s?\\d{6})\$".toRegex()

//                val NUM_REGEX = "^[-,0-9]+\$".toRegex()
//                val ALPHA3_REGEX = "^[A-Z]{3}$".toRegex()

                "Only the current user may sign the transaction." using
                        (command.signers.toSet() == registerOrganization.participants.map { it.owningKey }.toSet())
            }

        }
    }



}